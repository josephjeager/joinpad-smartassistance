﻿using System;
using SmartAssistanceSDK.Models;

namespace SmartAssistanceSDK.Delegates
{
    public class SASignalerEventArgs
    {
        public SASignalerEventType EventType { get; private set; }
        public string Message { get; private set; }
        public SAUser User { get; private set; }
        public SASignalerEventArgs(SASignalerEventType eventType, string message, SAUser user = null)
        {
            this.EventType = eventType;
            this.Message = message;
            this.User = user;
        }
    }
}
