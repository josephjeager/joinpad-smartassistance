﻿using System;
namespace SmartAssistanceSDK.Delegates
{
    public delegate void SmartAssistanceSignalerEvent(object sender, SASignalerEventArgs args);
}
