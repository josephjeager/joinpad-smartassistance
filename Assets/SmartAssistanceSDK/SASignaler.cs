﻿using System;
using System.Collections.Generic;
using BestHTTP;
using BestHTTP.SocketIO;
using BestHTTP.SocketIO.Transports;
using LitJson;
using PlatformSupport.Collections.ObjectModel;
using SmartAssistanceSDK.Delegates;
using SmartAssistanceSDK.EnvironmentConfiguration;
using SmartAssistanceSDK.HttpResponse;
using SmartAssistanceSDK.Models;
using UnityEngine;

namespace SmartAssistanceSDK
{
    public class SASignaler : IDisposable
    {
        private SocketManager Manager;
        public SAEnviromentConfig Environment;
        private readonly SADeviceType _deviceType;
        public SAUser Me { get; private set; }
        private string authToken;
        private SAUserRole userRole;
        private SAUserStatus _prevStatus = SAUserStatus.Available;


        public List<SAUser> ConnectedUsers { get; private set; }

        public event SmartAssistanceSignalerEvent SignalerEvent;

        private void SendSignalerEvent(SASignalerEventType eventType, string message = null, SAUser user = null)
        {
            if (SignalerEvent != null)
            {
                SignalerEvent(this, new SASignalerEventArgs(eventType, message, user));
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:SmartAssistanceSDK.SAUserSignaler"/> class.
        /// </summary>
        /// <param name="environment">Environment, needed connect to the correct server</param>
        /// <param name="deviceType">Device type (smatphone, tablet or hololens)</param>
        public SASignaler(SAEnvironmentType environment, SADeviceType deviceType = SADeviceType.Hololens)
        {
            this.Environment = SAConfigFactory.GetConfiguration(environment);
            this._deviceType = deviceType;
            this.ConnectedUsers = new List<SAUser>();
        }

        public void Dispose()
        {
            if (Manager != null)
            {
                Manager.Close();
                Manager = null;
            }

            if (this.ConnectedUsers != null)
            {
                this.ConnectedUsers.Clear();
                this.ConnectedUsers = null;
            }
        }
        private void OnLogout(){
            this.Me = null;
            this.authToken = "";
            if (Manager != null)
            {
                Manager.Close();
            }
            if (this.ConnectedUsers != null)
            {
                this.ConnectedUsers.Clear();
            }
        }

        /// <summary>
        /// Let the user login over the Smart Assistance Backend.
        /// Emits OnLoginSuccess event on success.
        /// After that event, Me variable is set.
        /// </summary>
        /// <param name="username">Username</param>
        /// <param name="password">Password</param>
        /// <param name="deviceID">Device Identifier, set in Smart Assistance device settings, optional</param>
        public void Login(SAUserRole userRole, string username, string password = null, string deviceID = null)
        {
            try
            {
                this.userRole = userRole;
                HTTPRequest request = new HTTPRequest(new Uri(this.Environment.LoginUrl), HTTPMethods.Post,
                (HTTPRequest originalRequest, HTTPResponse response) =>
                {
                    if (response.IsSuccess)
                    {
                        JsonReader reader = new JsonReader(response.DataAsText)
                        {
                            SkipNonMembers = true
                        };
                        var loginresp = JsonMapper.ToObject<LoginResponse>(reader);
                        this.Me = loginresp.user;
                        this.authToken = loginresp.token;
                        this.SendSignalerEvent(SASignalerEventType.LoginSuccess);
                        this.InitUserSocket();

                    }
                    else
                    {
                        this.SendSignalerEvent(SASignalerEventType.LoginError, response.Message);
                    }
                });
                request.AddField("username", username);
                request.AddField("user_role", ((int)userRole).ToString());
                request.AddField("device_id", this._deviceType.ToString());
                if (!String.IsNullOrEmpty(password)) request.AddField("password", password);
                //if (!String.IsNullOrEmpty(deviceID)) request.AddField("device_id", deviceID);
                request.Send();
            }catch(Exception e)
            {
                this.SendSignalerEvent(SASignalerEventType.LoginError, e.Message);
            }
        }

        /// <summary>
        /// Logout this user.
        /// </summary>
        public void Logout()
        {

            try
            {
                HTTPRequest request = new HTTPRequest(new Uri(this.Environment.LogoutUrl), HTTPMethods.Get,
            (HTTPRequest originalRequest, HTTPResponse response) =>
            {
                if (response.IsSuccess)
                {
                    this.OnLogout();
                    this.SendSignalerEvent(SASignalerEventType.LogoutSuccess);
                }
                else
                {
                    this.SendSignalerEvent(SASignalerEventType.LogoutError, response.Message);
                }
            });
                request.AddHeader("Authorization", "Bearer " + this.authToken);
                request.Send();
            }
            catch (Exception e)
            {
                this.SendSignalerEvent(SASignalerEventType.LogoutError, e.Message);
            }
        }

        /// <summary>
        /// Inits the user socket.
        /// </summary>
        private void InitUserSocket()
        {
            var options = new SocketOptions()
            {
                AutoConnect = true,
                AdditionalQueryParams = new ObservableDictionary<string, string>(),
                ConnectWith = TransportTypes.WebSocket,
                ReconnectionAttempts = 2
            };
            options.AdditionalQueryParams.Add("token", authToken);
            options.AdditionalQueryParams.Add("user_role", string.Format("{0}", (int)this.userRole));
            options.AdditionalQueryParams.Add("device_type", string.Format("{0}", (int)this._deviceType));

            // Create the Socket.IO manager
            Manager = new SocketManager(new Uri(Environment.UserSignalerUrl), options);

            // The argument will be an Error object.

            Manager.Socket.On(SocketIOEventTypes.Error, (socket, packet, args) =>
                              this.SendSignalerEvent(SASignalerEventType.Error, string.Format("Socket Error: {0}", args[0]))
                             );

            //emitted by the server on succesfull connection
            Manager.Socket.Once(SocketIOEventTypes.Connect, (socket, packet, arg) =>
            {
                this.ConnectedUsers.Clear();
                this.SendSignalerEvent(SASignalerEventType.SignalerConnected);
            });

            //emitted by the server on socket disconnection
            Manager.Socket.Once(SocketIOEventTypes.Disconnect, (socket, packet, arg) =>
            {
                this.ConnectedUsers.Clear();
                this.SendSignalerEvent(SASignalerEventType.SignalerDisconnected);
            });

            //sent when a user connects on the server, user object is sent in the package
            Manager.Socket.On("user:connected", (socket, packet, args) =>
            {
                JsonReader reader = new JsonReader(packet.RemoveEventName(true))
                {
                    SkipNonMembers = true
                };
                var newUser = JsonMapper.ToObject<SAUser>(reader);
                this.ConnectedUsers.Add(newUser);
                this.SendSignalerEvent(SASignalerEventType.UserConnected, newUser.username, newUser);
            });

            //sent after connection, the package contains the list of users already connected
            Manager.Socket.On("user:list", (socket, packet, args) =>
            {
                JsonReader reader = new JsonReader(packet.RemoveEventName(true))
                {
                    SkipNonMembers = true
                };
                var userlist = JsonMapper.ToObject<SAUser[]>(reader);
                this.ConnectedUsers.AddRange(userlist);
                this.SendSignalerEvent(SASignalerEventType.UserListUpdated);
            });
            //sent when a user disconnects from the server, user object is sent in the package
            Manager.Socket.On("user:disconnected", (socket, packet, args) =>
            {
                JsonReader reader = new JsonReader(packet.RemoveEventName(true))
                {
                    SkipNonMembers = true
                };
                var discUser = JsonMapper.ToObject<SAUser>(reader);
                var usr = this.ConnectedUsers.Find(u => u.peer_id == discUser.peer_id);
                if (usr != null)
                {
                    this.ConnectedUsers.Remove(usr);
                    this.SendSignalerEvent(SASignalerEventType.UserDisconnected, usr.username, usr);
                }
            });


            Manager.Socket.On("user:message", (socket, packet, args) => { Debug.Log("user " + args[0] + " sent message: " + args[1]); });

            Manager.Socket.On("call:request", (socket, packet, args) =>
            {
                var usr = this.ConnectedUsers.Find(u => u.peer_id == args[0] as string);
                if (usr != null)
                {
                    this.SendSignalerEvent(SASignalerEventType.CallRequestReceived, args[1] as string, usr);
                    Debug.Log("Call request received from " + usr.username);
                }
                else
                {
                    Debug.Log("Call request received from UNKNOWN");
                }
            });
            Manager.Socket.On("call:accepted", (socket, packet, args) =>
            {
                var usr = this.ConnectedUsers.Find(u => u.peer_id == args[0] as string);
                if (usr != null)
                {
                    SendSignalerEvent(SASignalerEventType.CallAccepted, args[1] as string, usr);
                }
                else
                {
                    this.SendSignalerEvent(SASignalerEventType.Error, "Accepted call by an unknown user");
                }
            });

            Manager.Socket.On("call:rejected", (socket, packet, args) =>
            {

                var usr = this.ConnectedUsers.Find(u => u.peer_id == args[0] as string);
                if (usr != null)
                {
                    SendSignalerEvent(SASignalerEventType.CallRejected, args[1] as string, usr);
                }
                else
                {
                    this.SendSignalerEvent(SASignalerEventType.Error, "Rejected call by an unknown user");
                }
                this.SetStatus(this._prevStatus);
            });

            Manager.Socket.On("call:stopRequest", (socket, packet, args) =>
            {
                var usr = this.ConnectedUsers.Find(u => u.peer_id == args[0] as string);
                if (usr != null)
                {
                    SendSignalerEvent(SASignalerEventType.CallStopRequestReceived, args[1] as string, usr);
                }
                else
                {
                    this.SendSignalerEvent(SASignalerEventType.Error, "Call Request stopped by an unknown user");
                }
                this.SetStatus(this._prevStatus);
            });

            Manager.Socket.On("call:stop", (socket, packet, args) =>
            {
                var usr = this.ConnectedUsers.Find(u => u.peer_id == args[0] as string);
                if (usr != null)
                {
                    SendSignalerEvent(SASignalerEventType.CallStop, args[1] as string, usr);
                }
                else
                {
                    this.SendSignalerEvent(SASignalerEventType.Error, "Call stopped by an unknown user");
                }
                this.SetStatus(this._prevStatus);
            });

            Manager.Socket.On("user:statuschange", (socket, packet, args) =>
            {
                JsonReader reader = new JsonReader(packet.RemoveEventName(true))
                {
                    SkipNonMembers = true
                };
                var userChange = JsonMapper.ToObject<SAUser>(reader);
                var usr = this.ConnectedUsers.Find(u => u.peer_id == userChange.peer_id);
                if (usr != null)
                {
                    usr.status = userChange.status;
                    this.SendSignalerEvent(SASignalerEventType.UserListUpdated, usr.username, usr);
                }

            });
            Manager.Socket.On("reconnect_failed", (socket, packet, args) =>
            {
                this.SendSignalerEvent(SASignalerEventType.SocketReconnectionFailed);
            });

            // We set SocketOptions' AutoConnect to false, so we have to call it manually.
            Manager.Open();
        }

        /// <summary>
        /// Calls the user.
        /// </summary>
        /// <returns>The session ID. Use it to open the call socket.</returns>
        /// <param name="username">Username of the user to call.</param>
        public string CallUser(string username)
        {
            this._prevStatus = this.Me.status;
            this.SetStatus(SAUserStatus.InCall);
            var usr = this.ConnectedUsers.Find(u => u.username.ToLower() == username.ToLower());
            if (usr != null)
            {
                var sessionId = "session_" + this.Me.username + DateTime.UtcNow.Ticks;
                Manager.Socket.Emit("call:request", usr.peer_id, sessionId);
                return sessionId;
            }
            else
            {
                this.SendSignalerEvent(SASignalerEventType.Error, "User not found");
                return null;
            }

        }

        public void AcceptCall(string username, string sessionID)
        {
            this._prevStatus = this.Me.status;
            this.SetStatus(SAUserStatus.InCall);
            var usr = this.ConnectedUsers.Find(u => u.username.ToLower() == username.ToLower());
            if (usr != null)
            {
                Manager.Socket.Emit("call:accepted", usr.peer_id, sessionID);
            }
            else
            {
                this.SendSignalerEvent(SASignalerEventType.Error, "User not found");
            }
        }

        public void RejectCall(string username, string sessionID)
        {
            var usr = this.ConnectedUsers.Find(u => u.username.ToLower() == username.ToLower());
            if (usr != null)
            {
                Manager.Socket.Emit("call:rejected", usr.peer_id, sessionID);
            }
            else
            {
                this.SendSignalerEvent(SASignalerEventType.Error, "User not found");
            }
            this.SetStatus(this._prevStatus);
        }


        public void StopCallRequest(string username, string sessionID)
        {
            var usr = this.ConnectedUsers.Find(u => u.username.ToLower() == username.ToLower());
            if (usr != null)
            {
                Manager.Socket.Emit("call:stopRequest", usr.peer_id, sessionID);
            }
            else
            {
                this.SendSignalerEvent(SASignalerEventType.Error, "User not found");
            }
            this.SetStatus(this._prevStatus);
        }

        public void StopCall(string username, string sessionID)
        {
            var usr = this.ConnectedUsers.Find(u => u.username.ToLower() == username.ToLower());
            if (usr != null)
            {
                Manager.Socket.Emit("call:stop", usr.peer_id, sessionID);
            }
            else
            {
                this.SendSignalerEvent(SASignalerEventType.Error, "User not found");
            }
            this.SetStatus(this._prevStatus);
        }


        public void SetStatus(SAUserStatus status)
        {
            this.Me.status = status;
            this.Manager.Socket.Emit("user:statuschange", status);
            this.SendSignalerEvent(SASignalerEventType.MeUpdated,"",this.Me);
        }
    }
}