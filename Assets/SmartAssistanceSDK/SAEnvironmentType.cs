﻿namespace SmartAssistanceSDK
{
    public enum SAEnvironmentType
    {
        Alstom,
        China,
        Debug,
        DevUbuntu,
        Joinpad,
        Test
    }
}