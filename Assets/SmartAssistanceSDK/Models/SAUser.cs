﻿using System;
namespace SmartAssistanceSDK.Models
{
    public class SAUser
    {
        public string id { get; set; }
        public string username { get; set; }
        public string name { get; set; }
        public string lastname { get; set; }
        public string location { get; set; }
        public string country { get; set; }
        public string description { get; set; }
        public string peer_id { get; set; }
        public string device_id { get; set; }
        public string profile_picture { get; set; }
        public SADeviceType device_type { get; set; }
        public SAUserStatus status { get; set; }
        public SAUserRole role { get; set; }
        public SARooms[] rooms { get; set; }

        public SAUser() {
        
        }
    }
}
