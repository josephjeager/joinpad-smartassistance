﻿using System;
namespace SmartAssistanceSDK.Models
{
    public class SAIceServer
    {
        public string Url { get; private set; }
        public string Username { get; private set; }
        public string Password { get; private set; }
        public SAIceServer(string url,string username=null,string password=null)
        {
            this.Url = url;
            this.Username = username;
            this.Password = password;
        }
    }
}
