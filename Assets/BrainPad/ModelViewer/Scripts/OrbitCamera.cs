using System.Diagnostics.CodeAnalysis;
using UnityEngine;

namespace BrainPad.ModelViewer.Scripts
{
    [SuppressMessage("ReSharper", "NotAccessedField.Global")]
    [SuppressMessage("ReSharper", "UnusedMember.Local")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_BuiltInTypes")]
    public class OrbitCamera : MonoBehaviour
    {
        public GameObject PlayerPivot;
        public GameObject PlayerCamera;

        public int PinchUpMaxDegrees = 90;
        public int PinchDownMaxDegrees = 90;
        public int ZoomMaxDistance = 100;
        public int ZoomMinDistance = 10;
        public float GestureSensitivityX = 1f;
        public float GestureSensitivityY = 1f;
        public float RotationInertiaTime = 1;
        
        private const float FingerPinchMinDistance = 0;
        private const float FingerPinchScaleFactor = 10;

        private float _mouseRotationY;
        private float _mouseRotationX;
        private float _lastMouseRotationY;
        private float _lastMouseRotationX;
        private float _fingerPinchDistanceDelta;
        private float _fingerPinchDistance;
        private float _screenGestureSensitivityX;
        private float _screenGestureSensitivityY;

        private float _currentLerpTime;
        
        private void Start()
        {
            _screenGestureSensitivityX = GestureSensitivityX * 2000 / Screen.width;
            _screenGestureSensitivityY = GestureSensitivityY * 2000 / Screen.height;
        }

        private void LateUpdate()
        {
#if UNITY_EDITOR
            if (Input.GetMouseButton(0))
            {
#else
            if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Moved)
            {
#endif
                _currentLerpTime = 0;
                _lastMouseRotationX = Input.GetAxis("Mouse X");
                _lastMouseRotationY = Input.GetAxis("Mouse Y");

                _mouseRotationX = PlayerPivot.transform.localEulerAngles.y + _lastMouseRotationX * _screenGestureSensitivityX;
                _mouseRotationY += _lastMouseRotationY * _screenGestureSensitivityY;
                _mouseRotationY = Mathf.Clamp(_mouseRotationY, -PinchDownMaxDegrees, PinchUpMaxDegrees);

                PlayerPivot.transform.localEulerAngles = new Vector3(-_mouseRotationY, _mouseRotationX, 0);
            }
#if !UNITY_EDITOR
            else if (Input.touchCount >= 2)
            {
                // Stop inertia if zooming in / out
                _currentLerpTime = RotationInertiaTime;
    
                Touch touch1 = Input.touches[0];
                Touch touch2 = Input.touches[1];

                if (touch1.phase == TouchPhase.Moved || touch2.phase == TouchPhase.Moved)
                {
                    // ... check the delta distance between them ...
                    _fingerPinchDistance = Vector2.Distance(touch1.position, touch2.position);
                    float prevDistance = Vector2.Distance(touch1.position - touch1.deltaPosition, touch2.position - touch2.deltaPosition);
                    _fingerPinchDistanceDelta = _fingerPinchDistance - prevDistance;

                    // ... if it's greater than a minimum threshold, it's a pinch!
                    if (Mathf.Abs(_fingerPinchDistanceDelta) > FingerPinchMinDistance)
                    {
                        _fingerPinchDistanceDelta *= _screenGestureSensitivityX / FingerPinchScaleFactor;
                    }
                    else
                    {
                        _fingerPinchDistance = _fingerPinchDistanceDelta = 0;
                    }
                    float distance = Mathf.Clamp(PlayerCamera.transform.localPosition.z + _fingerPinchDistanceDelta, -ZoomMaxDistance, -ZoomMinDistance);
                    PlayerCamera.transform.localPosition = new Vector3(PlayerCamera.transform.localPosition.x, PlayerCamera.transform.localPosition.y, distance);
                }
            }
#endif
            else if (_currentLerpTime < RotationInertiaTime)
            {
                _currentLerpTime += Time.deltaTime;
                if (_currentLerpTime > RotationInertiaTime)
                {
                    _currentLerpTime = RotationInertiaTime;
                }

                float lerpTime = _currentLerpTime / RotationInertiaTime;

                lerpTime = 1f - Mathf.Cos(lerpTime * Mathf.PI * 0.5f);
                
                float mouseRotationLerpX = Mathf.Lerp(_lastMouseRotationX, 0, lerpTime);
                float mouseRotationLerpY = Mathf.Lerp(_lastMouseRotationY, 0, lerpTime);

                _mouseRotationX = PlayerPivot.transform.localEulerAngles.y + mouseRotationLerpX * _screenGestureSensitivityX;
                _mouseRotationY += mouseRotationLerpY * _screenGestureSensitivityY;
                _mouseRotationY = Mathf.Clamp(_mouseRotationY, -PinchDownMaxDegrees, PinchUpMaxDegrees);

                PlayerPivot.transform.localEulerAngles = new Vector3(-_mouseRotationY, _mouseRotationX, 0);
            }
        }
    }
}