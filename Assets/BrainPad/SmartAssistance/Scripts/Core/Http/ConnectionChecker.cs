﻿using System.Collections;
using System.Diagnostics.CodeAnalysis;
using BrainPad.SmartAssistance.Scripts.Core.Delegates;
using UnityEngine;

namespace BrainPad.SmartAssistance.Scripts.Core.Http
{
    [SuppressMessage("ReSharper", "SuggestVarOrType_BuiltInTypes")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_SimpleTypes")]
    public class ConnectionChecker : MonoBehaviour
    {
        public static ConnectionChecker Instance;

        public string Endpoint = "http://www.google.com";
        public float TimeoutSeconds = 10;
        public float WaitBetweenChecksSeconds = 5;
        public float OptimalDownloadSpeedMbps = 0.5f;

        public Status CurrentStatus = Status.Offline;
        public float CurrentFitness;
        public float CurrentSpeedMbps;

        public event ConnectionCheckedEvent ConnectionCheckedEvent;

        private IEnumerator _connecionCheckLoop;
        private bool _stop;

        public enum Status
        {
            Offline,
            Timeout,
            Online
        }

        private void Awake()
        {
            if (Instance != null) return;
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }

        private void Start()
        {
            StartCoroutine(_connecionCheckLoop = CheckConnectionRoutine());
        }

        private void OnDestroy()
        {
            _stop = true;
            if (_connecionCheckLoop != null)
            {
                StopCoroutine(_connecionCheckLoop);
            }
        }

        private IEnumerator CheckConnectionRoutine()
        {
            while (!_stop)
            {
                float time = Time.time;
                WWW www = new WWW(Endpoint);
                float timer = 0;
                bool timeout = false;
                bool error = false;

                while (!www.isDone)
                {
                    if (timer > TimeoutSeconds)
                    {
                        timeout = true;
                        break;
                    }

                    timer += Time.deltaTime;
                    yield return null;
                }

                if (!string.IsNullOrEmpty(www.error))
                {
                    error = true;
                }

                if (error)
                {
                    CurrentStatus = Status.Offline;
                    CurrentFitness = 0;
                }
                else if (timeout)
                {
                    CurrentStatus = Status.Timeout;
                    CurrentFitness = 0;
                }
                else
                {
                    float responseTime = Time.time - time;
                    CurrentSpeedMbps = www.bytesDownloaded / responseTime / 1000000;

                    // Debug.Log("Connection Speed Mb/s: " + CurrentSpeedMbps);

                    CurrentFitness = CurrentSpeedMbps / OptimalDownloadSpeedMbps;
                    if (CurrentFitness > 1)
                    {
                        CurrentFitness = 1;
                    }

                    CurrentStatus = Status.Online;
                }

                www.Dispose();

                if (ConnectionCheckedEvent != null)
                {
                    ConnectionCheckedEvent(this, CurrentStatus, CurrentSpeedMbps, CurrentFitness);
                }

                yield return new WaitForSeconds(WaitBetweenChecksSeconds);
            }
        }
    }
}