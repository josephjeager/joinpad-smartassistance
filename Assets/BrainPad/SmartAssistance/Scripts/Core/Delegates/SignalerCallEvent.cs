﻿using SmartAssistanceSDK.Delegates;

namespace BrainPad.SmartAssistance.Scripts.Core.Delegates
{
    public delegate void SignalerCallEvent(object sender, SASignalerEventArgs args);
}