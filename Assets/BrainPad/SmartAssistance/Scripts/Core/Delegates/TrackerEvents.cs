﻿using UnityEngine;

namespace BrainPad.SmartAssistance.Scripts.Core.Delegates
{
    public delegate bool AddTrackerEvent(object sender, int widgetId, Rect trackingRect);

    public delegate void RemoveTrackerEvent(object sender, int widgetId);
}