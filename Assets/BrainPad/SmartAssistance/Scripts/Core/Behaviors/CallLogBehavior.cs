﻿using System.Collections;
using System.Diagnostics.CodeAnalysis;
using Byn.Media;
using GameToolkit.Localization;
using TMPro;
using UnityEngine;

namespace BrainPad.SmartAssistance.Scripts.Core.Behaviors
{
    [SuppressMessage("ReSharper", "SwitchStatementMissingSomeCases")]
    [SuppressMessage("ReSharper", "InvertIf")]
    public class CallLogBehavior : MonoBehaviour
    {
        public TextMeshProUGUI TextArea;
        
        [Header("Notifications")] public LocalizedText ConfigurationComplete;
        public LocalizedText AccessingAudioVideoFailed;
        public LocalizedText ConnectionFailed;
        public LocalizedText StreamReceiverFaileIinitialization;
        public LocalizedText CallAccepted;
        public LocalizedText CallEnded;
        public LocalizedText MessageReceived;
        public LocalizedText WaitingForIncomingStream;
        public LocalizedText SendingStream;
        public LocalizedText ReceivingStream;
        
        private IEnumerator _clearDelayedCoroutine;

        private void OnEnable()
        {
            TextArea.text = "";
            SmartAssistanceManager.Instance.WebRtcCallEvent += OnWebRtcCallEvent;
            SmartAssistanceManager.Instance.NotificationEvent += OnNotificationEvent;
        }

        private void OnDisable()
        {
            if (SmartAssistanceManager.Instance != null)
            {
                SmartAssistanceManager.Instance.WebRtcCallEvent -= OnWebRtcCallEvent;
                SmartAssistanceManager.Instance.NotificationEvent -= OnNotificationEvent;
            }
        }

        private void OnWebRtcCallEvent(object sender, ICall call, CallEventArgs args)
        {
            switch (args.Type)
            {
                case CallEventType.ConfigurationComplete:
                    TextArea.text = ConfigurationComplete.Value;
                    break;
                case CallEventType.ConfigurationFailed:
                    TextArea.text = AccessingAudioVideoFailed.Value;
                    break;
                case CallEventType.ConnectionFailed:
                    TextArea.text = ConnectionFailed.Value;
                    break;
                case CallEventType.ListeningFailed:
                    TextArea.text = StreamReceiverFaileIinitialization.Value;
                    break;
                case CallEventType.CallAccepted:
                    TextArea.text = CallAccepted.Value;
                    break;
                case CallEventType.CallEnded:
                    TextArea.text = CallEnded.Value;
                    break;
                case CallEventType.Message:
                    TextArea.text = MessageReceived.Value + ((MessageEventArgs) args).Content;
                    break;
                case CallEventType.WaitForIncomingCall:
                    TextArea.text = WaitingForIncomingStream.Value;
                    break;
                case CallEventType.FrameUpdate:
                    TextArea.text = SmartAssistanceManager.Instance.IsSender ? SendingStream.Value : ReceivingStream.Value;
                    break;
            }

            if (_clearDelayedCoroutine != null)
            {
                StopCoroutine(_clearDelayedCoroutine);
            }

            if (gameObject.activeInHierarchy)
            {
                StartCoroutine(_clearDelayedCoroutine = ClearDelayedRoutine());
            }
        }

        private void OnNotificationEvent(object sender, string message)
        {
            TextArea.text = message;

            if (_clearDelayedCoroutine != null)
            {
                StopCoroutine(_clearDelayedCoroutine);
            }

            StartCoroutine(_clearDelayedCoroutine = ClearDelayedRoutine());
        }

        private IEnumerator ClearDelayedRoutine()
        {
            yield return new WaitForSeconds(2);
            TextArea.text = "";
        }
    }
}