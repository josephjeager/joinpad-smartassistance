﻿using System.Diagnostics.CodeAnalysis;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BrainPad.SmartAssistance.Scripts.Core.Utils
{
    [SuppressMessage("ReSharper", "SuggestVarOrType_BuiltInTypes")]
    public class DragThresholdUtil : MonoBehaviour
    {
        public float Factor = 2f;

        public void Start()
        {
            int defaultValue = EventSystem.current.pixelDragThreshold;
            EventSystem.current.pixelDragThreshold = Mathf.Max(defaultValue, (int) (defaultValue * Screen.dpi / 160f * Factor));
        }
    }
}