﻿using System;
using System.Collections;
using System.Diagnostics.CodeAnalysis;
using BrainPad.SmartAssistance.Scripts.Core.Controllers;
using BrainPad.SmartAssistance.Scripts.Core.Delegates;
using BrainPad.SmartAssistance.Scripts.Core.Http;
using Byn.Media;
using Byn.Net;
using GameToolkit.Localization;
using SmartAssistanceSDK;
using SmartAssistanceSDK.Delegates;
using SmartAssistanceSDK.Models;
using UnityEngine;

namespace BrainPad.SmartAssistance.Scripts
{
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_SimpleTypes")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_BuiltInTypes")]
    [SuppressMessage("ReSharper", "ConvertIfStatementToConditionalTernaryExpression")]
    [SuppressMessage("ReSharper", "SwitchStatementMissingSomeCases")]
    [SuppressMessage("ReSharper", "NotAccessedField.Global")]
    [SuppressMessage("ReSharper", "InvertIf")]
    public class SmartAssistanceManager : MonoBehaviour
    {
        public static SmartAssistanceManager Instance;

        public int FrameWidth = 640;
        public int FrameHeight = 480;

        public bool AutoRejoin = true;
        public float RejoinTime = 2;
        public bool BlockSleep = true;
        public int NetworkFailuresBeforeLogout = 5;

        //Not yet stable on all platforms but coming soon.
        public bool TryI420;

        #region UserSignaler

        public SAUser RemoteUser;
        public SADeviceType DeviceType = SADeviceType.Smartphone;
        public SAEnvironmentType EnvironmentType = SAEnvironmentType.Joinpad;
        public SASignaler SaSignaler;

        #endregion

        public enum CallState
        {
            Invalid,
            Config,
            Calling,
            InCall,
            Ended,
            Error
        }

        public ICall Call;
        public CallState CurrentCallState = CallState.Invalid;

        [Header("Errors")] public LocalizedText ConnectionFailed;
        public LocalizedText AccessingAudioVideoFailed;
        public LocalizedText ListeningFailed;
        public LocalizedText CallEnded;
        public LocalizedText ErrorConfiguringSignalerChannel;
        public LocalizedText JoinCallFailedCallIsAlreadyStillActive;

        [HideInInspector] public bool IsLoggingIn;
        [HideInInspector] public ConnectionId RemoteConnectionId;
        [HideInInspector] public bool IsSender;
        
        public event WebRtcCallEvent WebRtcCallEvent;
        public event SignalerCallEvent SignalerCallEvent;
        public event NotificationEvent NotificationEvent;
        public event ErrorEvent ErrorEvent;

        private UiController _uiController;
        private string _signalerChannel;
        private bool _callActive;
        private int _sleepTimeoutBackup;
        private string _sessionId;
        private float _actualRejoinTime;

        private IEnumerator _rejoinCoroutine;

        private int _networkFailuresCount;

// Gyroscope is needed on iOS by UnityMediaHelper
// to properly rotate the camera according to the device rotation
//#if UNITY_IOS && !UNITY_EDITOR
//        public GameObject DeviceRotationHolder;
//#endif

        // Unity Events

        private void Awake()
        {
            if (Instance != null) return;
            Instance = this;

// Gyroscope is needed on iOS by UnityMediaHelper
// to properly rotate the camera according to the device rotation
//#if UNITY_IOS && !UNITY_EDITOR
//            DeviceRotationHolder = new GameObject();
//            Input.gyro.enabled = true;
//#endif

            DontDestroyOnLoad(gameObject);
        }

        private void Start()
        {
//            Localization.Instance.CurrentLanguage = SystemLanguage.Chinese;

            if (BlockSleep)
            {
                //backup sleep timeout and set it to never sleep
                _sleepTimeoutBackup = Screen.sleepTimeout;
                Screen.sleepTimeout = SleepTimeout.NeverSleep;
            }

            _uiController = GetComponent<UiController>();

            ConnectionChecker.Instance.ConnectionCheckedEvent += OnConnectionChecked;

            InitSignaler();
            LoginScreen();
        }

        private void Update()
        {
// Gyroscope is needed on iOS by UnityMediaHelper
// to properly rotate the camera according to the device rotation
//#if UNITY_IOS && !UNITY_EDITOR
//            DeviceRotationHolder.transform.rotation = Input.gyro.attitude;
//            DeviceRotationHolder.transform.Rotate(0f, 0f, 180f, Space.Self); // Swap "handedness" of quaternion from gyro.
//            DeviceRotationHolder.transform.Rotate(90f, 180f, 0f, Space.World);
//#endif

            if (Call != null)
            {
                Call.Update();
            }
        }

        private void OnApplicationPause(bool pauseStatus)
        {
            // This notifies both OnPause and OnResume
            bool isPaused = pauseStatus;
            if (isPaused)
            {
                if (BlockSleep)
                {
                    //revert to the original value
                    Screen.sleepTimeout = _sleepTimeoutBackup;
                }
            }
            else
            {
                if (BlockSleep)
                {
                    //backup sleep timeout and set it to never sleep
                    _sleepTimeoutBackup = Screen.sleepTimeout;
                    Screen.sleepTimeout = SleepTimeout.NeverSleep;
                }
            }
        }

        private void OnDestroy()
        {
            if (_callActive)
            {
                SaSignaler.StopCall(RemoteUser.name, _sessionId);
                AutoRejoin = false;
                if (_rejoinCoroutine != null)
                {
                    StopCoroutine(_rejoinCoroutine);
                }
            }

            Instance = null;
            CleanupWebRtcCall();
            CleanupSignaler();
        }

        // App Level Navigation

        public void LoginScreen()
        {
            _uiController.OpenLoginScreen();
        }

        public void ContactsScreen()
        {
            _uiController.OpenContactsScreen();
        }

        public void InstructionsScreen()
        {
            _uiController.OpenInstructionsScreen();
        }

        public void InstructionsScreenWithAnimation()
        {
            _uiController.OpenInstructionsScreenWithAnimation();
        }

        public void TutorialStartACallScreen()
        {
            _uiController.OpenTutorialStartACallScreen();
        }

        public void TutorialArWidgetsScreen()
        {
            _uiController.OpenTutorialArWidgetsScreen();
        }

        public void TutorialDrawWidgetsScreen()
        {
            _uiController.OpenTutorialDrawWidgetsScreen();
        }

        public void TutorialRemoveWidgetsScreen()
        {
            _uiController.OpenTutorialRemoveWidgetsScreen();
        }

        public void CallScreen()
        {
            _uiController.OpenCallScreen();
        }

        public void ShowLogout()
        {
            _uiController.ShowLogoutScreen();
        }

        public void HideLogout()
        {
            _uiController.HideLogoutScreen();
        }

        // Signaler Configuration

        private void InitSignaler()
        {
            SaSignaler = new SASignaler(EnvironmentType, DeviceType);
            SaSignaler.SignalerEvent += SaSignaler_SignalerEvent;
        }

        // Signaler Controls

        public void Login(SAUserRole saUserRole, string username, string password)
        {
            SaSignaler.Login(saUserRole, username, password, SystemInfo.deviceUniqueIdentifier);
        }

        public void CallUser(SAUser user)
        {
            RemoteUser = user;
            _sessionId = SaSignaler.CallUser(user.username);
            _signalerChannel = _sessionId;

            // Calling RemoteExpert configures Call immediately
            if (SaSignaler.Me.role == SAUserRole.RemoteExpert)
            {
                ConfigureReceiver();
            }

            // Calling OnField just needs to ConfigureSender() once call is accepted (Called RemoteExpert is supposed to have already done ConfigureReceiver() at that time)
            _uiController.OpenStartCallScreen();
        }

        public void AcceptCall()
        {
            // Called RemoteExpert does ConfigureReceiver(), then wait some delay and then accepts the call notifing OnField that he is ready to receive the stream
            if (SaSignaler.Me.role == SAUserRole.RemoteExpert)
            {
                ConfigureReceiver();
                StartCoroutine(AcceptCallDelayedRoutine());
            }
            // Called OnField starts the stream, knowing that RemoteExpert has already done ConfigureReceiver()
            else
            {
                SaSignaler.AcceptCall(RemoteUser.username, _sessionId);
                ConfigureSender();
            }
        }

        private IEnumerator AcceptCallDelayedRoutine()
        {
            yield return new WaitForSeconds(5);
            SaSignaler.AcceptCall(RemoteUser.username, _sessionId);
        }

        public void StopCallRequest()
        {
            SaSignaler.StopCallRequest(RemoteUser.name, _sessionId);
            WebRtcEndCall();
        }

        public void RejectCall()
        {
            SaSignaler.RejectCall(RemoteUser.name, _sessionId);
            WebRtcEndCall();
        }

        public void EndCall()
        {
            SignalerEndCall();
            WebRtcEndCall();
            ContactsScreen();
        }

        public void SignalerEndCall()
        {
            if (SaSignaler != null && RemoteUser != null && !string.IsNullOrEmpty(_sessionId))
            {
                SaSignaler.StopCall(RemoteUser.name, _sessionId);
            }
        }

        public void Logout()
        {
            SaSignaler.Logout();
        }

        // Signaler Ingoing Events

        public void OnCallAccepted()
        {
            // The OnField starts the stream once RemoteExpert notified that it is ready it receive it trough accepting the call (after a little delay)
            if (SaSignaler.Me.role != SAUserRole.RemoteExpert)
            {
                ConfigureSender();
            }

            // The RemoteExpert does not need to do anything as the stream will be started automatically once both Sender and Receiver are configured and OnField is the one who completes the configuration last
        }

        public void OnCallRejected()
        {
            WebRtcEndCall();
        }

        public void OnCallRequestStopped()
        {
            WebRtcEndCall();
        }

        public void OnRemoteUserDisconnectedWhileCalling()
        {
            WebRtcEndCall();
        }

        public void OnSignalerDisconnectedWhileCalling()
        {
            WebRtcEndCall();
        }

        // WebRtc Configuration

        private void ConfigureCall()
        {
            NetworkConfig netConf = new NetworkConfig {SignalingUrl = SaSignaler.Environment.CallSignalerUrl};
            netConf.IceServers.Add(new IceServer("stun:stun.l.google.com:19302"));

            Call = UnityCallFactory.Instance.Create(netConf);
            if (Call == null)
            {
                //this might happen if our configuration is invalid e.g. broken stun server url
                //(it won't notice if the stun server is offline though)
                Debug.LogError("Call init failed");
                return;
            }

            Call.CallEvent += Call_CallEvent;

            MediaConfig mediaConfig = new MediaConfig();
            if (IsSender)
            {
                //sender is sending audio and video
                mediaConfig.Audio = true;
                mediaConfig.Video = true;
                mediaConfig.IdealWidth = FrameWidth;
                mediaConfig.IdealHeight = FrameHeight;
            }
            else
            {
                mediaConfig.Audio = true;
                mediaConfig.Video = false;
            }

            if (TryI420)
            {
                mediaConfig.Format = FramePixelFormat.I420p;
            }

            Call.Configure(mediaConfig);
            CurrentCallState = CallState.Config;
        }

        public void ConfigureSender()
        {
            if (!string.IsNullOrEmpty(_signalerChannel))
            {
                // Add a little delay to avoid synchronous rejoins between Sender and Receiver
                _actualRejoinTime = RejoinTime + 0.3f;
                AutoRejoin = true;
                IsSender = true;
                ConfigureCall();
            }
            else
            {
                DispatchError(ErrorConfiguringSignalerChannel.Value);
            }
        }

        public void ConfigureReceiver()
        {
            if (!string.IsNullOrEmpty(_signalerChannel))
            {
                _actualRejoinTime = RejoinTime;
                AutoRejoin = true;
                IsSender = false;
                ConfigureCall();
            }
            else
            {
                DispatchError(ErrorConfiguringSignalerChannel.Value);
            }
        }

        private void InternalResetCall()
        {
            CleanupWebRtcCall();
            if (AutoRejoin)
                StartCoroutine(_rejoinCoroutine = RejoinRoutine());
        }

        private void InternalJoin()
        {
            if (_callActive)
            {
                DispatchError(JoinCallFailedCallIsAlreadyStillActive.Value);
                return;
            }

            _callActive = true;
            Call.Listen(_signalerChannel);
        }

        private IEnumerator RejoinRoutine()
        {
            yield return new WaitForSecondsRealtime(_actualRejoinTime);
            ConfigureCall();
            InternalJoin();
        }

        // WebRtc Controls

        private void WebRtcStartCall()
        {
            if (IsSender)
            {
                Call.Call(_signalerChannel);
            }
            else
            {
                Call.Listen(_signalerChannel);
            }

            CurrentCallState = CallState.Calling;
        }

        public void WebRtcEndCall()
        {
            AutoRejoin = false;
            if (_rejoinCoroutine != null)
            {
                StopCoroutine(_rejoinCoroutine);
            }

            CleanupWebRtcCall();
        }

        public void Send(string msg)
        {
            if (Call != null)
            {
                Call.Send(msg);
            }
        }

        // App Events Dispatchers   

        public void DispatchNotification(string message)
        {
            if (NotificationEvent != null)
            {
                NotificationEvent(this, message);
            }
        }

        public void DispatchError(string message)
        {
            if (ErrorEvent != null)
            {
                ErrorEvent(this, message);
            }
        }

        // Memory Cleanup

        private void CleanupSignaler()
        {
            if (SaSignaler == null) return;
            SaSignaler.SignalerEvent -= SaSignaler_SignalerEvent;
            SaSignaler.Dispose();
            SaSignaler = null;
        }

        private void CleanupWebRtcCall()
        {
            _callActive = false;
            RemoteConnectionId = ConnectionId.INVALID;

            if (Call != null)
            {
                Call.CallEvent -= Call_CallEvent;
                Call.Dispose();
                Call = null;
                // call the garbage collector. This isn't needed but helps discovering memory bugs early on.
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        // WebRtc Events Handler

        private void Call_CallEvent(object sender, CallEventArgs args)
        {
//            if (args.Type != CallEventType.Message && args.Type != CallEventType.FrameUpdate)
//            {
//                DebugMessage("WebRtc: " + args);
//            }

            switch (args.Type)
            {
                case CallEventType.ConfigurationComplete:

                    // Handled by:
                    // CallLogBehavior
                    WebRtcStartCall();
                    break;

                case CallEventType.ConfigurationFailed:
                    DispatchError(AccessingAudioVideoFailed.Value);
                    InternalResetCall();

                    // Handled by:
                    // CallLogBehavior
                    break;

                case CallEventType.ConnectionFailed:
                    DispatchError(ConnectionFailed.Value);
                    InternalResetCall();

                    // Handled by:
                    // CallLogBehavior
                    break;

                case CallEventType.ListeningFailed:
                    // Listening for incoming connections failed usually means a user is using the string / room name already to wait for incoming calls
                    DispatchError(ListeningFailed.Value);
                    InternalResetCall();

                    // Handled by:
                    // CallLogBehavior
                    break;

                case CallEventType.CallAccepted:
                    CurrentCallState = CallState.InCall;
                    RemoteConnectionId = ((CallAcceptedEventArgs) args).ConnectionId;
                    // " audio:" + Call.HasAudioTrack(RemoteConnectionId)
                    // " video:" + Call.HasVideoTrack(RemoteConnectionId));

                    // Handled by:
                    // CallLogBehavior
                    // UiControllerIncomingCallScreen:
                    // UiControllerStartCallScreen:
                    // UiControllerCallScreen
                    // ArController  
                    break;

                case CallEventType.CallEnded:
                    InternalResetCall();
                    CurrentCallState = CallState.Ended;
                    Debug.Log(CallEnded.Value);

                    // Handled by:
                    // CallLogBehavior
                    // UiControllerCallScreen
                    // ArController    
                    break;

                case CallEventType.Message:
                    // Handled by:
                    // CallLogBehavior
                    // UiControllerCallScreen
                    // ArController
                    break;

                case CallEventType.WaitForIncomingCall:
                    // Handled by:
                    // CallLogBehavior
                    break;

                case CallEventType.FrameUpdate:
                    // Handled by:
                    // ArController
                    // CallLogBehavior
                    break;
            }

            if (args.ToString().Equals("Byn.Media.ErrorEventArgs"))
            {
                if (_callActive)
                {
                    InternalResetCall();
                }
            }

            if (WebRtcCallEvent != null)
            {
                WebRtcCallEvent(this, Call, args);
            }
        }

        // Signaler Events Handler

        private void SaSignaler_SignalerEvent(object sender, SASignalerEventArgs args)
        {
//            DebugMessage("Signaler: " + args.EventType);

            switch (args.EventType)
            {
                case SASignalerEventType.Error:
                    DispatchError(args.Message);
                    break;
                case SASignalerEventType.LoginSuccess:
                    // Handled by:
                    // UiControllerLoginScreen
                    break;
                case SASignalerEventType.LoginError:
                    // Handled by:
                    // UiControllerLoginScreen
                    break;
                case SASignalerEventType.CallAccepted:
                    // Handled by:
                    // UiControllerStartCallScreen
                    break;
                case SASignalerEventType.CallRejected:
                    // Handled by:
                    // UiControllerStartCallScreen
                    break;
                case SASignalerEventType.CallRequestReceived:
                    if (!_callActive)
                    {
                        RemoteUser = args.User;
                        _sessionId = args.Message;
                        _signalerChannel = _sessionId;
                        _uiController.OpenIncomingCallScreen();
                    }

                    break;
                case SASignalerEventType.CallStop:
                    WebRtcEndCall();

                    // Handled by:
                    // CallScreen
                    break;
                case SASignalerEventType.CallStopRequestReceived:
                    // Handled by:
                    // UiControllerIncomingCallScreen
                    break;
                case SASignalerEventType.UserListUpdated:
                    // Handled by:
                    // UserListBehavior
                    break;
                case SASignalerEventType.UserConnected:
                    // Handled by:
                    // UserListBehavior
                    break;
                case SASignalerEventType.UserDisconnected:
                    // Handled by:
                    // UserListBehavior
                    break;
                case SASignalerEventType.SocketReconnectionFailed:
                    WebRtcEndCall();
                    SaSignaler.Logout();
                    _uiController.OpenDisconnectedFromServerScreen();
                    break;
                case SASignalerEventType.LogoutSuccess:
                    LoginScreen();
                    break;
                case SASignalerEventType.SignalerDisconnected:
                    // Handled by:
                    // UiControllerStartCallScreen
                    // UiControllerIncomingCallScreen
                    break;
                case SASignalerEventType.LogoutError:
                    DispatchError(args.Message);
                    break;
            }

            if (SignalerCallEvent != null)
            {
                SignalerCallEvent(this, args);
            }
        }

        // ConnectionChecker Event Handler

        private void OnConnectionChecked(object sender, ConnectionChecker.Status status, float speedMbps, float fitness)
        {
            if (Math.Abs(fitness) < 0.001f)
            {
                _networkFailuresCount++;
                if (!_callActive && !_uiController.LoginScreen.activeInHierarchy && !_uiController.DisconnectedFromServerScreen.activeInHierarchy)
                {
                    if (_networkFailuresCount > NetworkFailuresBeforeLogout)
                    {
                        WebRtcEndCall();
                        _uiController.OpenDisconnectedFromServerScreen();
                    }
                }
            }
            else
            {
                _networkFailuresCount = 0;
            }
        }

//        private string messagesLog = "";
//
//        public void DebugMessage(string message)
//        {
//            messagesLog = message + "\n" + messagesLog;
//        }

//        private void OnGUI()
//        {
//            GUI.skin.textField.fontSize = 35;
//            GUI.skin.button.fontSize = 35;
//            GUI.skin.label.fontSize = 35;
//            GUILayout.TextField(messagesLog);
////            GUILayout.TextField("localimage rect" + LocalImage.rectTransform.rect);
////            GUILayout.TextField("localimage screen space rect" + RectTransformToScreenSpace(LocalImage.rectTransform).ToString());
////            GUI.Label(new Rect(20, 20, Screen.width, Screen.height * 0.25f), mStatusText);
////            if (GUILayout.Button("RemoveLastWidget", GUILayout.Width(150), GUILayout.Height(50)))
////            {
////                RemoveLastWidget();
////            }
//        }
    }
}