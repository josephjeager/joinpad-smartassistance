﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Text.RegularExpressions;

namespace BrainPad.SmartAssistance.Scripts.Models.Messages
{
    [Serializable]
    [SuppressMessage("ReSharper", "SuggestVarOrType_BuiltInTypes")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_Elsewhere")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "InvertIf")]
    public class PoorConnectionWarning
    {
        public const string PrefixTag = "PoorConnectionWarning:";
        private const string Separator = "###___###";

        public readonly float Fitness;
        public readonly float SpeedMbps;

        public PoorConnectionWarning(string message)
        {
            string messageCommand = message.Substring(PrefixTag.Length, message.Length - PrefixTag.Length);
            string[] messageSplit = Regex.Split(messageCommand, Separator);
            if (messageSplit.Length == 2)
            {
                Fitness = Convert.ToSingle(messageSplit[0]);
                SpeedMbps = Convert.ToSingle(messageSplit[1]);
            }
        }

        public PoorConnectionWarning(float fitness, float speedMbps)
        {
            Fitness = fitness;
            SpeedMbps = speedMbps;
        }

        public string GetMessage()
        {
            return PrefixTag + Fitness + Separator + SpeedMbps;
        }
    }
}