﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Text.RegularExpressions;

namespace BrainPad.SmartAssistance.Scripts.Models.Messages
{
    [Serializable]
    [SuppressMessage("ReSharper", "SuggestVarOrType_BuiltInTypes")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_Elsewhere")]
    public class ApplicationPaused
    {
        public const string PrefixTag = "ApplicationPaused:";
        private const string Separator = "###___###";

        public readonly bool IsPaused;

        public ApplicationPaused(string message)
        {
            string messageCommand = message.Substring(PrefixTag.Length, message.Length - PrefixTag.Length);
            string[] messageSplit = Regex.Split(messageCommand, Separator);
            if (messageSplit.Length == 1)
            {
                IsPaused = Convert.ToInt32(messageSplit[0]) != 0;
            }
        }

        public ApplicationPaused(bool isPaused)
        {
            IsPaused = isPaused;
        }

        public string GetMessage()
        {
            return PrefixTag + (IsPaused ? 1 : 0);
        }
    }
}