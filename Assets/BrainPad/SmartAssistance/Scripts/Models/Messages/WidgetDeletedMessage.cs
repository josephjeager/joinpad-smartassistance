﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Text.RegularExpressions;

namespace BrainPad.SmartAssistance.Scripts.Models.Messages
{
    [Serializable]
    [SuppressMessage("ReSharper", "SuggestVarOrType_BuiltInTypes")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_Elsewhere")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    public class WidgetDeletedMessage
    {
        public const string PrefixTag = "WidgetDeletedMessage:";
        private const string Separator = "###___###";

        public readonly int WidgetId;

        public WidgetDeletedMessage(string message)
        {
            string messageCommand = message.Substring(PrefixTag.Length, message.Length - PrefixTag.Length);
            string[] messageSplit = Regex.Split(messageCommand, Separator);
            if (messageSplit.Length == 1)
            {
                WidgetId = Convert.ToInt32(messageSplit[0]);
            }
        }

        public WidgetDeletedMessage(int widgetId)
        {
            WidgetId = widgetId;
        }

        public string GetMessage()
        {
            return PrefixTag + WidgetId;
        }
    }
}