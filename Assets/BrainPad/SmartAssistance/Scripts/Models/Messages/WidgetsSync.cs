﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using System.Text.RegularExpressions;

namespace BrainPad.SmartAssistance.Scripts.Models.Messages
{
    [Serializable]
    [SuppressMessage("ReSharper", "SuggestVarOrType_BuiltInTypes")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_Elsewhere")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_SimpleTypes")]
    public class WidgetsSync
    {
        public const string PrefixTag = "WidgetsSync:";
        private const string Separator = "###___###";

        public readonly List<int> WidgetIds;

        public WidgetsSync(string message)
        {
            WidgetIds = new List<int>();

            string messageCommand = message.Substring(PrefixTag.Length, message.Length - PrefixTag.Length);
            if (messageCommand.Length <= 0) return;

            string[] messageTrackerUpdateSplit = Regex.Split(messageCommand, Separator);
            foreach (string widgetId in messageTrackerUpdateSplit)
            {
                WidgetIds.Add(Convert.ToInt32(widgetId));
            }
        }

        public WidgetsSync(IEnumerable<Widget> widgets)
        {
            WidgetIds = new List<int>();
            foreach (Widget widget in widgets)
            {
                WidgetIds.Add(widget.Id);
            }
        }

        public string GetMessage()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(PrefixTag);
            for (int i = 0; i < WidgetIds.Count; i++)
            {
                int widgetId = WidgetIds[i];
                stringBuilder.Append(widgetId);
                if (i < WidgetIds.Count - 1)
                {
                    stringBuilder.Append(Separator);
                }
            }

            return stringBuilder.ToString();
        }

//        public class TrackerPositionUpdate
//        {
//            public int WidgetId;
//            public float X;
//            public float Y;
//
//            public TrackerPositionUpdate(int widgetId, float x, float y)
//            {
//                WidgetId = widgetId;
//                X = x;
//                Y = y;
//            }
//        }
    }
}