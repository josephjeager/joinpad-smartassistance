﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Text.RegularExpressions;
using BrainPad.SmartAssistance.Scripts.CallScreen.Controllers;
using UnityEngine;
using WebSocketSharpUnityMod;

namespace BrainPad.SmartAssistance.Scripts.Models.Messages
{
    [Serializable]
    [SuppressMessage("ReSharper", "SuggestVarOrType_BuiltInTypes")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_Elsewhere")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "InvertIf")]
    public class TrackerOn
    {
        public readonly float Top;
        public readonly float Left;
        public readonly float Bottom;
        public readonly float Right;
        public readonly float FramePointX;
        public readonly float FramePointY;
        public readonly string WidgetType;
        public readonly Color WidgetColor;
        public readonly int WidgetId;
        public readonly string Owner;
        public readonly string WidgetMessage;
        public readonly Texture2D Texture2D;
        public readonly float UnitySpaceWidth;
        public readonly float UnitySpaceHeight;

        public const string PrefixTag = "TrackerOn:";
        private const string Separator = "###___###";

        public TrackerOn(string message)
        {
            string messageCommand = message.Substring(PrefixTag.Length, message.Length - PrefixTag.Length);
            string[] messageSplit = Regex.Split(messageCommand, Separator);
            if (messageSplit.Length == 16)
            {
                Top = Convert.ToSingle(messageSplit[0]);
                Left = Convert.ToSingle(messageSplit[1]);
                Bottom = Convert.ToSingle(messageSplit[2]);
                Right = Convert.ToSingle(messageSplit[3]);
                FramePointX = Convert.ToSingle(messageSplit[4]);
                FramePointY = Convert.ToSingle(messageSplit[5]);
                WidgetType = messageSplit[6];
                ColorUtility.TryParseHtmlString("#" + messageSplit[7], out WidgetColor);
                WidgetId = Convert.ToInt32(messageSplit[8]);
                Owner = messageSplit[9];
                WidgetMessage = messageSplit[10];
                if (!messageSplit[11].IsNullOrEmpty())
                {
                    Texture2D = new Texture2D(Convert.ToInt32(messageSplit[12]), Convert.ToInt32(messageSplit[13]))
                    {
                        filterMode = FilterMode.Point,
                        wrapMode = TextureWrapMode.Clamp
                    };
                    Texture2D.LoadImage(Convert.FromBase64String(messageSplit[11]));
                }

                UnitySpaceWidth = Convert.ToSingle(messageSplit[14]);
                UnitySpaceHeight = Convert.ToSingle(messageSplit[15]);
            }
        }

        public TrackerOn(float top, float left, float bottom, float right, float framePointX, float framePointY, string widgetType, Color widgetColor, int widgetId, string owner, string widgetMessage, Texture2D texture2D, float unitySpaceWidth, float unitySpaceHeight)
        {
            Top = top;
            Left = left;
            Bottom = bottom;
            Right = right;
            FramePointX = framePointX;
            FramePointY = framePointY;
            WidgetType = widgetType;
            WidgetColor = widgetColor;
            WidgetId = widgetId;
            Owner = owner;
            WidgetMessage = widgetMessage;
            Texture2D = texture2D;
            UnitySpaceWidth = unitySpaceWidth;
            UnitySpaceHeight = unitySpaceHeight;
        }

        public string GetMessage()
        {
            string message = PrefixTag +
                             Top +
                             Separator +
                             Left +
                             Separator +
                             Bottom +
                             Separator +
                             Right +
                             Separator +
                             FramePointX +
                             Separator +
                             FramePointY +
                             Separator +
                             WidgetType +
                             Separator +
                             ColorUtility.ToHtmlStringRGBA(WidgetColor) +
                             Separator +
                             WidgetId +
                             Separator +
                             Owner +
                             Separator +
                             WidgetMessage +
                             Separator;
            if (WidgetType == ArController.WidgetPaint)
            {
                message += Convert.ToBase64String(Texture2D.EncodeToPNG()) +
                           Separator +
                           Texture2D.width +
                           Separator +
                           Texture2D.height +
                           Separator;
            }
            else
            {
                message += "" + Separator + "" + Separator + "" + Separator;
            }

            message += UnitySpaceWidth +
                       Separator +
                       UnitySpaceHeight;
            return message;
        }
    }
}