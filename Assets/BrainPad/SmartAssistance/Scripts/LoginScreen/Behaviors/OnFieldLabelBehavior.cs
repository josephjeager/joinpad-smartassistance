﻿using System.Diagnostics.CodeAnalysis;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace BrainPad.SmartAssistance.Scripts.LoginScreen.Behaviors
{
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_SimpleTypes")]
    public class OnFieldLabelBehavior : MonoBehaviour
    {
        public bool Selected;
        public Color32 LabelSelectedTextColor = new Color32(255, 255, 255, 255);
        public Color32 LabelDeselectedTextColor = new Color32(80, 93, 118, 255);
        public Color32 PanelSelectedColor = new Color32(56, 65, 82, 255);
        public Image PanelBackground;
        public Image Underline;

        public void Select()
        {
            Selected = true;
            transform.GetChild(0).GetComponent<TextMeshProUGUI>().color = LabelSelectedTextColor;
            PanelBackground.color = PanelSelectedColor;
            if (Underline != null)
            {
                Underline.color = LabelSelectedTextColor;
            }
        }

        public void Deselect()
        {
            Selected = false;
            transform.GetChild(0).GetComponent<TextMeshProUGUI>().color = LabelDeselectedTextColor;
            if (Underline != null)
            {
                Underline.color = LabelDeselectedTextColor;
            }
        }
    }
}