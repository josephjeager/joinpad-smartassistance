﻿using System.Collections;
using System.Diagnostics.CodeAnalysis;
using BrainPad.SmartAssistance.Scripts.Core.Http;
using BrainPad.SmartAssistance.Scripts.LoginScreen.Behaviors;
using GameToolkit.Localization;
using SmartAssistanceSDK;
using SmartAssistanceSDK.Delegates;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace BrainPad.SmartAssistance.Scripts.LoginScreen.Controllers
{
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_SimpleTypes")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_BuiltInTypes")]
    [SuppressMessage("ReSharper", "ConvertIfStatementToConditionalTernaryExpression")]
    [SuppressMessage("ReSharper", "SwitchStatementMissingSomeCases")]
    [SuppressMessage("ReSharper", "MemberCanBeMadeStatic.Local")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "InvertIf")]
    [SuppressMessage("ReSharper", "PossibleNullReferenceException")]
    public class UiControllerLoginScreen : MonoBehaviour
    {
        public GameObject SupportLabel;
        public GameObject OnFieldLabel;
        public GameObject Username;
        public GameObject Password;
        public GameObject ShowPasswordButton;
        public GameObject HidePasswordButton;
        public Toggle RememberCredentialsCheck;
        public GameObject LowConnectionWarning;
        public GameObject LoginButton;
        public GameObject ConnectingLabel;

        public float MinFitnessForLogin = 0.3f;

        [Header("Errors")] public LocalizedText TimeoutErrorCheckYourConnection;
        public LocalizedText InvelidUsernamePasswordOrRole;
        
        private IEnumerator _resetLoginRoutine;
        private float _startTime;
        private const int LoginTimeout = 10;

        private void OnEnable()
        {
            ConnectingLabel.SetActive(false);
            LoginButton.SetActive(true);

            // In case any connection is still appended
            if (SmartAssistanceManager.Instance != null)
            {
                SmartAssistanceManager.Instance.WebRtcEndCall();
            }

            bool rememberCredentials = PlayerPrefs.GetInt("RememberCredentials") == 1;
            if (rememberCredentials)
            {
                Username.GetComponent<TMP_InputField>().text = PlayerPrefs.GetString("Username");
//                Password.GetComponent<TMP_InputField>().text = PlayerPrefs.GetString("Password");

                if (PlayerPrefs.GetString("Role").Equals("OnField"))
                {
                    SelectOnField();
                }
                else
                {
                    SelectSupport();
                }

                RememberCredentialsCheck.isOn = true;
            }
            else
            {
                SelectOnField();
                RememberCredentialsCheck.isOn = false;
            }

            Password.GetComponent<TMP_InputField>().text = "";

            ConnectionChecker.Instance.ConnectionCheckedEvent += OnConnectionChecked;
            SmartAssistanceManager.Instance.ErrorEvent += OnErrorEvent;
            SmartAssistanceManager.Instance.SignalerCallEvent += OnSignalerCallEvent;
            CheckEnableLogin(ConnectionChecker.Instance.CurrentFitness);
        }

        private void OnDisable()
        {
            if (ConnectionChecker.Instance != null)
            {
                ConnectionChecker.Instance.ConnectionCheckedEvent -= OnConnectionChecked;
            }

            if (SmartAssistanceManager.Instance != null)
            {
                SmartAssistanceManager.Instance.SignalerCallEvent -= OnSignalerCallEvent;
                SmartAssistanceManager.Instance.ErrorEvent -= OnErrorEvent;
            }

            if (_resetLoginRoutine != null)
            {
                StopCoroutine(_resetLoginRoutine);
            }
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }
        }

        private void OnApplicationPause(bool pauseStatus)
        {
            if (!pauseStatus)
            {
                if (_startTime > 0 && _startTime + LoginTimeout > Time.time)
                {
                    SmartAssistanceManager.Instance.DispatchError(TimeoutErrorCheckYourConnection.Value);
                    _startTime = 0;
                }
            }
        }

        public void Login()
        {
            ConnectingLabel.SetActive(true);
            LoginButton.SetActive(false);

            string username = Username.GetComponent<TMP_InputField>().text;
            string password = Password.GetComponent<TMP_InputField>().text;
            string role;
            SAUserRole saUserRole;
            if (OnFieldLabel.GetComponent<OnFieldLabelBehavior>().Selected)
            {
                role = "OnField";
                saUserRole = SAUserRole.UserOnField;
            }
            else
            {
                role = "RemoteExpert";
                saUserRole = SAUserRole.RemoteExpert;
            }

            if (RememberCredentialsCheck.isOn)
            {
                PlayerPrefs.SetInt("RememberCredentials", 1);
                PlayerPrefs.SetString("Username", username);
//                PlayerPrefs.SetString("Password", password);
                PlayerPrefs.SetString("Role", role);
                PlayerPrefs.Save();
            }
            else
            {
                PlayerPrefs.SetInt("RememberCredentials", 0);
                PlayerPrefs.SetString("Username", "");
//                PlayerPrefs.SetString("Password", "");
                PlayerPrefs.SetString("Role", "");
                PlayerPrefs.Save();
            }

            SmartAssistanceManager.Instance.IsLoggingIn = true;

            SmartAssistanceManager.Instance.Login(saUserRole, username, password);
            StartCoroutine(_resetLoginRoutine = ResetLoginRoutine());
        }

        private IEnumerator ResetLoginRoutine()
        {
            _startTime = Time.time;
            yield return new WaitForSeconds(LoginTimeout);
            SmartAssistanceManager.Instance.DispatchError(TimeoutErrorCheckYourConnection.Value);
        }

        public void ShowPassword()
        {
            ShowPasswordButton.SetActive(false);
            HidePasswordButton.SetActive(true);
            Password.GetComponent<TMP_InputField>().contentType = TMP_InputField.ContentType.Standard;
            Password.GetComponent<TMP_InputField>().ForceLabelUpdate();
        }

        public void HidePassword()
        {
            ShowPasswordButton.SetActive(true);
            HidePasswordButton.SetActive(false);
            Password.GetComponent<TMP_InputField>().contentType = TMP_InputField.ContentType.Password;
            Password.GetComponent<TMP_InputField>().ForceLabelUpdate();
        }

        public void SelectSupport()
        {
            SupportLabel.GetComponent<SupportLabelBehavior>().Select();
            OnFieldLabel.GetComponent<OnFieldLabelBehavior>().Deselect();
        }

        public void SelectOnField()
        {
            SupportLabel.GetComponent<SupportLabelBehavior>().Deselect();
            OnFieldLabel.GetComponent<OnFieldLabelBehavior>().Select();
        }

        public void CheckEnableLogin(float fitness)
        {
            if (fitness > MinFitnessForLogin)
            {
                LowConnectionWarning.SetActive(false);
                LoginButton.GetComponent<Button>().interactable = true;
            }
            else
            {
                LowConnectionWarning.SetActive(true);
                LoginButton.GetComponent<Button>().interactable = false;
            }
        }

        private void OnConnectionChecked(object sender, ConnectionChecker.Status status, float speedMbps, float fitness)
        {
            CheckEnableLogin(fitness);
        }

        private void OnErrorEvent(object sender, string message)
        {
            if (_resetLoginRoutine != null)
            {
                StopCoroutine(_resetLoginRoutine);
            }

            ConnectingLabel.SetActive(false);
            LoginButton.SetActive(true);
        }

        private void OnSignalerCallEvent(object sender, SASignalerEventArgs args)
        {
            switch (args.EventType)
            {
                case SASignalerEventType.LoginSuccess:
                    if (SmartAssistanceManager.Instance.IsLoggingIn)
                    {
                        if (SmartAssistanceManager.Instance.DeviceType == SADeviceType.Tablet)
                        {
                            SmartAssistanceManager.Instance.ContactsScreen();
                        }
                        else
                        {
                            bool hideInstructionsOnStartup = PlayerPrefs.GetInt("HideInstructionsOnStartup") == 1;
                            if (hideInstructionsOnStartup)
                            {
                                SmartAssistanceManager.Instance.ContactsScreen();
                            }
                            else
                            {
                                SmartAssistanceManager.Instance.InstructionsScreen();
                            }
                        }
                    }

                    break;
                case SASignalerEventType.LoginError:
                    if (args.Message.Equals("Unauthorized"))
                    {
                        SmartAssistanceManager.Instance.DispatchError(InvelidUsernamePasswordOrRole.Value);
                    }
                    else
                    {
                        SmartAssistanceManager.Instance.DispatchError(args.Message);
                    }

                    break;
            }
        }
    }
}