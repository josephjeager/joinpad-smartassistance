﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using SmartAssistanceSDK.Delegates;
using SmartAssistanceSDK.Models;
using TMPro;
using UnityEngine;

namespace BrainPad.SmartAssistance.Scripts.ContactsScreen.Tablet.Behaviors
{
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_SimpleTypes")]
    [SuppressMessage("ReSharper", "InvertIf")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "UnusedParameter.Local")]
    [SuppressMessage("ReSharper", "SwitchStatementMissingSomeCases")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_Elsewhere")]
    [SuppressMessage("ReSharper", "ConvertIfStatementToConditionalTernaryExpression")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_BuiltInTypes")]
    public class UserGridBehavior : MonoBehaviour
    {
        public GameObject ItemsContainer;
        public GameObject SectionTitlePrefab;
        public GameObject GridRowPrefab;
        public GameObject UserItemPrefab;
        public TMP_InputField SearchInputField;

        public GameObject ContactsLabel;
        public GameObject SearchLabel;
        public GameObject NoResultsFoundWarning;

        public bool FilterClones = true;
        public bool AutoRefresh = true;
        public int AutoRefreshDelay = 5;

        private string _searchQuery;

        private List<SAUser> _users = new List<SAUser>();
        private List<SAUser> _filtredUsers = new List<SAUser>();
        private ILookup<string, SAUser> _usersByLocation;

        private bool _refresh;
        private IEnumerator _refreshCoroutine;

        private void OnEnable()
        {
            SmartAssistanceManager.Instance.SignalerCallEvent += OnSignalerCallEvent;
            ResetFilter();
            if (AutoRefresh)
            {
                _refresh = true;
                StartCoroutine(_refreshCoroutine = RefreshUsersRoutine());
            }
            else
            {
                SetUsers(this, SmartAssistanceManager.Instance.SaSignaler.ConnectedUsers);
            }
        }

        private void OnDisable()
        {
            if (SmartAssistanceManager.Instance != null)
            {
                SmartAssistanceManager.Instance.SignalerCallEvent -= OnSignalerCallEvent;
            }

            if (AutoRefresh)
            {
                _refresh = false;
                if (_refreshCoroutine != null)
                {
                    StopCoroutine(_refreshCoroutine);
                }
            }
        }

        public void Filter(string query)
        {
            if (string.IsNullOrEmpty(query))
            {
                ContactsLabel.SetActive(true);
                SearchLabel.SetActive(false);
            }
            else
            {
                ContactsLabel.SetActive(false);
                SearchLabel.SetActive(true);
            }

            _searchQuery = query;
            ApplyFilter();
            RefreshUsers();
        }

        public void ResetFilter()
        {
            SearchInputField.text = "";
            Filter("");
        }

        private class DistinctSaUserComparer : IEqualityComparer<SAUser>
        {
            public bool Equals(SAUser x, SAUser y)
            {
                return y != null && x != null && x.username == y.username;
            }

            public int GetHashCode(SAUser obj)
            {
                return obj.username.GetHashCode();
            }
        }

        private void ApplyFilter()
        {
            if (_users == null) return;

            if (FilterClones)
            {
                _filtredUsers = _users.Distinct(new DistinctSaUserComparer()).ToList();
            }
            else
            {
                _filtredUsers = _users;
            }

            _filtredUsers = _filtredUsers.FindAll(user =>
                user.name.ToLower().Contains(_searchQuery.ToLower()) ||
                user.lastname.ToLower().Contains(_searchQuery.ToLower()) ||
                user.location.ToLower().Contains(_searchQuery.ToLower()));

            _usersByLocation = _filtredUsers.ToLookup(u => u.location, p => p);
        }

        private void SetUsers(object sender, List<SAUser> users)
        {
            _users = users;
            ApplyFilter();
            RefreshUsers();
        }

        private void RefreshUsers()
        {
            foreach (Transform child in ItemsContainer.transform)
            {
                Destroy(child.gameObject);
            }

            foreach (IGrouping<string, SAUser> groupItem in _usersByLocation)
            {
                var groupKey = groupItem.Key;
                GameObject sectionTitle = Instantiate(SectionTitlePrefab, ItemsContainer.transform);
                sectionTitle.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = groupKey;
                sectionTitle.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "(" + groupItem.Count() + ")";

                GameObject currentRow = null;
                int i = 0;
                foreach (SAUser user in groupItem)
                {
                    if (i % 4 == 0 || currentRow == null)
                    {
                        currentRow = Instantiate(GridRowPrefab, ItemsContainer.transform);
                    }
                    GameObject userItem = Instantiate(UserItemPrefab, currentRow.transform);
                    UserGridItemBehavior userGridItemBehavior = userItem.GetComponent<UserGridItemBehavior>();
                    userGridItemBehavior.SetUser(user);
                    userGridItemBehavior.SetUserGridBehavior(this);
                    i++;
                }
            }

            if (_usersByLocation.Count > 0 || string.IsNullOrEmpty(_searchQuery))
            {
                NoResultsFoundWarning.SetActive(false);
            }
            else
            {
                NoResultsFoundWarning.SetActive(true);
            }
        }

        private void OnSignalerCallEvent(object sender, SASignalerEventArgs args)
        {
            switch (args.EventType)
            {
                case SASignalerEventType.UserListUpdated:
                case SASignalerEventType.UserConnected:
                case SASignalerEventType.UserDisconnected:
                    SetUsers(sender, SmartAssistanceManager.Instance.SaSignaler.ConnectedUsers);
                    break;
            }
        }

        private IEnumerator RefreshUsersRoutine()
        {
            while (_refresh)
            {
                SetUsers(this, SmartAssistanceManager.Instance.SaSignaler.ConnectedUsers);
                yield return new WaitForSeconds(AutoRefreshDelay);
            }
        }
    }
}