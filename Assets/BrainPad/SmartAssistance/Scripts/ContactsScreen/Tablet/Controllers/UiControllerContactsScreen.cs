﻿using System.Diagnostics.CodeAnalysis;
using BrainPad.SmartAssistance.Scripts.Core.Http;
using GameToolkit.Localization;
using SmartAssistanceSDK;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace BrainPad.SmartAssistance.Scripts.ContactsScreen.Tablet.Controllers
{
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "InvertIf")]
    [SuppressMessage("ReSharper", "MemberCanBeMadeStatic.Global")]
    public class UiControllerContactsScreen : MonoBehaviour
    {
        public Image ProfilePic;
        public TextMeshProUGUI UserNameLabel;
        public TextMeshProUGUI RoleLabel;
        public TextMeshProUGUI NoProfilePicCapitalLetter;
        
        public GameObject ContactsButton;
        public GameObject InstructionsButton;
        
        public GameObject ContactsTab;
        public GameObject InstructionsTab;

        [Header("Labels")] public LocalizedText OnField;
        public LocalizedText Support;

        private void OnEnable()
        {
            UserNameLabel.text = SmartAssistanceManager.Instance.SaSignaler.Me.name + " " + SmartAssistanceManager.Instance.SaSignaler.Me.lastname;
            RoleLabel.text = SmartAssistanceManager.Instance.SaSignaler.Me.role == SAUserRole.UserOnField ? OnField : Support;

            if (SmartAssistanceManager.Instance.SaSignaler.Me.name.Length > 0)
            {
                NoProfilePicCapitalLetter.text = SmartAssistanceManager.Instance.SaSignaler.Me.name[0].ToString().ToUpper();
            }

            ProfilePicturesLoader.Instance.LoadProfilePic(SmartAssistanceManager.Instance.SaSignaler.Me, ProfilePic.mainTexture.width, ProfilePic.mainTexture.height, ((Texture2D) ProfilePic.mainTexture).format, sprite =>
            {
                if (sprite != null)
                {
                    ProfilePic.sprite = sprite;
                    ProfilePic.gameObject.SetActive(true);
                }
            });

            if (!ContactsTab.activeInHierarchy && !InstructionsTab.activeInHierarchy)
            {
                SelectContacts();
            }
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Logout();
            }
        }
        
        public void SelectContacts()
        {
            CleanSelection();
            ContactsTab.SetActive(true);
            ContactsButton.GetComponent<Image>().enabled = true;
        }

        public void SelectInstructions()
        {
            CleanSelection();
            InstructionsTab.SetActive(true);
            InstructionsButton.GetComponent<Image>().enabled = true;
        }

        public void Logout()
        {
           SmartAssistanceManager.Instance.ShowLogout();
        }
        
        private void CleanSelection()
        {
            ContactsTab.SetActive(false);
            ContactsButton.GetComponent<Image>().enabled = false;
            InstructionsTab.SetActive(false);
            InstructionsButton.GetComponent<Image>().enabled = false;
        }
        
        public void TutorialStartACall()
        {
            SmartAssistanceManager.Instance.TutorialStartACallScreen();
        }

        public void TutorialArWidgets()
        {
            SmartAssistanceManager.Instance.TutorialArWidgetsScreen();
        }

        public void TutorialDrawWidgets()
        {
            SmartAssistanceManager.Instance.TutorialDrawWidgetsScreen();
        }

        public void TutorialRemoveWidgets()
        {
            SmartAssistanceManager.Instance.TutorialRemoveWidgetsScreen();
        }
    }
}