﻿using System.Diagnostics.CodeAnalysis;
using GameToolkit.Localization;
using SmartAssistanceSDK;
using TMPro;
using UnityEngine;

namespace BrainPad.SmartAssistance.Scripts.ContactsScreen.Smartphone.Controllers
{
    [SuppressMessage("ReSharper", "MemberCanBeMadeStatic.Global")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    public class UiControllerContactsScreen : MonoBehaviour
    {
        public TextMeshProUGUI UserAndRoleLabel;

        [Header("Labels")] public LocalizedText OnField;
        public LocalizedText Support;
        
        private void OnEnable()
        {
            UserAndRoleLabel.text = SmartAssistanceManager.Instance.SaSignaler.Me.name + " " + SmartAssistanceManager.Instance.SaSignaler.Me.lastname + " | " + (SmartAssistanceManager.Instance.SaSignaler.Me.role == SAUserRole.UserOnField ? OnField.Value : Support.Value);
        }
        
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Logout();
            }
        }

        public void Logout()
        {
            SmartAssistanceManager.Instance.ShowLogout();
        }
    }
}