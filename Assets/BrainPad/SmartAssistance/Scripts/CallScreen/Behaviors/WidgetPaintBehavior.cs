using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using BrainPad.SmartAssistance.Scripts.CallScreen.Controllers;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BrainPad.SmartAssistance.Scripts.CallScreen.Behaviors
{
    [SuppressMessage("ReSharper", "FieldCanBeMadeReadOnly.Global")]
    [SuppressMessage("ReSharper", "ConvertToConstant.Global")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_BuiltInTypes")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_Elsewhere")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_SimpleTypes")]
    [SuppressMessage("ReSharper", "MemberCanBeMadeStatic.Local")]
    public class WidgetPaintBehavior : MonoBehaviour
    {
        public UiControllerCallScreen UiController;
        public ArController ArController;

        public static int TextureScale = 3; // Needed for optimization, greater number means faster draw (and worst paint quality)
        private int _penWidth = 9; // This will be scaled according to _textureScale and screen pixels density

        private readonly Color32 _resetColor = new Color32(0, 0, 0, 0);

        private Sprite _drawableSprite;
        private Texture2D _drawableTexture;
        private Vector2 _previousDragPosition;

        private Color[] _cleanColorsArray;
        private Color32[] _curColors;
        private bool _mouseWasPreviouslyHeldDown;
        private bool _noDrawingOnCurrentDrag;

        private float _xMin;
        private float _xMax;
        private float _yMin;
        private float _yMax;

        private bool _isDrawing;

        public float WidgetAddDelay = 0.5f;
        private bool _addEnable;

        private void Start()
        {
            _penWidth = Mathf.RoundToInt((float) _penWidth / TextureScale * ArController.UnitySpaceToScreenSpaceRatio);
            if (_penWidth < 1)
            {
                _penWidth = 1;
            }

            ResetCanvas();
        }

        private void OnEnable()
        {
            _addEnable = true;
        }

        private void Update()
        {
            if (_addEnable)
            {
                bool mouseHeldDown = Input.GetMouseButton(0);
                if (mouseHeldDown && !_noDrawingOnCurrentDrag)
                {
//              If touch did hit drawing canvas and if it is the first UI object clicked
                    List<RaycastResult> results = new List<RaycastResult>();
                    if (RaycastUi(results) && results[0].gameObject.name == gameObject.name)
                    {
                        if (!_isDrawing)
                        {
                            _isDrawing = true;
                        }

                        // We're over the texture we're draw
                        ChangeColorAtPoint(Input.mousePosition);
                    }
                    else
                    {
                        // We're not over our destination texture
                        _previousDragPosition = Vector2.zero;
                        if (!_mouseWasPreviouslyHeldDown)
                        {
                            // This is a new drag where the user is left clicking off the canvas
                            // Ensure no drawing happens until a new drag is started
                            _noDrawingOnCurrentDrag = true;
                        }
                    }
                }
                else if (!mouseHeldDown) // Mouse is released
                {
                    _previousDragPosition = Vector2.zero;
                    _noDrawingOnCurrentDrag = false;

                    if (_isDrawing)
                    {
                        _isDrawing = false;

                        Texture2D paint = CutoutPaint();
                        CreateWidget(paint);
                        ResetCanvas();
                    }
                }

                _mouseWasPreviouslyHeldDown = mouseHeldDown;
            }
            else
            {
                _mouseWasPreviouslyHeldDown = false;
            }
        }

        public void StartPaint()
        {
            gameObject.SetActive(true);
        }

        public void StopPaint()
        {
            gameObject.SetActive(false);
        }

        // Changes every pixel to be the reset color
        private void ResetCanvas()
        {
            // Correct way to get actual width and height for this scenario
            RectTransform parentRectTransform = GetComponentInParent<RectTransform>();
            Canvas parentCanvas = GetComponentInParent<Canvas>();
            int surfaceActualWidth = (int) (parentRectTransform.rect.width * parentCanvas.scaleFactor);
            int surfaceActualHeight = (int) parentCanvas.pixelRect.height;

            Texture2D original = new Texture2D(surfaceActualWidth / TextureScale, surfaceActualHeight / TextureScale)
            {
                filterMode = FilterMode.Point,
                wrapMode = TextureWrapMode.Clamp
            };

            _drawableSprite = Sprite.Create(original, new Rect(0.0f, 0.0f, original.width, original.height), new Vector2(0.5f, 0.5f), 100);
            GetComponent<Image>().sprite = _drawableSprite;
            _drawableTexture = _drawableSprite.texture;
            _cleanColorsArray = new Color[(int) _drawableSprite.rect.width * (int) _drawableSprite.rect.height];

            for (int x = 0; x < _cleanColorsArray.Length; x++)
            {
                _cleanColorsArray[x] = _resetColor;
            }

            _xMax = 0;
            _yMax = 0;

            _xMin = surfaceActualWidth;
            _yMin = surfaceActualHeight;

            _drawableTexture.SetPixels(_cleanColorsArray);
            _drawableTexture.Apply();
        }

        // Saves a paint using the smallest canvas possible
        private Texture2D CutoutPaint()
        {
            Texture2D cutout = new Texture2D((int) (_xMax - _xMin), (int) (_yMax - _yMin), TextureFormat.ARGB32, true)
            {
                filterMode = FilterMode.Point,
                wrapMode = TextureWrapMode.Clamp
            };
            Graphics.CopyTexture(_drawableTexture, 0, 0, (int) _xMin, (int) _yMin, (int) (_xMax - _xMin), (int) (_yMax - _yMin), cutout, 0, 0, 0, 0);
            cutout.Apply();
            return cutout;
        }

        private void ChangeColorAtPoint(Vector2 coordinates)
        {
            coordinates /= TextureScale;

            _curColors = _drawableTexture.GetPixels32();

            if (Mathf.FloorToInt(coordinates.x - _penWidth) < _xMin)
            {
                _xMin = Mathf.FloorToInt(coordinates.x - _penWidth);
            }

            if (Mathf.CeilToInt(coordinates.x + _penWidth) > _xMax)
            {
                _xMax = Mathf.CeilToInt(coordinates.x + _penWidth);
            }

            if (Mathf.FloorToInt(coordinates.y - _penWidth) < _yMin)
            {
                _yMin = Mathf.FloorToInt(coordinates.y - _penWidth);
            }

            if (Mathf.CeilToInt(coordinates.y + _penWidth) > _yMax)
            {
                _yMax = Mathf.CeilToInt(coordinates.y + _penWidth);
            }

            if (_previousDragPosition == Vector2.zero)
            {
                // If this is the first time we've ever dragged on this image, simply color the pixels at our mouse position
                MarkPixelsToColorRound(coordinates, _penWidth, UiController.SelectedColor);
            }
            else
            {
                // Color in a line from where we were on the last update call
                ColorBetween(_previousDragPosition, coordinates);
            }

            ApplyMarkedPixelChanges();
            _previousDragPosition = coordinates;
        }

        // Set the color of pixels in a straight line from start_point all the way to end_point, to ensure everything inbetween is colored
        private void ColorBetween(Vector2 startPoint, Vector2 endPoint)
        {
            float distance = Vector2.Distance(startPoint, endPoint);
            float lerpSteps = 1 / distance;
            for (float lerp = 0; lerp <= 1; lerp += lerpSteps)
            {
                Vector2 curPosition = Vector2.Lerp(startPoint, endPoint, lerp);
                MarkPixelsToColorRound(curPosition, _penWidth, UiController.SelectedColor);
            }
        }

        // ReSharper disable once UnusedMember.Local
        private void MarkPixelsToColor(Vector2 centerPixel, int penThickness, Color colorOfPen)
        {
            // Figure out how many pixels we need to color in each direction (x and y)
            int centerX = (int) centerPixel.x;
            int centerY = (int) centerPixel.y;
//            int extraRadius = Mathf.Min(0, penThickness - 2);

            for (int x = centerX - penThickness; x <= centerX + penThickness; x++)
            {
                // Check if the X wraps around the image, so we don't draw pixels on the other side of the image
                if (x >= (int) _drawableSprite.rect.width || x < 0)
                {
                    continue;
                }

                for (int y = centerY - penThickness; y <= centerY + penThickness; y++)
                {
                    MarkPixelToChange(x, y, colorOfPen);
                }
            }
        }

        private void MarkPixelsToColorRound(Vector2 centerPixel, int penThickness, Color colorOfPen)
        {
            int centerX = (int) centerPixel.x;
            int centerY = (int) centerPixel.y;
            for (int y = -penThickness; y <= penThickness; y++)
            {
                for (int x = -penThickness; x <= penThickness; x++)
                {
                    if (x * x + y * y <= penThickness * penThickness)
                    {
                        MarkPixelToChange(centerX + x, centerY + y, colorOfPen);
                    }
                }
            }
        }

        private void MarkPixelToChange(int x, int y, Color color)
        {
            // Need to transform x and y coordinates to flat coordinates of array
            int arrayPos = y * (int) _drawableSprite.rect.width + x;

            // Check if this is a valid position
            if (arrayPos > _curColors.Length || arrayPos < 0)
            {
                return;
            }

            _curColors[arrayPos] = color;
        }

        private void ApplyMarkedPixelChanges()
        {
            _drawableTexture.SetPixels32(_curColors);
            _drawableTexture.Apply();
        }

//        // Directly colors pixels. This method is slower than using MarkPixelsToColor then using ApplyMarkedPixelChanges
//        // SetPixels32 is far faster than SetPixel
//        // Colors both the center pixel, and a number of pixels around the center pixel based on pen_thickness (pen radius)
//        public void ColorPixels(Vector2 center_pixel, int pen_thickness, Color color_of_pen)
//        {
//            // Figure out how many pixels we need to color in each direction (x and y)
//            int center_x = (int) center_pixel.x;
//            int center_y = (int) center_pixel.y;
//            int extra_radius = Mathf.Min(0, pen_thickness - 2);
//
//            for (int x = center_x - pen_thickness; x <= center_x + pen_thickness; x++)
//            {
//                for (int y = center_y - pen_thickness; y <= center_y + pen_thickness; y++)
//                {
//                    _drawableTexture.SetPixel(x, y, color_of_pen);
//                }
//            }
//
//            _drawableTexture.Apply();
//        }

        private bool RaycastUi(List<RaycastResult> raycastResults)
        {
            PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current) {position = new Vector2(Input.mousePosition.x, Input.mousePosition.y)};
            EventSystem.current.RaycastAll(eventDataCurrentPosition, raycastResults);
            return raycastResults.Count > 0;
        }

        private void CreateWidget(Texture2D texture2D)
        {
            StartCoroutine(LockRoutine());

            GameObject paintWidget = Resources.Load<GameObject>("DefaultWidgets/Paint");
            float unitySpaceWidth = texture2D.width / ArController.UnitySpaceToScreenSpaceRatio;
            float unitySpaceHeight = texture2D.height / ArController.UnitySpaceToScreenSpaceRatio;
            ((RectTransform) paintWidget.transform).sizeDelta = new Vector2(unitySpaceWidth, unitySpaceHeight);
            Sprite sprite = Sprite.Create(texture2D, new Rect(0.0f, 0.0f, texture2D.width, texture2D.height), new Vector2(0.5f, 0.5f), 100);
            paintWidget.GetComponent<Image>().sprite = sprite;
            paintWidget.transform.localScale = new Vector3(TextureScale, TextureScale, TextureScale);
            UiController.SelectedWidget = paintWidget;
            Vector2 position = new Vector2(TextureScale * (_xMin + (_xMax - _xMin) / 2), TextureScale * (_yMin + (_yMax - _yMin) / 2));
            ArController.AddWidgetStream(ArController.WidgetPaint, paintWidget, UiController.SelectedColor, position, unitySpaceWidth, unitySpaceHeight);
        }

        private IEnumerator LockRoutine()
        {
            _addEnable = false;
            yield return new WaitForSeconds(WidgetAddDelay);
            _addEnable = true;
        }
    }
}