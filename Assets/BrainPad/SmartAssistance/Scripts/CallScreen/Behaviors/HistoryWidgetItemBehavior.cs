﻿using System.Diagnostics.CodeAnalysis;
using BrainPad.SmartAssistance.Scripts.CallScreen.Controllers;
using BrainPad.SmartAssistance.Scripts.Core.Utils;
using BrainPad.SmartAssistance.Scripts.Models;
using GameToolkit.Localization;
using SmartAssistanceSDK;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace BrainPad.SmartAssistance.Scripts.CallScreen.Behaviors
{
    [SuppressMessage("ReSharper", "MemberCanBeMadeStatic.Local")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_SimpleTypes")]
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_BuiltInTypes")]
    public class HistoryWidgetItemBehavior : MonoBehaviour
    {
        public TextMeshProUGUI WidgetSource;
        public Image Image;
        public GameObject Selected;
        public Button DeleteButton;

        private Widget _widget;
        private HistoryListBehavior _historyListBehavior;

        private Vector2 _spriteSize;

        [Header("Labels")] public LocalizedText OnField;
        public LocalizedText Support;
        public LocalizedText You;
        
        public void SetWidget(Widget widget)
        {
            if (_spriteSize.Equals(Vector2.zero))
            {
                _spriteSize = new Vector2(((RectTransform) Image.gameObject.transform).sizeDelta.x, ((RectTransform) Image.gameObject.transform).sizeDelta.y);
            }

            _widget = widget;
            string source = _widget.Owner.Equals(SystemInfo.deviceUniqueIdentifier) ? You.Value : SmartAssistanceManager.Instance.RemoteUser.role == SAUserRole.UserOnField ? OnField.Value : Support.Value;
            WidgetSource.text = source;

            if (_widget.Type == ArController.WidgetPaint)
            {
                Texture2D scaledTexture = CreateScaledTexture(_widget.Instance.GetComponent<Image>().sprite.texture, (int) _spriteSize.x, (int) _spriteSize.y);
                Image.sprite = Sprite.Create(scaledTexture, new Rect(0.0f, 0.0f, scaledTexture.width, scaledTexture.height), new Vector2(0.5f, 0.5f), 100);
                ((RectTransform) Image.gameObject.transform).sizeDelta = new Vector2(scaledTexture.width, scaledTexture.height);
            }
            else
            {
                Image.sprite = Instantiate(_widget.Instance.GetComponent<Image>().sprite);
            }

            Image.color = _widget.Instance.GetComponent<Image>().color;
            if (_widget.SelectedLocal)
            {
                Selected.SetActive(true);
                DeleteButton.gameObject.SetActive(true);
            }
            else
            {
                DeleteButton.gameObject.SetActive(false);
            }
        }

        public void SetHistoryListBehavior(HistoryListBehavior historyListBehavior)
        {
            _historyListBehavior = historyListBehavior;
        }

        public void OnClick()
        {
            if (_historyListBehavior == null) return;
            if (_widget.SelectedLocal)
            {
                DeleteButton.gameObject.SetActive(false);
                Selected.SetActive(false);
                _historyListBehavior.HistoryItemDeselected(this);
            }
            else
            {
                DeleteButton.gameObject.SetActive(true);
                Selected.SetActive(true);
                _historyListBehavior.HistoryItemSelected(this);
            }
        }

        public void DeleteWidget()
        {
            _historyListBehavior.DeleteWidget(_widget);
        }

        public Widget GetWidget()
        {
            return _widget;
        }

        private Texture2D CreateScaledTexture(Texture2D source, int width, int height)
        {
            Texture2D copy = Instantiate(source);
            int newWidth;
            int newHeight;
            if (copy.width > copy.height)
            {
                newWidth = width;
                newHeight = (int) (copy.height / (float) copy.width * newWidth);
            }
            else
            {
                newHeight = height;
                newWidth = (int) (copy.width / (float) copy.height * newHeight);
            }

            TextureScale.Bilinear(copy, newWidth, newHeight);
            return copy;
        }
    }
}