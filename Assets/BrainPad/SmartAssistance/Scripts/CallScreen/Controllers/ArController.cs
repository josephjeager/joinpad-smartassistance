﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using BrainPad.SmartAssistance.Scripts.CallScreen.Behaviors;
using BrainPad.SmartAssistance.Scripts.Core.Delegates;
using BrainPad.SmartAssistance.Scripts.Models;
using BrainPad.SmartAssistance.Scripts.Models.Messages;
using Byn.Media;
using Byn.Net;
using GameToolkit.Localization;
using SmartAssistanceSDK;
using SmartAssistanceSDK.Delegates;
using UnityEngine;
using UnityEngine.UI;
using Random = System.Random;

namespace BrainPad.SmartAssistance.Scripts.CallScreen.Controllers
{
    [SuppressMessage("ReSharper", "UnusedMethodReturnValue.Local")]
    [SuppressMessage("ReSharper", "UseObjectOrCollectionInitializer")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_BuiltInTypes")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_SimpleTypes")]
    [SuppressMessage("ReSharper", "UnusedMethodReturnValue.Global")]
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    [SuppressMessage("ReSharper", "InvertIf")]
    [SuppressMessage("ReSharper", "SwitchStatementMissingSomeCases")]
    [SuppressMessage("ReSharper", "MemberCanBeMadeStatic.Global")]
    [SuppressMessage("ReSharper", "MemberCanBeMadeStatic.Local")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_Elsewhere")]
    public class ArController : MonoBehaviour
    {
        [Header("Configuration")] public ArTracker ArTracker;
        public RawImage CameraFeedImage;
        public GameObject ArWidgetsContainer;

        private List<Widget> _widgets = new List<Widget>();

        public const string WidgetAlert = "Alert";
        public const string WidgetCircle = "Circle";
        public const string WidgetArrowUp = "ArrowUp";
        public const string WidgetArrowDown = "ArrowDown";
        public const string WidgetArrowLeft = "ArrowLeft";
        public const string WidgetArrowRight = "ArrowRight";
        public const string WidgetArrowSquare = "Square";
        public const string WidgetArrowTriangle = "Triangle";
        public const string WidgetPaint = "Paint";

        public Dictionary<string, GameObject> AvailableWidgets;

        public int WidgetsLimit;
        public bool SmoothWidgetsMovement;
        public bool ShowGhost;
        public bool SimulateTracker;

        public bool RemoteVolumeOn = true;
        public bool MicOn = true;
        public bool VideoOn = true;
        public bool VideoEnabledByRemote = true;
        public bool IsStreaming = true;

        public event WidgetsChangedEvent WidgetsChangedEvent;
        public event AddTrackerEvent AddTrackerEvent;
        public event RemoveTrackerEvent RemoveTrackerEvent;

        [Header("Notifications")]
        public LocalizedText WidgetLost;
        public LocalizedText DeletedAWidget;
        public LocalizedText OnFieldUser;
        public LocalizedText Support;
        public LocalizedText WidgetsLimitReached;
        
        private Canvas _cameraFeedParentCanvas;
//        private float _cameraFeedScaleFactor;
        private Vector2 _screenSpaceCameraFeedSize; // The camera feed size in screen pixels
        private float _screenSpaceCameraFeedVerticalOverflow; // The camera feed vertical overflow in screen pixels, needed to remap touch points fo camera frame coordinates
        private float _unitySpaceToCameraFrameRatio; // The ratio between the unity space and the camera frame
        [HideInInspector] public float UnitySpaceToScreenSpaceRatio; // The ratio between the unity space and the screen space in pixels

        private bool _frameAvailable;
        private Vector2 _resolution;
        private int _nextWidgetId;
        private GameObject _ghost;
        private IEnumerator _environmentCheckCoroutine;

        private int _currentFrameWidth;
        private int _currentFrameHeight;

        // Unity Events               

        private void Start()
        {
            AvailableWidgets = new Dictionary<string, GameObject>();
            AvailableWidgets.Add(WidgetAlert, Resources.Load<GameObject>("DefaultWidgets/Alert"));
            AvailableWidgets.Add(WidgetCircle, Resources.Load<GameObject>("DefaultWidgets/Circle"));
            AvailableWidgets.Add(WidgetArrowUp, Resources.Load<GameObject>("DefaultWidgets/ArrowUp"));
            AvailableWidgets.Add(WidgetArrowDown, Resources.Load<GameObject>("DefaultWidgets/ArrowDown"));
            AvailableWidgets.Add(WidgetArrowLeft, Resources.Load<GameObject>("DefaultWidgets/ArrowLeft"));
            AvailableWidgets.Add(WidgetArrowRight, Resources.Load<GameObject>("DefaultWidgets/ArrowRight"));
            AvailableWidgets.Add(WidgetArrowSquare, Resources.Load<GameObject>("DefaultWidgets/Square"));
            AvailableWidgets.Add(WidgetArrowTriangle, Resources.Load<GameObject>("DefaultWidgets/Triangle"));
            AvailableWidgets.Add(WidgetPaint, Resources.Load<GameObject>("DefaultWidgets/Paint"));
            
            _cameraFeedParentCanvas = CameraFeedImage.GetComponentInParent<Canvas>();
        }

        private void OnEnable()
        {
            ReleaseObjects();

            SmartAssistanceManager.Instance.WebRtcCallEvent += OnWebRtcCallEvent;
            SmartAssistanceManager.Instance.SignalerCallEvent += OnSignalerCallEvent;

            _widgets = new List<Widget>();

            if (SmartAssistanceManager.Instance.IsSender)
            {
                if (!SimulateTracker)
                {
                    ArTracker.StartTracker();
                    StartCoroutine(_environmentCheckCoroutine = EnvironmentCheckRoutine());
                }
            }

            IsStreaming = true;
            ArWidgetsContainer.SetActive(true);
            CameraFeedImage.color = Color.white;
            
            VideoOn = true;
            VideoEnabledByRemote = true;
        }

        private void OnDisable()
        {
            ReleaseObjects();
            if (SmartAssistanceManager.Instance != null)
            {
                SmartAssistanceManager.Instance.WebRtcCallEvent -= OnWebRtcCallEvent;
                SmartAssistanceManager.Instance.SignalerCallEvent -= OnSignalerCallEvent;
            }

            if (_environmentCheckCoroutine != null)
            {
                StopCoroutine(_environmentCheckCoroutine);
            }
        }

        private void Update()
        {
            if (Math.Abs(_resolution.x - Screen.width) > 0.1f || Math.Abs(_resolution.y - Screen.height) > 0.1f || Math.Abs(UnitySpaceToScreenSpaceRatio - _cameraFeedParentCanvas.scaleFactor) > 0.1f)
            {
                RefreshSpaceMeasures();
            }

            if (SmoothWidgetsMovement)
            {
                foreach (Widget widget in _widgets)
                {
                    widget.LerpUpdate();
                }
            }
        }

        private void OnApplicationPause(bool pauseStatus)
        {
            bool isPaused = pauseStatus;
            if (isPaused)
            {
                SmartAssistanceManager.Instance.Call.SetVolume(0, SmartAssistanceManager.Instance.RemoteConnectionId);
                SmartAssistanceManager.Instance.Call.SetMute(true);
                VideoOn = false;
            }
            else
            {
                VideoOn = true;
                if (RemoteVolumeOn)
                {
                    SmartAssistanceManager.Instance.Call.SetVolume(1, SmartAssistanceManager.Instance.RemoteConnectionId);
                }

                if (MicOn)
                {
                    SmartAssistanceManager.Instance.Call.SetMute(false);
                }
            }
        }

        // Ingoing Events

        public void OnFrameProcessed(Dictionary<int, Matrix4x4> trackingMatrixes)
        {
            foreach (Widget addedWidget in _widgets)
            {
                Matrix4x4 matrix4X4;
                if (trackingMatrixes.TryGetValue(addedWidget.Id, out matrix4X4))
                {
                    UpdateWidgetLocal(addedWidget, matrix4X4.m02, matrix4X4.m12);

                    Tracking trackingMessage = new Tracking(addedWidget.Id, matrix4X4.m02, matrix4X4.m12);
                    SmartAssistanceManager.Instance.Send(trackingMessage.GetMessage());
                }
                else
                {
                    if (addedWidget.Instance.activeInHierarchy)
                    {
                        HideWidgetLocal(addedWidget);

                        TrackerOff trackerOff = new TrackerOff(addedWidget.Id);
                        SmartAssistanceManager.Instance.Send(trackerOff.GetMessage());
                    }
                }
            }
        }

        public void OnTrackerLost(int widgetId)
        {
            Widget toRemoveWidget = _widgets.FirstOrDefault(widget => widget.Id == widgetId);
            if (toRemoveWidget != null)
            {
                _widgets.RemoveAll(widget => widget.Id == widgetId);
                Destroy(toRemoveWidget.Instance);
                if (WidgetsChangedEvent != null)
                {
                    WidgetsChangedEvent(this, _widgets);
                }

                SmartAssistanceManager.Instance.DispatchNotification(WidgetLost.Value);

                WidgetLostMessage widgetLostMessage = new WidgetLostMessage(widgetId);
                SmartAssistanceManager.Instance.Send(widgetLostMessage.GetMessage());

                TrackerRemove trackerRemove = new TrackerRemove(widgetId);
                SmartAssistanceManager.Instance.Send(trackerRemove.GetMessage());

                WidgetsSync widgetsSync = new WidgetsSync(_widgets);
                SmartAssistanceManager.Instance.Send(widgetsSync.GetMessage());
            }
        }

        private void OnWebRtcCallEvent(object sender, ICall iCall, CallEventArgs args)
        {
            switch (args.Type)
            {
                case CallEventType.CallAccepted:
                    IsStreaming = true;
                    ToggleArWidgetsVisibility();
                    CameraFeedImage.color = VideoEnabledByRemote ? Color.white : Color.black;
                    break;
                case CallEventType.CallEnded:
                    IsStreaming = false;
                    ToggleArWidgetsVisibility();
                    CameraFeedImage.color = Color.black;
                    break;
                case CallEventType.Message:
                    MessageEventArgs messageEventArgs = args as MessageEventArgs;
                    if (messageEventArgs != null)
                    {
                        HandleMessage(messageEventArgs.Content);
                    }

                    break;
                case CallEventType.FrameUpdate:
                    FrameUpdateEventArgs frameArgs = args as FrameUpdateEventArgs;
                    if (frameArgs != null)
                    {
                        if (_currentFrameWidth != frameArgs.Frame.Width || _currentFrameHeight != frameArgs.Frame.Height)
                        {
                            _currentFrameWidth = frameArgs.Frame.Width;
                            _currentFrameHeight = frameArgs.Frame.Height;
                            RefreshSpaceMeasures();
                        }

                        if (VideoEnabledByRemote)
                        {
//#if UNITY_IOS && !UNITY_EDITOR
//                            if (frameArgs.Frame.Width < frameArgs.Frame.Height)
//                            {
//                                if (SmartAssistanceManager.Instance.DeviceRotationHolder.transform.eulerAngles.z < 360 && SmartAssistanceManager.Instance.DeviceRotationHolder.transform.eulerAngles.z > 180)
//                                {
//                                    CameraFeedImage.rectTransform.sizeDelta = new Vector2(0, 2720);
//                                    CameraFeedImage.GetComponent<AspectRatioFitter>().aspectMode = AspectRatioFitter.AspectMode.HeightControlsWidth;
//                                    CameraFeedImage.GetComponent<AspectRatioFitter>().aspectRatio = 0.75f;
//                                    CameraFeedImage.transform.localEulerAngles = new Vector3(0, 0, 90);
//                                }
//                                else
//                                {
////                     Inverse Portrait
////                    if (CameraFeedImage.rectTransform.sizeDelta.x < 3627)
////                    {
//                                    CameraFeedImage.rectTransform.sizeDelta = new Vector2(0, 2720);
//                                    CameraFeedImage.GetComponent<AspectRatioFitter>().aspectMode = AspectRatioFitter.AspectMode.HeightControlsWidth;
//                                    CameraFeedImage.GetComponent<AspectRatioFitter>().aspectRatio = 0.75f;
//                                    CameraFeedImage.transform.localEulerAngles = new Vector3(0, 0, 270);
////                    }
//                                }
//                            }
//                            else
//                            {
//                                if (SmartAssistanceManager.Instance.DeviceRotationHolder.transform.eulerAngles.z < 270 && SmartAssistanceManager.Instance.DeviceRotationHolder.transform.eulerAngles.z > 90)
//                                {
//                                    // Landscape Left 
//                                    CameraFeedImage.rectTransform.sizeDelta = new Vector2(2720, 0);
//                                    CameraFeedImage.GetComponent<AspectRatioFitter>().aspectRatio = 1.333333f;
//                                    CameraFeedImage.GetComponent<AspectRatioFitter>().aspectMode = AspectRatioFitter.AspectMode.WidthControlsHeight;
//                                    CameraFeedImage.transform.localEulerAngles = new Vector3(0, 0, 180);
//                                }
//                                else
//                                {
////                             Landscape Right 
//                                    CameraFeedImage.rectTransform.sizeDelta = new Vector2(2720, 0);
//                                    CameraFeedImage.GetComponent<AspectRatioFitter>().aspectRatio = 1.333333f;
//                                    CameraFeedImage.GetComponent<AspectRatioFitter>().aspectMode = AspectRatioFitter.AspectMode.WidthControlsHeight;
//                                    CameraFeedImage.transform.localEulerAngles = new Vector3(0, 0, 0);
//                                }
//                            }
//#endif

                            // bool textureCreated = 
                            UnityMediaHelper.UpdateRawImage(CameraFeedImage, frameArgs.Frame);
                        }

                        if (!frameArgs.IsRemote)
                        {
                            ArTracker.UpdateFrame(frameArgs);
                        }

                        _frameAvailable = true;
                    }

                    break;
            }
        }

        private void OnSignalerCallEvent(object sender, SASignalerEventArgs args)
        {
            switch (args.EventType)
            {
                case SASignalerEventType.CallStop:
                    if (SmartAssistanceManager.Instance.IsSender && !SimulateTracker)
                    {
                        ArTracker.StopTracker();
                    }

                    ReleaseObjects();

                    IsStreaming = false;
                    ToggleArWidgetsVisibility();
                    CameraFeedImage.color = Color.black;
                    break;
                case SASignalerEventType.SocketReconnectionFailed:
                    if (SmartAssistanceManager.Instance.IsSender && !SimulateTracker)
                    {
                        ArTracker.StopTracker();
                    }

                    ReleaseObjects();

                    IsStreaming = false;
                    ToggleArWidgetsVisibility();
                    CameraFeedImage.color = Color.black;
                    break;
            }
        }

        // Messages Dispatcher   

        private void HandleMessage(string message)
        {
            if (SmartAssistanceManager.Instance.IsSender)
            {
                if (message.StartsWith(TrackerRequest.PrefixTag))
                {
                    TrackerRequest trackerRequest = new TrackerRequest(message);
                    AddRequestedTracker(trackerRequest);
                }
                else if (message.StartsWith(TrackerRemove.PrefixTag))
                {
                    if (!_frameAvailable) return;
                    _frameAvailable = false;

                    TrackerRemove trackerRemove = new TrackerRemove(message);
                    if (RemoveTrackerEvent != null)
                    {
                        RemoveTrackerEvent(this, trackerRemove.WidgetId);
                    }

                    Widget toRemoveWidget = _widgets.FirstOrDefault(widget => widget.Id == trackerRemove.WidgetId);
                    RemoveWidgetLocal(toRemoveWidget);
                }
            }
            else
            {
                if (message.StartsWith(TrackerOn.PrefixTag))
                {
                    TrackerOn trackerOn = new TrackerOn(message);
                    AddWidgetRemote(trackerOn);
                }
                else if (message.StartsWith(Tracking.PrefixTag))
                {
                    Tracking tracking = new Tracking(message);
                    UpdateWidgetRemote(tracking);
                }
                else if (message.StartsWith(TrackerOff.PrefixTag))
                {
                    TrackerOff trackerOff = new TrackerOff(message);
                    HideWidgetRemote(trackerOff);
                }
                else if (message.StartsWith(TrackerRemove.PrefixTag))
                {
                    if (!_frameAvailable) return;
                    _frameAvailable = false;

                    TrackerRemove trackerRemove = new TrackerRemove(message);
                    RemoveWidgetRemote(trackerRemove);
                }
                else if (message.StartsWith(VideoEnabled.PrefixTag))
                {
                    VideoEnabled videoEnabled = new VideoEnabled(message);
                    VideoEnabledByRemote = videoEnabled.Enable;
                    ToggleArWidgetsVisibility();
                    CameraFeedImage.color = VideoEnabledByRemote ? Color.white : Color.black;
                }
                else if (message.StartsWith(WidgetLostMessage.PrefixTag))
                {
                    SmartAssistanceManager.Instance.DispatchNotification(WidgetLost.Value);
                }
                else if (message.StartsWith(WidgetsSync.PrefixTag))
                {
                    WidgetsSync widgetsSync = new WidgetsSync(message);
                    RemoveOrphanWidgets(widgetsSync);
                }
            }

            if (message.StartsWith(WidgetSelect.PrefixTag))
            {
                WidgetSelect widgetSelect = new WidgetSelect(message);
                SelectWidgetRemote(widgetSelect);
            }
            else if (message.StartsWith(WidgetDeselect.PrefixTag))
            {
                WidgetDeselect widgetDeselect = new WidgetDeselect(message);
                DeselectWidgetRemote(widgetDeselect);
            }
            else if (message.StartsWith(WidgetDeletedMessage.PrefixTag))
            {
                string who = SmartAssistanceManager.Instance.RemoteUser.role == SAUserRole.UserOnField ? OnFieldUser.Value : Support.Value;
                SmartAssistanceManager.Instance.DispatchNotification(who + DeletedAWidget.Value);
            }
            else if (message.StartsWith(ApplicationPaused.PrefixTag))
            {
                ApplicationPaused applicationPaused = new ApplicationPaused(message);
                if (!applicationPaused.IsPaused)
                {
                    VideoEnabledByRemote = true;
                    ToggleArWidgetsVisibility();
                    CameraFeedImage.color = Color.white;
                }
            }
        }

        // Memory Cleanup

        private void ReleaseObjects()
        {
            if (_widgets != null)
            {
                foreach (Widget addedWidget in _widgets)
                {
                    DestroyImmediate(addedWidget.Instance);
                }

                _widgets.Clear();

                if (WidgetsChangedEvent != null)
                {
                    WidgetsChangedEvent(this, _widgets);
                }
            }
        }

        // Settings Controls        

        public void ToggleSpeaker(bool activate)
        {
            if (SmartAssistanceManager.Instance.RemoteConnectionId == ConnectionId.INVALID)
            {
                return;
            }

            if (activate)
            {
                SmartAssistanceManager.Instance.Call.SetVolume(1, SmartAssistanceManager.Instance.RemoteConnectionId);
                RemoteVolumeOn = true;
            }
            else
            {
                SmartAssistanceManager.Instance.Call.SetVolume(0, SmartAssistanceManager.Instance.RemoteConnectionId);
                RemoteVolumeOn = false;
            }

            SpeakersEnabled speakersEnabled = new SpeakersEnabled(activate);
            SmartAssistanceManager.Instance.Send(speakersEnabled.GetMessage());
        }

        public void ToggleVideo(bool activate)
        {
            VideoOn = activate;
            VideoEnabled videoEnabled = new VideoEnabled(activate);
            SmartAssistanceManager.Instance.Send(videoEnabled.GetMessage());
        }

        public void ToggleMic(bool activate)
        {
            MicOn = activate;
            SmartAssistanceManager.Instance.Call.SetMute(!activate);

            MicEnabled micEnabled = new MicEnabled(activate);
            SmartAssistanceManager.Instance.Send(micEnabled.GetMessage());
        }

        // Widgets Handlers   

        public void AddWidgetStream(string widgetType, GameObject widget, Color32 color, Vector3 touchPosition, float frameSpaceWidth, float frameSpaceHeight)
        {
            if (!_frameAvailable) return;
            _frameAvailable = false;

            if (_widgets.Count >= WidgetsLimit)
            {
                SmartAssistanceManager.Instance.DispatchNotification(WidgetsLimitReached.Value);
                return;
            }

            if (SimulateTracker)
            {
                AddWidgetStreamSimulated(widgetType, widget, color, touchPosition, frameSpaceWidth, frameSpaceHeight);
                return;
            }

            if (SmartAssistanceManager.Instance.IsSender)
            {
                AddTracker(widgetType, SystemInfo.deviceUniqueIdentifier, widget, color, touchPosition, frameSpaceWidth, frameSpaceHeight);
            }
            else
            {
                Vector2 framePoint = TouchToFrameCoordinates(touchPosition);
                Rect trackingRect = GetTrackingRect(framePoint);
                TrackerRequest trackerRequest = new TrackerRequest(trackingRect.xMin, trackingRect.yMin, trackingRect.xMax, trackingRect.yMax, framePoint.x, framePoint.y, widgetType, color, SystemInfo.deviceUniqueIdentifier, "", widget.GetComponent<Image>().sprite.texture, frameSpaceWidth, frameSpaceHeight);
                SmartAssistanceManager.Instance.Send(trackerRequest.GetMessage());
            }
        }

        private void AddTracker(string widgetType, string owner, GameObject widgetGameObject, Color32 color, Vector3 touchPosition, float frameSpaceWidth, float frameSpaceHeight)
        {
            Vector2 framePoint = TouchToFrameCoordinates(touchPosition);
            Rect trackingRect = GetTrackingRect(framePoint);

            if (AddTrackerEvent != null)
            {
                int newWidgetId = _nextWidgetId;
                bool trackerAddedSuccesfully = AddTrackerEvent(this, newWidgetId, trackingRect);
                if (trackerAddedSuccesfully)
                {
                    GameObject addedWidget = AddWidgetLocal(newWidgetId, widgetType, owner, widgetGameObject, framePoint.x, framePoint.y, color /*, trackingRect*/);
                    TrackerOn trackerOn = new TrackerOn(trackingRect.xMin, trackingRect.yMin, trackingRect.xMax, trackingRect.yMax, framePoint.x, framePoint.y, widgetType, color, newWidgetId, owner, "", addedWidget.GetComponent<Image>().sprite.texture, frameSpaceWidth, frameSpaceHeight);
                    SmartAssistanceManager.Instance.Send(trackerOn.GetMessage());

                    WidgetsSync widgetsSync = new WidgetsSync(_widgets);
                    SmartAssistanceManager.Instance.Send(widgetsSync.GetMessage());

                    _nextWidgetId++;
                }
            }
        }

        public void RemoveTracker(int widgetId)
        {
            if (!_frameAvailable) return;

            _frameAvailable = false;

            if (RemoveTrackerEvent != null)
            {
                RemoveTrackerEvent(this, widgetId);
            }

            Widget toRemoveWidget = _widgets.FirstOrDefault(widget => widget.Id == widgetId);
            RemoveWidgetLocal(toRemoveWidget);

            TrackerRemove trackerRemove = new TrackerRemove(widgetId);
            SmartAssistanceManager.Instance.Send(trackerRemove.GetMessage());

            WidgetDeletedMessage widgetDeletedMessage = new WidgetDeletedMessage(widgetId);
            SmartAssistanceManager.Instance.Send(widgetDeletedMessage.GetMessage());
        }

        private void AddRequestedTracker(TrackerRequest trackerRequest)
        {
            if (SimulateTracker)
            {
                AddRequestedTrackerSimulated(trackerRequest);
                return;
            }

            GameObject widget;
            if (trackerRequest.WidgetType == WidgetPaint)
            {
                widget = Resources.Load<GameObject>("DefaultWidgets/Paint");
                Texture2D texture2D = trackerRequest.Texture2D;
                ((RectTransform) widget.transform).sizeDelta = new Vector2(trackerRequest.UnitySpaceWidth, trackerRequest.UnitySpaceHeight);
                widget.GetComponent<Image>().sprite = Sprite.Create(texture2D, new Rect(0.0f, 0.0f, texture2D.width, texture2D.height), new Vector2(0.5f, 0.5f), 100);
                widget.transform.localScale = new Vector3(WidgetPaintBehavior.TextureScale, WidgetPaintBehavior.TextureScale, WidgetPaintBehavior.TextureScale);
            }
            else
            {
                widget = AvailableWidgets[trackerRequest.WidgetType];
            }

            Vector2 touchCoordinates = FrameToTouchCoordinates(new Vector2(trackerRequest.FramePointX, trackerRequest.FramePointY));
            AddTracker(trackerRequest.WidgetType, trackerRequest.Owner, widget, trackerRequest.WidgetColor, touchCoordinates, trackerRequest.UnitySpaceWidth, trackerRequest.UnitySpaceHeight);
        }

        private GameObject AddWidgetLocal(int widgetId, string widgetType, string owner, GameObject widgetGameObject, float framePointX, float framePointY, Color32 color /*, Rect trackingRect*/)
        {
            GameObject widgetInstance = Instantiate(widgetGameObject, ArWidgetsContainer.transform);
            widgetInstance.GetComponent<Image>().color = color;
            widgetInstance.transform.localPosition = FrameToCameraFeedCoordinates(framePointX, framePointY);
            _widgets.Add(new Widget(widgetId, widgetType, owner, widgetInstance));

            if (WidgetsChangedEvent != null)
            {
                WidgetsChangedEvent(this, _widgets);
            }

            return widgetInstance;
        }

        private GameObject AddWidgetRemote(TrackerOn trackerOn)
        {
            GameObject widget;
            if (trackerOn.WidgetType == WidgetPaint)
            {
                widget = Resources.Load<GameObject>("DefaultWidgets/Paint");
                Texture2D texture2D = trackerOn.Texture2D;
                ((RectTransform) widget.transform).sizeDelta = new Vector2(trackerOn.UnitySpaceWidth, trackerOn.UnitySpaceHeight);
                Sprite sprite = Sprite.Create(texture2D, new Rect(0.0f, 0.0f, texture2D.width, texture2D.height), new Vector2(0.5f, 0.5f), 100);
                widget.transform.localScale = new Vector3(WidgetPaintBehavior.TextureScale, WidgetPaintBehavior.TextureScale, WidgetPaintBehavior.TextureScale);
                widget.GetComponent<Image>().sprite = sprite;
            }
            else
            {
                widget = AvailableWidgets[trackerOn.WidgetType];
            }

            GameObject widgetInstance = Instantiate(widget, ArWidgetsContainer.transform);
            widgetInstance.transform.localPosition = FrameToCameraFeedCoordinates(trackerOn.FramePointX, trackerOn.FramePointY);
            widgetInstance.GetComponent<Image>().color = trackerOn.WidgetColor;
            _widgets.Add(new Widget(trackerOn.WidgetId, trackerOn.WidgetType, trackerOn.Owner, widgetInstance /*Widget.WidgetSource.Remote*/));

            if (WidgetsChangedEvent != null)
            {
                WidgetsChangedEvent(this, _widgets);
            }

            return widgetInstance;
        }

        private GameObject UpdateWidgetLocal(Widget widget, float framePointX, float framePointY)
        {
            if (ShowGhost)
            {
                if (_ghost == null)
                {
                    _ghost = Instantiate(widget.Instance, ArWidgetsContainer.transform);
                    _ghost.transform.SetAsFirstSibling();
                    _ghost.GetComponent<Image>().color = Color.magenta;
                }

                _ghost.transform.localPosition = TrackerToCameraFeedCoordinates(framePointX, framePointY);
            }

            if (!SmoothWidgetsMovement)
            {
                widget.Instance.transform.localPosition = TrackerToCameraFeedCoordinates(framePointX, framePointY);
            }

            if (!widget.Instance.activeInHierarchy)
            {
                if (SmoothWidgetsMovement)
                {
                    widget.LerpInitialPosition(TrackerToCameraFeedCoordinates(framePointX, framePointY));
                }

                widget.Instance.SetActive(true);
                if (WidgetsChangedEvent != null)
                {
                    WidgetsChangedEvent(this, _widgets);
                }
            }
            else
            {
                if (SmoothWidgetsMovement)
                {
                    widget.LerpNewPosition(TrackerToCameraFeedCoordinates(framePointX, framePointY));
                }
            }

            return widget.Instance;
        }

        private GameObject UpdateWidgetRemote(Tracking tracking)
        {
            Widget trackedWidget = _widgets.FirstOrDefault(widget => widget.Id == tracking.WidgetId);
            if (trackedWidget != null)
            {
                if (ShowGhost)
                {
                    if (_ghost == null)
                    {
                        _ghost = Instantiate(trackedWidget.Instance, ArWidgetsContainer.transform);
                        _ghost.transform.SetAsFirstSibling();
                        _ghost.GetComponent<Image>().color = Color.magenta;
                    }

                    _ghost.transform.localPosition = TrackerToCameraFeedCoordinates(tracking.X, tracking.Y);
                }

                if (!SmoothWidgetsMovement)
                {
                    trackedWidget.Instance.transform.localPosition = TrackerToCameraFeedCoordinates(tracking.X, tracking.Y);
                }

                if (!trackedWidget.Instance.activeInHierarchy)
                {
                    if (SmoothWidgetsMovement)
                    {
                        trackedWidget.LerpInitialPosition(TrackerToCameraFeedCoordinates(tracking.X, tracking.Y));
                    }

                    trackedWidget.Instance.SetActive(true);
                    if (WidgetsChangedEvent != null)
                    {
                        WidgetsChangedEvent(this, _widgets);
                    }
                }
                else
                {
                    trackedWidget.LerpNewPosition(TrackerToCameraFeedCoordinates(tracking.X, tracking.Y));
                }

                return trackedWidget.Instance;
            }

            return null;
        }

        private GameObject HideWidgetLocal(Widget widget)
        {
            widget.Instance.SetActive(false);
            if (WidgetsChangedEvent != null)
            {
                WidgetsChangedEvent(this, _widgets);
            }

            return widget.Instance;
        }

        private GameObject HideWidgetRemote(TrackerOff trackerOff)
        {
            GameObject widgetGameObject = null;
            Widget trackedWidget = _widgets.FirstOrDefault(widget => widget.Id == trackerOff.WidgetId);
            if (trackedWidget != null)
            {
                if (trackedWidget.Instance.activeInHierarchy)
                {
                    trackedWidget.Instance.SetActive(false);
                    if (WidgetsChangedEvent != null)
                    {
                        WidgetsChangedEvent(this, _widgets);
                    }
                }

                widgetGameObject = trackedWidget.Instance;
            }

            return widgetGameObject;
        }

        public GameObject SelectWidgetLocal(Widget widgetLocal)
        {
            Widget toSelectWidget = _widgets.FirstOrDefault(widget => widget.Id == widgetLocal.Id);
            if (toSelectWidget == null)
                return null;

            toSelectWidget.SelectLocal();
            return toSelectWidget.Instance;
        }

        private GameObject SelectWidgetRemote(WidgetSelect widgetSelect)
        {
            GameObject widgetGameObject = null;
            Widget toSelectWidget = _widgets.FirstOrDefault(widget => widget.Id == widgetSelect.WidgetId);
            if (toSelectWidget != null)
            {
                toSelectWidget.SelectRemote();
                widgetGameObject = toSelectWidget.Instance;
            }

            return widgetGameObject;
        }

        public GameObject DeselectWidgetLocal(Widget widgetLocal)
        {
            Widget toDeselectWidget = _widgets.FirstOrDefault(widget => widget.Id == widgetLocal.Id);
            if (toDeselectWidget == null)
                return null;

            toDeselectWidget.DeselectLocal();
            return toDeselectWidget.Instance;
        }

        private GameObject DeselectWidgetRemote(WidgetDeselect widgetDeselect)
        {
            GameObject widgetGameObject = null;
            Widget toDeselectWidget = _widgets.FirstOrDefault(widget => widget.Id == widgetDeselect.WidgetId);
            if (toDeselectWidget != null)
            {
                toDeselectWidget.DeselectRemote();
                widgetGameObject = toDeselectWidget.Instance;
            }

            return widgetGameObject;
        }

        private void RemoveWidgetLocal(Widget toBeRemovedWidget)
        {
            _widgets.RemoveAll(widget => widget.Id == toBeRemovedWidget.Id);
            if (WidgetsChangedEvent != null)
            {
                WidgetsChangedEvent(this, _widgets);
            }

            Destroy(toBeRemovedWidget.Instance);
        }

        private void RemoveWidgetRemote(TrackerRemove trackerRemove)
        {
            Widget toRemoveWidget = _widgets.FirstOrDefault(widget => widget.Id == trackerRemove.WidgetId);
            if (toRemoveWidget != null)
            {
                _widgets.RemoveAll(widget => widget.Id == trackerRemove.WidgetId);
                if (WidgetsChangedEvent != null)
                {
                    WidgetsChangedEvent(this, _widgets);
                }

                Destroy(toRemoveWidget.Instance);
            }
        }

        private void RemoveOrphanWidgets(WidgetsSync widgetsSync)
        {
            // Remove widget not prent in sender widgets list
            for (int i = _widgets.Count - 1; i >= 0; i--)
            {
                Widget widget = _widgets[i];
                List<int> lookUp = widgetsSync.WidgetIds.FindAll(widgetId => widgetId == widget.Id);
                if (lookUp.Count == 0)
                {
                    if (widget.Instance != null)
                    {
                        Destroy(widget.Instance);
                    }

                    _widgets.RemoveAt(i);
                    if (WidgetsChangedEvent != null)
                    {
                        WidgetsChangedEvent(this, _widgets);
                    }
                }
            }

            RemoveOrphanWidgetInstance();
        }

        private IEnumerator EnvironmentCheckRoutine()
        {
            while (gameObject.activeInHierarchy)
            {
                yield return new WaitForSeconds(5);

                ToggleArWidgetsVisibility();
                CameraFeedImage.color = VideoEnabledByRemote ? Color.white : Color.black;

                if (SmartAssistanceManager.Instance.IsSender)
                {
                    WidgetsSync widgetsSync = new WidgetsSync(_widgets);
                    SmartAssistanceManager.Instance.Send(widgetsSync.GetMessage());
                }
                else
                {
                    RemoveOrphanWidgetInstance();
                }
            }
        }

        private void RemoveOrphanWidgetInstance()
        {
            // Remove widget instances not prent in local widgets list
            for (int i = ArWidgetsContainer.transform.childCount - 1; i >= 0; i--)
            {
                List<Widget> lookUp = _widgets.FindAll(widget => widget.Instance == ArWidgetsContainer.transform.GetChild(i).gameObject);
                if (lookUp.Count == 0)
                {
                    Destroy(ArWidgetsContainer.transform.GetChild(i).gameObject);
                }
            }
        }

        // Visibility Toggle

        private void ToggleArWidgetsVisibility()
        {
            ArWidgetsContainer.SetActive(VideoEnabledByRemote && IsStreaming);
        }

        // Tracker Simulation

        private void AddWidgetStreamSimulated(string widgetType, GameObject widget, Color32 color, Vector3 touchPosition, float frameSpaceWidth, float frameSpaceHeight)
        {
            if (_widgets.Count >= WidgetsLimit)
            {
                SmartAssistanceManager.Instance.DispatchNotification(WidgetsLimitReached.Value);
                return;
            }

            if (SmartAssistanceManager.Instance.IsSender)
            {
                Vector2 framePoint = TouchToFrameCoordinates(touchPosition);
                Rect trackingRect = GetTrackingRect(framePoint);
                int newWidgetId = _nextWidgetId;

                GameObject widgetInstance = Instantiate(widget, ArWidgetsContainer.transform);
                widgetInstance.GetComponent<Image>().color = color;
                widgetInstance.transform.localPosition = FrameToCameraFeedCoordinates(framePoint.x, framePoint.y);

                Widget widgetObject = new Widget(newWidgetId, widgetType, SystemInfo.deviceUniqueIdentifier, widgetInstance);
                _widgets.Add(widgetObject);
                if (WidgetsChangedEvent != null)
                {
                    WidgetsChangedEvent(this, _widgets);
                }

                TrackerOn trackerOn = new TrackerOn(trackingRect.xMin, trackingRect.yMin, trackingRect.xMax, trackingRect.yMax, framePoint.x, framePoint.y, widgetType, color, newWidgetId, SystemInfo.deviceUniqueIdentifier, "", widgetInstance.GetComponent<Image>().sprite.texture, frameSpaceWidth, frameSpaceHeight);
                SmartAssistanceManager.Instance.Send(trackerOn.GetMessage());

                WidgetsSync widgetsSync = new WidgetsSync(_widgets);
                SmartAssistanceManager.Instance.Send(widgetsSync.GetMessage());

                _nextWidgetId++;
                StartCoroutine(SimulateTracking(widgetObject, framePoint));
            }
            else
            {
                Vector2 framePoint = TouchToFrameCoordinates(touchPosition);
                Rect trackingRect = GetTrackingRect(framePoint);
                TrackerRequest trackerRequest = new TrackerRequest(trackingRect.xMin, trackingRect.yMin, trackingRect.xMax, trackingRect.yMax, framePoint.x, framePoint.y, widgetType, color, SystemInfo.deviceUniqueIdentifier, "", widget.GetComponent<Image>().sprite.texture, frameSpaceWidth, frameSpaceHeight);
                SmartAssistanceManager.Instance.Send(trackerRequest.GetMessage());
            }
        }

        private void AddRequestedTrackerSimulated(TrackerRequest trackerRequest)
        {
            GameObject widget;
            if (trackerRequest.WidgetType == WidgetPaint)
            {
                widget = Resources.Load<GameObject>("DefaultWidgets/Paint");
                Texture2D texture2D = trackerRequest.Texture2D;
                ((RectTransform) widget.transform).sizeDelta = new Vector2(trackerRequest.UnitySpaceWidth, trackerRequest.UnitySpaceHeight);
                widget.GetComponent<Image>().sprite = Sprite.Create(texture2D, new Rect(0.0f, 0.0f, texture2D.width, texture2D.height), new Vector2(0.5f, 0.5f), 100);
                widget.transform.localScale = new Vector3(WidgetPaintBehavior.TextureScale, WidgetPaintBehavior.TextureScale, WidgetPaintBehavior.TextureScale);
            }
            else
            {
                widget = AvailableWidgets[trackerRequest.WidgetType];
            }

            Vector2 touchCoordinates = FrameToTouchCoordinates(new Vector2(trackerRequest.FramePointX, trackerRequest.FramePointY));
            Vector2 framePoint = TouchToFrameCoordinates(touchCoordinates);
            Rect trackingRect = GetTrackingRect(framePoint);

            GameObject widgetInstance = Instantiate(widget, ArWidgetsContainer.transform);
            widgetInstance.GetComponent<Image>().color = trackerRequest.WidgetColor;
            widgetInstance.transform.localPosition = FrameToCameraFeedCoordinates(touchCoordinates.x, touchCoordinates.y);

            Widget widgetObject = new Widget(_nextWidgetId, trackerRequest.WidgetType, SystemInfo.deviceUniqueIdentifier, widgetInstance /*Widget.WidgetSource.Local*/);
            _widgets.Add(widgetObject);
            if (WidgetsChangedEvent != null)
            {
                WidgetsChangedEvent(this, _widgets);
            }

            TrackerOn trackerOn = new TrackerOn(trackingRect.xMin, trackingRect.yMin, trackingRect.xMax, trackingRect.yMax, framePoint.x, framePoint.y, trackerRequest.WidgetType, trackerRequest.WidgetColor, _nextWidgetId, SystemInfo.deviceUniqueIdentifier, "", widgetInstance.GetComponent<Image>().sprite.texture, trackerRequest.UnitySpaceWidth, trackerRequest.UnitySpaceHeight);
            SmartAssistanceManager.Instance.Send(trackerOn.GetMessage());
            _nextWidgetId++;
            StartCoroutine(SimulateTracking(widgetObject, framePoint));
        }

        private IEnumerator SimulateTracking(Widget widget, Vector2 framePoint)
        {
            yield return new WaitForSeconds(0.3f);

            Random r = new Random();
            const float minTimePerFrameUpdate = 0.02f;
            const float maxTimePerFrameUpdate = 0.8f;
            const float minDistPerFrameUpdate = 10;
            const float maxDistPerFrameUpdate = 70;

            Vector2 currentFramePoint = new Vector2(framePoint.x, framePoint.y);
            while (true)
            {
                float offsetX = (float) (r.NextDouble() * maxDistPerFrameUpdate - maxDistPerFrameUpdate / 2);
                float offsetY = (float) (r.NextDouble() * maxDistPerFrameUpdate - maxDistPerFrameUpdate / 2);

                if (offsetX > 0)
                {
                    offsetX += minDistPerFrameUpdate;
                }
                else
                {
                    offsetX -= minDistPerFrameUpdate;
                }

                if (offsetY > 0)
                {
                    offsetY += minDistPerFrameUpdate;
                }
                else
                {
                    offsetY -= minDistPerFrameUpdate;
                }

                float newX = currentFramePoint.x + offsetX;
                float newY = currentFramePoint.y + offsetY;

                double randomTime = r.NextDouble() * (maxTimePerFrameUpdate - minTimePerFrameUpdate) + minTimePerFrameUpdate;
                newX *= (float) randomTime / maxTimePerFrameUpdate;

                if (newX < 200) newX = 200;
                if (newX > _currentFrameWidth - 200) newX = _currentFrameWidth - 200;
                if (newY < 200) newY = 200;
                if (newY > _currentFrameHeight - 200) newY = _currentFrameHeight - 200;

                currentFramePoint.x = newX;
                currentFramePoint.y = newY;

                Debug.Log("Ramdom position for widget " + widget.Id + " :" + currentFramePoint.x + " " + currentFramePoint.y);

                yield return new WaitForSeconds((float) randomTime);

                UpdateWidgetLocal(widget, currentFramePoint.x, currentFramePoint.y);

                Tracking trackingMessage = new Tracking(widget.Id, currentFramePoint.x, currentFramePoint.y);
                SmartAssistanceManager.Instance.Send(trackingMessage.GetMessage());
            }

            // ReSharper disable once IteratorNeverReturns
        }

        // Measure Utils

        private void RefreshSpaceMeasures()
        {
            _resolution.x = Screen.width;
            _resolution.y = Screen.height;

//            Canvas cameraFeedParentCanvas = CameraFeedImage.GetComponentInParent<Canvas>();
            UnitySpaceToScreenSpaceRatio = _cameraFeedParentCanvas.scaleFactor;
            Rect screenSpaceCameraFeedRect = RectTransformToScreenSpace(CameraFeedImage.rectTransform, UnitySpaceToScreenSpaceRatio);
            _screenSpaceCameraFeedSize = screenSpaceCameraFeedRect.size;

            _unitySpaceToCameraFrameRatio = CameraFeedImage.rectTransform.sizeDelta.x / _currentFrameWidth;
//            UnitySpaceToScreenSpaceRatio = _cameraFeedScaleFactor;

            _screenSpaceCameraFeedVerticalOverflow = (CameraFeedImage.rectTransform.sizeDelta.y - ((RectTransform) _cameraFeedParentCanvas.transform).sizeDelta.y) * UnitySpaceToScreenSpaceRatio / 2;
        }

        private Vector3 TouchToFrameCoordinates(Vector3 touchPoint)
        {
            return new Vector3((int) (touchPoint.x / _screenSpaceCameraFeedSize.x * _currentFrameWidth), (int) ((touchPoint.y + _screenSpaceCameraFeedVerticalOverflow) / _screenSpaceCameraFeedSize.x * _currentFrameWidth), touchPoint.z);
        }

        private Vector3 FrameToTouchCoordinates(Vector3 framePoint)
        {
            return new Vector3((int) (framePoint.x * _screenSpaceCameraFeedSize.x / _currentFrameWidth), (int) (framePoint.y / _currentFrameWidth * _screenSpaceCameraFeedSize.x) - _screenSpaceCameraFeedVerticalOverflow, framePoint.z);
        }

        private Vector3 FrameToCameraFeedCoordinates(float x, float y)
        {
            x = x * _unitySpaceToCameraFrameRatio - CameraFeedImage.rectTransform.sizeDelta.x / 2;
            y = y * _unitySpaceToCameraFrameRatio - CameraFeedImage.rectTransform.sizeDelta.y / 2;
            return new Vector3((int) x, (int) y, 0);
        }

        // Given the top-left corner
        private Vector3 TrackerToCameraFeedCoordinates(float x, float y)
        {
            x = (x + ArTracker.TrackingRectWidth / 2f) * _unitySpaceToCameraFrameRatio - CameraFeedImage.rectTransform.sizeDelta.x / 2;
            y = (y + ArTracker.TrackingRectHeight / 2f) * _unitySpaceToCameraFrameRatio - CameraFeedImage.rectTransform.sizeDelta.y / 2;
            return new Vector3((int) x, (int) y, 0);
        }

        private Rect GetTrackingRect(Vector2 framePoint)
        {
            float leftOverflow = framePoint.x - ArTracker.TrackingRectWidth / 2f;
            if (leftOverflow < 0)
            {
                framePoint.x += Mathf.Abs(leftOverflow);
            }

            float topOverflow = framePoint.y - ArTracker.TrackingRectHeight / 2f;
            if (topOverflow < 0)
            {
                framePoint.y += Mathf.Abs(topOverflow);
            }

            float rightOverflow = _currentFrameWidth - (framePoint.x + ArTracker.TrackingRectHeight / 2f);
            if (rightOverflow < 0)
            {
                framePoint.x -= Mathf.Abs(rightOverflow);
            }

            float bottomOverflow = _currentFrameHeight - (framePoint.y + ArTracker.TrackingRectHeight / 2f);
            if (bottomOverflow < 0)
            {
                framePoint.y -= Mathf.Abs(bottomOverflow);
            }

            return new Rect(framePoint.x - ArTracker.TrackingRectWidth / 2f, framePoint.y - ArTracker.TrackingRectHeight / 2f, ArTracker.TrackingRectWidth, ArTracker.TrackingRectHeight);
        }

        private static Rect RectTransformToScreenSpace(RectTransform rectTransform, float scaleFactor)
        {
            Vector2 size = new Vector2(rectTransform.rect.size.x * scaleFactor, rectTransform.rect.size.y * scaleFactor);
            return new Rect((Vector2) rectTransform.position - size * 0.5f, size);
        }
    }
}