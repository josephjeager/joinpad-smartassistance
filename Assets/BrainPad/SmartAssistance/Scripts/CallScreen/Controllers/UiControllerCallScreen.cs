﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using BrainPad.SmartAssistance.Scripts.CallScreen.Behaviors;
using BrainPad.SmartAssistance.Scripts.Core.Http;
using BrainPad.SmartAssistance.Scripts.Models;
using BrainPad.SmartAssistance.Scripts.Models.Messages;
using Byn.Media;
using GameToolkit.Localization;
using SmartAssistanceSDK;
using SmartAssistanceSDK.Delegates;
using UnityEngine;
using UnityEngine.UI;

namespace BrainPad.SmartAssistance.Scripts.CallScreen.Controllers
{
    [SuppressMessage("ReSharper", "SwitchStatementMissingSomeCases")]
    [SuppressMessage("ReSharper", "MemberCanBeMadeStatic.Local")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_SimpleTypes")]
    [SuppressMessage("ReSharper", "ConvertIfStatementToConditionalTernaryExpression")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_BuiltInTypes")]
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    [SuppressMessage("ReSharper", "ConvertIfStatementToSwitchStatement")]
    [SuppressMessage("ReSharper", "InvertIf")]
    public class UiControllerCallScreen : MonoBehaviour
    {
        public ArController ArController;

        [Header("WidgetPaint Script")] public WidgetPaintBehavior WidgetPaintBehavior;

        [Header("Left Menu Containers")] public GameObject WidgetPickerContainer;
        public GameObject ColorPickerContainer;

        [Header("Widget Picker")] public GameObject ButtonPen;
        public GameObject ButtonAlert;
        public GameObject ButtonCircle;
        public GameObject ButtonArrowUp;
        public GameObject ButtonArrowDown;
        public GameObject ButtonArrowLeft;
        public GameObject ButtonArrowRight;
        public GameObject ButtonSquare;
        public GameObject ButtonTriangle;

        [Header("Colour Picker")] public GameObject ButtonGreen;
        public GameObject ButtonYellow;
        public GameObject ButtonRed;
        public GameObject ButtonBlack;
        public GameObject ButtonWhite;

        [Header("Icons")] public GameObject IconWidgetPicker;
        public GameObject IconColourPicker;
        public GameObject IconHistoryOn;
        public GameObject IconHistoryActive;
        public GameObject IconHistoryOff;

        [Header("Toggles")] public GameObject ButtonMicOn;
        public GameObject ButtonMicOff;
        public GameObject ButtonSpeakerOn;
        public GameObject ButtonSpeakerOff;
        public GameObject ButtonVideoOn;
        public GameObject ButtonVideoOff;

        [Header("History")] public GameObject HistoryContainer;
        protected internal Color32 SelectedColor;
        protected internal GameObject SelectedWidget;
        protected internal string SelectedWidgetType;
        protected internal string UiStatus;

        [Header("Panels")] public GameObject StreamingSuspendedPanel;
        public GameObject ResumeStreamingPanel;
        public GameObject DisconnectedPanel;
        public GameObject RemoteEndedCallPanel;
        public GameObject RemotePausedTheAppPanel;
        public GameObject ConfirmEndCallPanel;

        [Header("Notifications")]
        public LocalizedText OnFieldUser;
        public LocalizedText Support;
        public LocalizedText SetVideoStream;
        public LocalizedText TheSpeakers;
        public LocalizedText TheMicrophone;
        public LocalizedText Enabled;
        public LocalizedText Disabled;
        public LocalizedText IsExperiencingLowConnection;
        
        [Header("Connection Check")] public bool NotifyRemoteUserForPoorConnection = true;
        public float PoorConnectionFitness = 0.4f;
        
        // Needed to reset arrow up button on arrows menu open
        private Sprite _arrowUpSprite;
        private Sprite _arrowDownSprite;
        private Sprite _arrowLeftSprite;
        private Sprite _arrowRightSprite;

        private bool _isPickingArrow;
        private bool _isPickingColor;
        private bool _endCallRequested;
        private bool _videoEnabledByRemote = true;

        private readonly Color32 _green = new Color32(0, 255, 0, 255);
        private readonly Color32 _yellow = new Color32(255, 255, 0, 255);
        private readonly Color32 _red = new Color32(255, 0, 0, 255);
        private readonly Color32 _black = new Color32(0, 0, 0, 255);
        private readonly Color32 _white = new Color32(255, 255, 255, 255);

        // Unity Events

        private void Start()
        {
            // Widget Picker Actions
            ButtonPen.GetComponent<Button>().onClick.AddListener(OnClickPen);
            ButtonAlert.GetComponent<Button>().onClick.AddListener(OnClickAlert);
            ButtonCircle.GetComponent<Button>().onClick.AddListener(OnClickCircle);
            ButtonArrowUp.GetComponent<Button>().onClick.AddListener(OnClickArrowUp);
            ButtonArrowDown.GetComponent<Button>().onClick.AddListener(OnClickArrowDown);
            ButtonArrowLeft.GetComponent<Button>().onClick.AddListener(OnClickArrowLeft);
            ButtonArrowRight.GetComponent<Button>().onClick.AddListener(OnClickArrowRight);
            ButtonSquare.GetComponent<Button>().onClick.AddListener(OnClickSquare);
            ButtonTriangle.GetComponent<Button>().onClick.AddListener(OnClickTriangle);

            // Colour Picker Actions
            ButtonGreen.GetComponent<Button>().onClick.AddListener(() => OnClickColor(_green));
            ButtonYellow.GetComponent<Button>().onClick.AddListener(() => OnClickColor(_yellow));
            ButtonRed.GetComponent<Button>().onClick.AddListener(() => OnClickColor(_red));
            ButtonBlack.GetComponent<Button>().onClick.AddListener(() => OnClickColor(_black));
            ButtonWhite.GetComponent<Button>().onClick.AddListener(() => OnClickColor(_white));

            // Toggle Buttons Actions
            ButtonMicOn.GetComponent<Button>().onClick.AddListener(() => ToggleMic(false));
            ButtonMicOff.GetComponent<Button>().onClick.AddListener(() => ToggleMic(true));
            ButtonSpeakerOn.GetComponent<Button>().onClick.AddListener(() => ToggleSpeaker(false));
            ButtonSpeakerOff.GetComponent<Button>().onClick.AddListener(() => ToggleSpeaker(true));
            ButtonVideoOn.GetComponent<Button>().onClick.AddListener(() => ToggleVideo(false));
            ButtonVideoOff.GetComponent<Button>().onClick.AddListener(() => ToggleVideo(true));

            // Icons Actions
            IconWidgetPicker.GetComponent<Button>().onClick.AddListener(UiWidgetPicker);
            IconColourPicker.GetComponent<Button>().onClick.AddListener(UiColourPicker);
            IconHistoryOn.GetComponent<Button>().onClick.AddListener(OpenHistory);
            IconHistoryActive.GetComponent<Button>().onClick.AddListener(CloseHistory);

            // Needed to reset arrow up button on arrows menu open
            _arrowUpSprite = ButtonArrowUp.GetComponent<Image>().sprite;
            _arrowDownSprite = ButtonArrowDown.GetComponent<Image>().sprite;
            _arrowLeftSprite = ButtonArrowLeft.GetComponent<Image>().sprite;
            _arrowRightSprite = ButtonArrowRight.GetComponent<Image>().sprite;
        }

        private void OnEnable()
        {
            GetComponent<ArController>().WidgetsChangedEvent += OnWidgetsChangedEvent;
            SmartAssistanceManager.Instance.WebRtcCallEvent += OnWebRtcCallEvent;
            SmartAssistanceManager.Instance.SignalerCallEvent += OnSignalerCallEvent;
            ConnectionChecker.Instance.ConnectionCheckedEvent += OnConnectionChecked;

            SelectedWidget = null;
            SelectedWidgetType = null;

            SelectedColor = _white;

            UiWidgetPicker();
            DisableHistory();

            RefreshToggleVideo();
            RefreshToggleMic(SmartAssistanceManager.Instance.Call);

            ArController.RemoteVolumeOn = true;
            RefreshToggleSpeaker();

            ClearPanels();

            _endCallRequested = false;
            _videoEnabledByRemote = true;
        }

        private void OnDisable()
        {
            if (GetComponent<ArController>() != null)
            {
                GetComponent<ArController>().WidgetsChangedEvent -= OnWidgetsChangedEvent;
            }

            if (SmartAssistanceManager.Instance != null)
            {
                SmartAssistanceManager.Instance.WebRtcCallEvent -= OnWebRtcCallEvent;
                SmartAssistanceManager.Instance.SignalerCallEvent -= OnSignalerCallEvent;
            }

            if (ConnectionChecker.Instance != null)
            {
                ConnectionChecker.Instance.ConnectionCheckedEvent -= OnConnectionChecked;
            }

            WidgetPaintBehavior.StopPaint();
            ClearHighlight();
            ClearWidgetPickersColors();
            SelectedColor = new Color32(255, 255, 255, 255);
            SelectedWidget = null;
            SelectedWidgetType = null;
            UiStatus = "";
            ArController.RemoteVolumeOn = true;
        }

        private void OnApplicationPause(bool pauseStatus)
        {
            // This notifies both OnPause and OnResume
            bool isPaused = pauseStatus;
            ApplicationPaused applicationPaused = new ApplicationPaused(isPaused);
            SmartAssistanceManager.Instance.Send(applicationPaused.GetMessage());
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                ConfirmEndCall();
            }
        }

        // Left Panel Actions

        private void OnClickPen()
        {
            PickWidget(ArController.WidgetPaint);
        }

        private void OnClickAlert()
        {
            PickWidget(ArController.WidgetAlert);
        }

        private void OnClickCircle()
        {
            PickWidget(ArController.WidgetCircle);
        }

        private void OnClickArrowUp()
        {
            if (_isPickingArrow)
            {
                _isPickingArrow = false;
                PickWidget(ArController.WidgetArrowUp);
            }
            else
            {
                _isPickingArrow = true;
                OpenSubmenuArrow();
            }
        }

        private void OnClickArrowDown()
        {
            PickWidget(ArController.WidgetArrowDown);
        }

        private void OnClickArrowLeft()
        {
            PickWidget(ArController.WidgetArrowLeft);
        }

        private void OnClickArrowRight()
        {
            PickWidget(ArController.WidgetArrowRight);
        }

        private void OnClickSquare()
        {
            PickWidget(ArController.WidgetArrowSquare);
        }

        private void OnClickTriangle()
        {
            PickWidget(ArController.WidgetArrowTriangle);
        }

        private void OnClickColor(Color32 color32)
        {
            PickColor(color32);
        }

        // Right Panel Actions        

        private void ToggleMic(bool activate)
        {
            ArController.ToggleMic(activate);
        }

        private void ToggleSpeaker(bool activate)
        {
            ArController.ToggleSpeaker(activate);
        }

        private void ToggleVideo(bool activate)
        {
            ArController.ToggleVideo(activate);
        }

        private void PickWidget(string widgetType)
        {
            WidgetPaintBehavior.StopPaint();
            SelectedWidgetType = widgetType;

            GameObject selectedWidget = ArController.AvailableWidgets[widgetType];
            if (selectedWidget != null)
            {
                SelectedWidget = selectedWidget;
            }

            UiColourPicker();
        }

        private void PickColor(Color32 color32)
        {
            SelectedColor = color32;
            WidgetUiFeedback();
            UiWidgetPicker();
        }

        private void OpenHistory()
        {
            IconHistoryActive.SetActive(true);
            IconHistoryOff.SetActive(false);
            IconHistoryOn.SetActive(false);
            HistoryContainer.SetActive(true);
        }

        private void CloseHistory()
        {
            IconHistoryActive.SetActive(false);
            IconHistoryOff.SetActive(false);
            IconHistoryOn.SetActive(true);
            HistoryContainer.SetActive(false);
        }

        // Call Actions

        private void ConfirmEndCall()
        {
            ClearPanels();
            ConfirmEndCallPanel.SetActive(true);
        }

        // Remove this method and the end of the world will occur before nighttime,
        // you're warned. 
        public void EndCall()
        {
            _endCallRequested = true;
            SmartAssistanceManager.Instance.EndCall();
        }

        // UI Controls

        private void WidgetUiFeedback()
        {
            GameObject selectedMenuItem = null;
            switch (SelectedWidgetType)
            {
                case ArController.WidgetPaint:
                    selectedMenuItem = ButtonPen;
                    break;
                case ArController.WidgetAlert:
                    selectedMenuItem = ButtonAlert;
                    break;
                case ArController.WidgetCircle:
                    selectedMenuItem = ButtonCircle;
                    break;
                case ArController.WidgetArrowUp:
                    selectedMenuItem = ButtonArrowUp;
                    ButtonArrowUp.GetComponent<Image>().sprite = _arrowUpSprite;
                    break;
                case ArController.WidgetArrowDown:
                    selectedMenuItem = ButtonArrowDown;
                    ButtonArrowUp.GetComponent<Image>().sprite = _arrowDownSprite;
                    break;
                case ArController.WidgetArrowLeft:
                    selectedMenuItem = ButtonArrowLeft;
                    ButtonArrowUp.GetComponent<Image>().sprite = _arrowLeftSprite;
                    break;
                case ArController.WidgetArrowRight:
                    selectedMenuItem = ButtonArrowRight;
                    ButtonArrowUp.GetComponent<Image>().sprite = _arrowRightSprite;
                    break;
                case ArController.WidgetArrowSquare:
                    selectedMenuItem = ButtonSquare;
                    break;
                case ArController.WidgetArrowTriangle:
                    selectedMenuItem = ButtonTriangle;
                    break;
            }

            CloseSubmenuArrow();

            if (selectedMenuItem != null)
            {
                ClearWidgetPickersColors();

                switch (ArController.AvailableWidgets.FirstOrDefault(x => x.Value == SelectedWidget).Key)
                {
                    case ArController.WidgetArrowUp:
                    case ArController.WidgetArrowDown:
                    case ArController.WidgetArrowLeft:
                    case ArController.WidgetArrowRight:
                        ButtonArrowUp.GetComponent<Image>().color = SelectedColor;
                        break;
                    default:
                        selectedMenuItem.GetComponent<Image>().color = SelectedColor;
                        break;
                }
            }

            HighlightWidgetPicker(selectedMenuItem);
        }

        private void HighlightWidgetPicker(GameObject menuItem)
        {
            ClearHighlight();
            if (menuItem != null)
            {
                if (menuItem == ButtonArrowDown || menuItem == ButtonArrowRight || menuItem == ButtonArrowLeft)
                {
                    menuItem = ButtonArrowUp;
                }

                menuItem.transform.parent.GetChild(0).gameObject.SetActive(true);
            }
        }

        private void ClearHighlight()
        {
            ButtonPen.transform.parent.GetChild(0).gameObject.SetActive(false);
            ButtonAlert.transform.parent.GetChild(0).gameObject.SetActive(false);
            ButtonCircle.transform.parent.GetChild(0).gameObject.SetActive(false);
            ButtonArrowUp.transform.parent.GetChild(0).gameObject.SetActive(false);
            ButtonSquare.transform.parent.GetChild(0).gameObject.SetActive(false);
            ButtonTriangle.transform.parent.GetChild(0).gameObject.SetActive(false);
        }

        private void ClearWidgetPickersColors()
        {
            ButtonPen.GetComponent<Image>().color = _white;
            ButtonAlert.GetComponent<Image>().color = _white;
            ButtonCircle.GetComponent<Image>().color = _white;
            ButtonArrowUp.GetComponent<Image>().color = _white;
            ButtonArrowDown.GetComponent<Image>().color = _white;
            ButtonArrowLeft.GetComponent<Image>().color = _white;
            ButtonArrowRight.GetComponent<Image>().color = _white;
            ButtonSquare.GetComponent<Image>().color = _white;
            ButtonTriangle.GetComponent<Image>().color = _white;
        }

        private void UiWidgetPicker()
        {
            UiStatus = "WidgetPicker";

            WidgetPickerContainer.SetActive(true);
            ColorPickerContainer.SetActive(false);

            IconColourPicker.SetActive(true);
            IconWidgetPicker.SetActive(false);

            if (SelectedWidgetType == ArController.WidgetPaint)
            {
                WidgetPaintBehavior.StartPaint();
            }

            WidgetUiFeedback();
        }

        private void UiColourPicker()
        {
            UiStatus = "ColorPicker";

            WidgetPickerContainer.SetActive(false);
            ColorPickerContainer.SetActive(true);

            IconColourPicker.SetActive(false);
            IconWidgetPicker.SetActive(true);
        }

        private void ToggleHistoryVisibility()
        {
            if (_videoEnabledByRemote && HistoryContainer.GetComponent<HistoryListBehavior>().Count > 0)
            {
                EnableHistory();
            }
            else
            {
                DisableHistory();
            }
        }

        private void EnableHistory()
        {
            IconHistoryOff.SetActive(false);
            if (HistoryContainer.activeInHierarchy)
            {
                IconHistoryActive.SetActive(true);
                IconHistoryOn.SetActive(false);
            }
            else
            {
                IconHistoryActive.SetActive(false);
                IconHistoryOn.SetActive(true);
            }
        }

        private void DisableHistory()
        {
            IconHistoryActive.SetActive(false);
            IconHistoryOff.SetActive(true);
            IconHistoryOn.SetActive(false);
            HistoryContainer.SetActive(false);
        }

        private void OpenSubmenuArrow()
        {
            ButtonArrowUp.GetComponent<Image>().sprite = _arrowUpSprite;

            ButtonArrowDown.SetActive(true);
            ButtonArrowLeft.SetActive(true);
            ButtonArrowRight.SetActive(true);
        }

        private void CloseSubmenuArrow()
        {
            _isPickingArrow = false;

            ButtonArrowDown.SetActive(false);
            ButtonArrowLeft.SetActive(false);
            ButtonArrowRight.SetActive(false);
        }

        private void RefreshToggleVideo()
        {
            if (SmartAssistanceManager.Instance.IsSender)
            {
                if (ArController.VideoOn)
                {
                    ButtonVideoOn.SetActive(true);
                    ButtonVideoOff.SetActive(false);
                }

                else
                {
                    ButtonVideoOn.SetActive(false);
                    ButtonVideoOff.SetActive(true);
                }
            }
            else
            {
                ButtonVideoOn.SetActive(false);
                ButtonVideoOff.SetActive(false);
            }
        }

        private void RefreshToggleSpeaker()
        {
            if (ArController.RemoteVolumeOn)
            {
                ButtonSpeakerOn.SetActive(true);
                ButtonSpeakerOff.SetActive(false);
            }
            else
            {
                ButtonSpeakerOn.SetActive(false);
                ButtonSpeakerOff.SetActive(true);
            }
        }

        private void RefreshToggleMic(ICall call)
        {
            if (call != null)
            {
                if (call.IsMute())
                {
                    ButtonMicOn.SetActive(false);
                    ButtonMicOff.SetActive(true);
                }
                else
                {
                    ButtonMicOn.SetActive(true);
                    ButtonMicOff.SetActive(false);
                }
            }
        }

        // Wall Panels Actions

        private void Disconnected()
        {
            ClearPanels();
            DisconnectedPanel.SetActive(true);
        }

        private void RemoteEndedCall()
        {
            ClearPanels();
            RemoteEndedCallPanel.SetActive(true);
        }

        private void RemotePausedTheApp()
        {
            ClearPanels();
            RemotePausedTheAppPanel.SetActive(true);
        }
        
        private void ClearPanels()
        {
            StreamingSuspendedPanel.SetActive(false);
            ResumeStreamingPanel.SetActive(false);
            DisconnectedPanel.SetActive(false);
            RemoteEndedCallPanel.SetActive(false);
            RemotePausedTheAppPanel.SetActive(false);
            ConfirmEndCallPanel.SetActive(false);
        }

        // Ingoing Events

        private void OnWidgetsChangedEvent(object sender, List<Widget> widgets)
        {
            HistoryListBehavior historyListBehavior = HistoryContainer.GetComponent<HistoryListBehavior>();
            historyListBehavior.SetWidgets(widgets);
            // if (historyListBehavior.VisibleWidgetsCount() > 0)
            ToggleHistoryVisibility();
        }

        private void OnConnectionChecked(object sender, ConnectionChecker.Status status, float speedMbps, float fitness)
        {
            if (!NotifyRemoteUserForPoorConnection || !(fitness < PoorConnectionFitness)) return;
            PoorConnectionWarning poorConnectionWarning = new PoorConnectionWarning(fitness, speedMbps);
            SmartAssistanceManager.Instance.Send(poorConnectionWarning.GetMessage());
        }

        private void OnSignalerCallEvent(object sender, SASignalerEventArgs args)
        {
            switch (args.EventType)
            {
                case SASignalerEventType.CallStop:
                    _endCallRequested = true;
                    RemoteEndedCall();
                    break;
            }
        }

        private void OnWebRtcCallEvent(object sender, ICall call, CallEventArgs args)
        {
            if (args.Type == CallEventType.CallAccepted)
            {
                DisconnectedPanel.SetActive(false);
            }
            else if (args.Type == CallEventType.CallEnded)
            {
                if (!_endCallRequested)
                {
                    Disconnected();
                }
            }
            else if (args.Type == CallEventType.Message)
            {
                MessageEventArgs messageEventArgs = (MessageEventArgs) args;
                string message = messageEventArgs.Content;

                if (!SmartAssistanceManager.Instance.IsSender)
                {
                    if (message.StartsWith(VideoEnabled.PrefixTag))
                    {
                        VideoEnabled videoEnabled = new VideoEnabled(message);
                        string who = SmartAssistanceManager.Instance.RemoteUser.role == SAUserRole.UserOnField ? OnFieldUser.Value : Support.Value;
                        SmartAssistanceManager.Instance.DispatchNotification(who + SetVideoStream.Value + (videoEnabled.Enable ? Enabled.Value : Disabled.Value));
                    }
                }

                if (message.StartsWith(VideoEnabled.PrefixTag))
                {
                    VideoEnabled videoEnabled = new VideoEnabled(message);
                    _videoEnabledByRemote = videoEnabled.Enable;
                    ToggleHistoryVisibility();

                    if (_videoEnabledByRemote)
                    {
                        StreamingSuspendedPanel.SetActive(false);
                    }
                    else
                    {
                        StreamingSuspendedPanel.SetActive(true);
                    }
                }
                else if (message.StartsWith(SpeakersEnabled.PrefixTag))
                {
                    string who = SmartAssistanceManager.Instance.RemoteUser.role == SAUserRole.UserOnField ? OnFieldUser.Value : Support.Value;
                    SpeakersEnabled speakersEnabled = new SpeakersEnabled(message);
                    SmartAssistanceManager.Instance.DispatchNotification(who + (Localization.Instance.CurrentLanguage == SystemLanguage.Chinese ? "" : " ") + (speakersEnabled.Enable ? Enabled.Value : Disabled.Value) + TheSpeakers.Value);
                }
                else if (message.StartsWith(MicEnabled.PrefixTag))
                {
                    string who = SmartAssistanceManager.Instance.RemoteUser.role == SAUserRole.UserOnField ? OnFieldUser.Value : Support.Value;
                    MicEnabled micEnabled = new MicEnabled(message);
                    SmartAssistanceManager.Instance.DispatchNotification(who + (Localization.Instance.CurrentLanguage == SystemLanguage.Chinese ? "" : " ") + (micEnabled.Enable ? Enabled.Value : Disabled.Value) + TheMicrophone.Value);
                }
                else if (message.StartsWith(PoorConnectionWarning.PrefixTag))
                {
                    string who = SmartAssistanceManager.Instance.RemoteUser.role == SAUserRole.UserOnField ? OnFieldUser.Value : Support.Value;
                    PoorConnectionWarning poorConnectionWarning = new PoorConnectionWarning(message);
                    SmartAssistanceManager.Instance.DispatchNotification(who + IsExperiencingLowConnection.Value + poorConnectionWarning.SpeedMbps.ToString("0.000") + "Mb/s");
                }
                else if (message.StartsWith(ApplicationPaused.PrefixTag))
                {
                    ApplicationPaused applicationPaused = new ApplicationPaused(message);
                    if (applicationPaused.IsPaused)
                    {
                        RemotePausedTheApp();
                    }
                    else
                    {
                        RemotePausedTheAppPanel.SetActive(false);
                    }
                }
            }

            RefreshToggleVideo();
            RefreshToggleMic(call);
            RefreshToggleSpeaker();
        }
    }
}