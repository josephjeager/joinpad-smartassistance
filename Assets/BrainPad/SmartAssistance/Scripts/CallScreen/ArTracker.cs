﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;
using BrainPad.SmartAssistance.Scripts.CallScreen.Controllers;
using Byn.Media;
using OpenCVForUnity;
using UnityEngine;
using WebRtcCSharp;
using Rect = UnityEngine.Rect;

namespace BrainPad.SmartAssistance.Scripts.CallScreen
{
    [SuppressMessage("ReSharper", "NotAccessedField.Local")]
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    [SuppressMessage("ReSharper", "UnusedMethodReturnValue.Global")]
    [SuppressMessage("ReSharper", "UnusedMember.Local")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_BuiltInTypes")]
    [SuppressMessage("ReSharper", "SuggestVarOrType_SimpleTypes")]
    [SuppressMessage("ReSharper", "UnusedMethodReturnValue.Local")]
    [SuppressMessage("ReSharper", "SuggestBaseTypeForParameter")]
    public class ArTracker : MonoBehaviour
    {
        public const int TrackingRectWidth = 32;
        public const int TrackingRectHeight = 32;

        private static ArController _arController;

        private bool _isTracking;

        private NativeMultiTracking2D _nativeObject;
        private Dictionary<int, Matrix4x4> _trackingMatrixes;

        private Mat _frameMatrix;
        private Mat _frameMatrixPreprocessed;
        private Mat _frameMatrixRotated;
        private Mat _homographyMatrixes;

        public bool Enable;
        private bool _initialized;
        private static readonly object Lock = new object();

        private bool _firstTrackDone;

        public delegate void CbOnObjectTrackingLostDelegate(int value);

        public event CbOnObjectTrackingLostDelegate CbOnObjectTrackingLost;

        // Unity Events

        private void Update()
        {
            if (!Enable || !_initialized) return;

            ProcessFrame();
            ExtractTrackingMatrixes();

            _arController.OnFrameProcessed(_trackingMatrixes);

            _isTracking = _trackingMatrixes.Count > 0;
        }

        private void OnDisable()
        {
            if (_arController != null)
            {
                _arController.AddTrackerEvent -= OnAddTracker;
                _arController.RemoveTrackerEvent -= OnRemoveTracker;
            }

            CbOnObjectTrackingLost -= OnTrackerLost;

            ReleaseObjects();
        }

        // Tracker Controls

        public void StartTracker()
        {
            _arController = GetComponent<ArController>();

            // This method is called by ArController.OnEnable() only if the tracker is real (it can be simulated)
            // OnAddTracker and OnRemoveTracker are NOT subscribed in case of simulated tracker
            _arController.AddTrackerEvent += OnAddTracker;
            _arController.RemoveTrackerEvent += OnRemoveTracker;

            ReleaseObjects();

            _trackingMatrixes = new Dictionary<int, Matrix4x4>();
            _homographyMatrixes = new Mat(4, 3, CvType.CV_64FC1);
            _nativeObject = new NativeMultiTracking2D();

            _nativeObject.SetCameraParams(800, 600, 320, 240, SmartAssistanceManager.Instance.FrameWidth, SmartAssistanceManager.Instance.FrameHeight);
            _nativeObject.Configure();
            _nativeObject.Init();
            _nativeObject.Start();

            CbOnObjectTrackingLost += OnTrackerLost;
            _nativeObject.SetCbOnObjectTrackingLost(CbOnObjectTrackingLost);

            // Workaround
            _nativeObject.InitDataStructuresWorkaraound();

            Enable = true;
            _initialized = true;
        }

        public void StopTracker()
        {
            ReleaseObjects();
        }

        private bool _isPortrait;

        public void UpdateFrame(FrameUpdateEventArgs frameUpdateEventArgs)
        {
            lock (Lock)
            {
                if (_frameMatrix == null)
                    _frameMatrix = new Mat(frameUpdateEventArgs.Frame.Height, frameUpdateEventArgs.Frame.Width, CvType.CV_8UC4);

                if (frameUpdateEventArgs.Frame.Width != _frameMatrix.cols() || frameUpdateEventArgs.Frame.Height != _frameMatrix.rows())
                    _frameMatrix = new Mat(frameUpdateEventArgs.Frame.Height, frameUpdateEventArgs.Frame.Width, CvType.CV_8UC4);

// Workaround needed to properly rotate the camera according to the device rotation on iOS devices
//#if UNITY_IOS && !UNITY_EDITOR
//                if (_frameMatrixRotated == null)
//                    _frameMatrixRotated = new Mat(frameUpdateEventArgs.Frame.Width, frameUpdateEventArgs.Frame.Height, CvType.CV_8UC4);
//
//                if (frameUpdateEventArgs.Frame.Height != _frameMatrixRotated.cols() || frameUpdateEventArgs.Frame.Width != _frameMatrixRotated.rows())
//                    _frameMatrixRotated = new Mat(frameUpdateEventArgs.Frame.Width, frameUpdateEventArgs.Frame.Height, CvType.CV_8UC4);
//
//                if (frameUpdateEventArgs.Frame.Width < frameUpdateEventArgs.Frame.Height)
//                {
//                    _isPortrait = true;
//
//                    if (SmartAssistanceManager.Instance.DeviceRotationHolder.transform.eulerAngles.z < 360 && SmartAssistanceManager.Instance.DeviceRotationHolder.transform.eulerAngles.z > 180)
//                    {
//                        // Portrait 
//                        OpenCVForUnity.Core.transpose(_frameMatrix, _frameMatrixRotated);
//                        OpenCVForUnity.Core.flip(_frameMatrixRotated, _frameMatrixRotated, 0);
//                        _frameMatrix = _frameMatrixRotated;
//                    }
//                    else
//                    {
//                        // Inverse Portrait
//                        OpenCVForUnity.Core.transpose(_frameMatrix, _frameMatrixRotated);
//                        OpenCVForUnity.Core.flip(_frameMatrixRotated, _frameMatrixRotated, 0);
//                        _frameMatrix = _frameMatrixRotated;
//                    }
//                }
//#endif 

                if (_frameMatrixPreprocessed == null)
                    _frameMatrixPreprocessed = new Mat(frameUpdateEventArgs.Frame.Height, frameUpdateEventArgs.Frame.Width, CvType.CV_8UC4);

                if (frameUpdateEventArgs.Frame.Width != _frameMatrixPreprocessed.cols() || frameUpdateEventArgs.Frame.Height != _frameMatrixPreprocessed.rows())
                    _frameMatrixPreprocessed = new Mat(frameUpdateEventArgs.Frame.Height, frameUpdateEventArgs.Frame.Width, CvType.CV_8UC4);

                Utils.fastBytesToMat(frameUpdateEventArgs.Frame.Buffer, _frameMatrix);
//                 Core.flip(_frameMatrix, _frameMatrix, 0);

                if (_initialized)
                {
                    Imgproc.cvtColor(_frameMatrix, _frameMatrixPreprocessed, Imgproc.COLOR_RGBA2GRAY);
                    // Imgproc.flip()
                }
            }
        }

        private void ProcessFrame()
        {
            if (_frameMatrixPreprocessed == null) return;
            lock (Lock)
            {
                _nativeObject.ProcessFrame(_frameMatrixPreprocessed);
                _nativeObject.GetHomographies(_homographyMatrixes);
            }
        }

        private void ExtractTrackingMatrixes()
        {
            _trackingMatrixes.Clear();
            for (int i = 0; i < _homographyMatrixes.rows() / 4; i++)
            {
                int maskedObjectId = (int) _homographyMatrixes.get(i * 4, 0)[0];
                int objectId = FromMaskedId(maskedObjectId);

                Matrix4x4 rtMatrix4X4 = new Matrix4x4();
                rtMatrix4X4.SetRow(0, new Vector4((float) _homographyMatrixes.get(i * 4 + 1, 0)[0], (float) _homographyMatrixes.get(i * 4 + 1, 1)[0], (float) _homographyMatrixes.get(i * 4 + 1, 2)[0], 0));
                rtMatrix4X4.SetRow(1, new Vector4((float) _homographyMatrixes.get(i * 4 + 2, 0)[0], (float) _homographyMatrixes.get(i * 4 + 2, 1)[0], (float) _homographyMatrixes.get(i * 4 + 2, 2)[0], 0));
                rtMatrix4X4.SetRow(2, new Vector4((float) _homographyMatrixes.get(i * 4 + 3, 0)[0], (float) _homographyMatrixes.get(i * 4 + 3, 1)[0], (float) _homographyMatrixes.get(i * 4 + 3, 2)[0], 0));
                rtMatrix4X4.SetRow(3, new Vector4(0, 0, 0, 1));

//                Matrix4x4 rtMatrix4X4Rotated = rtMatrix4X4;
//#if UNITY_IOS && ! UNITY_EDITOR
//                    if (frameArgs.Frame.Width < frameArgs.Frame.Height)
//                if (_isPortrait)
//                {
//                    if (SmartAssistanceManager.Instance.DeviceRotationHolder.transform.eulerAngles.z < 360 && SmartAssistanceManager.Instance.DeviceRotationHolder.transform.eulerAngles.z > 180)
//                    {
//                        // Portrait
//                        Quaternion rotation = Quaternion.Euler(0, 0, 90);
//                        Matrix4x4 rotationMat = Matrix4x4.Rotate(rotation);
//                        rtMatrix4X4Rotated = rtMatrix4X4 * rotationMat;
//                    }
//                    else
//                    {
//                        // Inverse Portrait
//                        Quaternion rotation = Quaternion.Euler(0, 0, 270);
//                        Matrix4x4 rotationMat = Matrix4x4.Rotate(rotation);
//                        rtMatrix4X4Rotated = rtMatrix4X4 * rotationMat;
//                    }
//                }
//                else
//                {
//                    if (SmartAssistanceManager.Instance.DeviceRotationHolder.transform.eulerAngles.z < 270 && SmartAssistanceManager.Instance.DeviceRotationHolder.transform.eulerAngles.z > 90)
//                    {
//                        // Landscape Left 
//                        Quaternion rotation = Quaternion.Euler(0, 0, 180);
//                        Matrix4x4 rotationMat = Matrix4x4.Rotate(rotation);
//                        rtMatrix4X4Rotated = rtMatrix4X4 * rotationMat;
//                    }
//                    else
//                    {
//                        // Landscape Right 
//                    }
//                }
//                #endif

                if (!_trackingMatrixes.ContainsKey(objectId))
                {
                    _trackingMatrixes.Add(objectId, rtMatrix4X4);
                }
            }
        }

        // Ingoing Events

        private bool OnAddTracker(object sender, int widgetId, Rect trackingRect)
        {
            bool trackerAddedSuccesfully;
            lock (Lock)
            {
                if (0 <= trackingRect.xMin && 0 <= trackingRect.xMax - trackingRect.xMin && trackingRect.xMax <= _frameMatrixPreprocessed.cols() && 0 <= trackingRect.yMin && 0 <= trackingRect.yMax - trackingRect.yMin && trackingRect.yMax <= _frameMatrixPreprocessed.rows())
                {
                    int ret = _nativeObject.AddNewObjectToTrack(widgetId, _frameMatrixPreprocessed, (int) trackingRect.xMin, (int) trackingRect.yMin, (int) trackingRect.xMax, (int) trackingRect.yMax);
                    trackerAddedSuccesfully = ret == 0;
                }
                else
                {
                    trackerAddedSuccesfully = false;
                }
            }

            return trackerAddedSuccesfully;
        }

        private void OnRemoveTracker(object sender, int widgetId)
        {
            lock (Lock)
            {
                _nativeObject.RemoveTrackedObject(ToMaskedId(widgetId));
            }
        }

        [MonoPInvokeCallback(typeof(CbOnObjectTrackingLostDelegate))]
        private static void OnTrackerLost(int widgetId)
        {
            _arController.OnTrackerLost(FromMaskedId(widgetId));
        }

        // Memory Cleanup

        private void ReleaseObjects()
        {
            lock (Lock)
            {
                _initialized = false;

                if (_nativeObject != null)
                {
                    _nativeObject.Destroy();
                    _nativeObject = null;
                }

                if (_homographyMatrixes != null)
                {
                    _homographyMatrixes.release();
                    _homographyMatrixes = null;
                }

                if (_frameMatrixPreprocessed != null)
                {
                    _frameMatrixPreprocessed.release();
                    _frameMatrixPreprocessed = null;
                }

                _isTracking = false;
            }
        }

        // Sapphire ID Conversion

        private static int ToMaskedId(int id)
        {
            return id * 1000 + 10;
        }

        private static int FromMaskedId(int maskedId)
        {
            return (maskedId - 10) / 1000;
        }

        // Native Object Wrapper

        private class NativeMultiTracking2D
        {
            private readonly IntPtr _nativeObjAddr;

            public NativeMultiTracking2D()
            {
                _nativeObjAddr = nativeMultiTracking2D_Create();
            }

            public void SetCameraParams(int cameraWidth, int cameraHeight, float horizontalViewAngle, float verticalViewAngle)
            {
                nativeMultiTracking2D_SetCameraParams(_nativeObjAddr, cameraWidth, cameraHeight, horizontalViewAngle, verticalViewAngle);
            }

            public void SetCameraParams(double fx, double fy, double cx, double cy, int width, int height)
            {
                nativeMultiTracking2D_SetCameraParams(_nativeObjAddr, fx, fy, cx, cy, width, height);
            }

            public int AddNewObjectToTrack(int objectId, Mat frame, int left, int top, int right, int bottom)
            {
                return nativeMultiTracking2D_AddNewObjectToTrack(_nativeObjAddr, frame.nativeObj, objectId, left, top, right, bottom);
            }

            public void Init()
            {
                nativeMultiTracking2D_Init(_nativeObjAddr);
            }

            public void Configure()
            {
                nativeMultiTracking2D_Configure(_nativeObjAddr);
            }

            public void Start()
            {
                nativeMultiTracking2D_Start(_nativeObjAddr);
            }

            public void InitDataStructuresWorkaraound()
            {
                // During the first added tracker Sapphire initializes some data structures causing delay,
                // Adding an object and imediately remove it forces the plugin to init its data structures immediately
                Mat frameMatrix = new Mat(SmartAssistanceManager.Instance.FrameHeight, SmartAssistanceManager.Instance.FrameWidth, CvType.CV_8UC1);
                const int widgetId = 0;
                AddNewObjectToTrack(0, frameMatrix, (frameMatrix.rows() - TrackingRectWidth) / 2, (frameMatrix.rows() - TrackingRectHeight) / 2, (frameMatrix.rows() + TrackingRectWidth) / 2, (frameMatrix.rows() + TrackingRectHeight) / 2);
                RemoveTrackedObject(ToMaskedId(widgetId));
            }

            public void Stop()
            {
                nativeMultiTracking2D_Stop(_nativeObjAddr);
            }

            public int ProcessFrame(Mat imageRgba)
            {
                return nativeMultiTracking2D_ProcessFrame(_nativeObjAddr, imageRgba.nativeObj);
            }

            public int GetHomographies(Mat homographiesMatrixOut)
            {
                return nativeMultiTracking2D_GetHomographies(_nativeObjAddr, homographiesMatrixOut.nativeObj);
            }

            public int SetCbOnObjectTrackingLost(CbOnObjectTrackingLostDelegate cBOnObjectTrackingLost)
            {
                return nativeMultiTracking2D_SetCBOnObjectTrackingLost(_nativeObjAddr, cBOnObjectTrackingLost);
            }

            public int RemoveTrackedObject(int objectId)
            {
                return nativeMultiTracking2D_RemoveTrackedObject(_nativeObjAddr, objectId);
            }

            public void Destroy()
            {
                nativeMultiTracking2D_Destroy(_nativeObjAddr);
            }

#if (UNITY_IOS || UNITY_WEBGL) && !UNITY_EDITOR
            private const string Libname = "__Internal";
#else
            private const string Libname = "Sapphire";
#endif

            [DllImport(Libname)]
            private static extern IntPtr nativeMultiTracking2D_Create();

            [DllImport(Libname)]
            private static extern void nativeMultiTracking2D_Destroy(IntPtr nativeObjAddr);

            [DllImport(Libname)]
            private static extern int nativeMultiTracking2D_AddNewObjectToTrack(IntPtr nativeObjAddr, IntPtr frame, int objectId, int left, int top, int right, int bottom);

            [DllImport(Libname)]
            private static extern int nativeMultiTracking2D_Init(IntPtr nativeObjAddr);

            [DllImport(Libname)]
            private static extern int nativeMultiTracking2D_SetCameraParams(IntPtr nativeObjAddr, int width, int height, float hFov, float vFov);

            [DllImport(Libname)]
            private static extern int nativeMultiTracking2D_SetCameraParams(IntPtr nativeObjAddr, double fx, double fy, double cx, double cy, int width, int height);

            [DllImport(Libname)]
            private static extern int nativeMultiTracking2D_Configure(IntPtr nativeObjAddr);

            [DllImport(Libname)]
            private static extern int nativeMultiTracking2D_Start(IntPtr nativeObjAddr);

            [DllImport(Libname)]
            private static extern int nativeMultiTracking2D_Stop(IntPtr nativeObjAddr);

            [DllImport(Libname)]
            private static extern int nativeMultiTracking2D_ProcessFrame(IntPtr nativeObjAddr, IntPtr rImage);

            [DllImport(Libname)]
            private static extern int nativeMultiTracking2D_GetHomographies(IntPtr nativeObjAddr, IntPtr rHomographiesOut);

            [DllImport(Libname)]
            private static extern int nativeMultiTracking2D_SetCBOnObjectTrackingLost(IntPtr nativeObjAddr, CbOnObjectTrackingLostDelegate cBOnObjectTrackingLost);

            [DllImport(Libname)]
            private static extern int nativeMultiTracking2D_RemoveTrackedObject(IntPtr nativeObjAddr, int id);

            [DllImport(Libname)]
            private static extern int nativeMultiTracking2D_GetImage(IntPtr nativeObjAddr, IntPtr rImageOut);

            [DllImport(Libname)]
            private static extern int nativeMultiTracking2D_SetImage(IntPtr nativeObjAddr, IntPtr rImage);
        }
    }
}