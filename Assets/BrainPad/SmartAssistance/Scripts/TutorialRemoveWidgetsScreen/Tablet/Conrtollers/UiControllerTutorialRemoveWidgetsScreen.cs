﻿using System.Diagnostics.CodeAnalysis;
using UnityEngine;

namespace BrainPad.SmartAssistance.Scripts.TutorialRemoveWidgetsScreen.Tablet.Conrtollers
{
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    public class UiControllerTutorialRemoveWidgetsScreen : MonoBehaviour
    {
        public GameObject Step1;
        public GameObject Step2;
        public GameObject Step3;
        public GameObject Step4;

        private void OnEnable()
        {
            OpenStep1();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                SmartAssistanceManager.Instance.ContactsScreen();
            }
        }

        public void Home()
        {
            SmartAssistanceManager.Instance.ContactsScreen();
        }

        public void OpenStep1()
        {
            Clear();
            Screen.orientation = ScreenOrientation.LandscapeLeft;
            Step1.SetActive(true);
        }

        public void OpenStep2()
        {
            Clear();
            Screen.orientation = ScreenOrientation.LandscapeLeft;
            Step2.SetActive(true);
        }

        public void OpenStep3()
        {
            Clear();
            Screen.orientation = ScreenOrientation.LandscapeLeft;
            Step3.SetActive(true);
        }

        public void OpenStep4()
        {
            Clear();
            Screen.orientation = ScreenOrientation.LandscapeLeft;
            Step4.SetActive(true);
        }

        private void Clear()
        {
            Step1.SetActive(false);
            Step2.SetActive(false);
            Step3.SetActive(false);
            Step4.SetActive(false);
        }
    }
}