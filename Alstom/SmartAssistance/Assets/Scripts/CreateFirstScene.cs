﻿using UnityEngine;
using UnityEngine.SceneManagement;
using HoloToolkit.Unity.InputModule;

public class CreateFirstScene : MonoBehaviour
{
    public string sceneName;

    private void Start()
    {
        SceneManager.LoadScene(sceneName);
    }
}
