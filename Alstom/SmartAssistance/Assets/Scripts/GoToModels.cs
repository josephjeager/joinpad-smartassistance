﻿using UnityEngine;
using UnityEngine.SceneManagement;
using HoloToolkit.Unity.InputModule;
using HoloToolkit.Unity;

public class GoToModels : MonoBehaviour, IInputClickHandler
{
    public static string userRole = "";

    private void Start()
    {
        
    }

    public void OnInputClicked(InputEventData eventData)
    {
        if (SceneManager.GetActiveScene().name == "Roles")
        {
            switch (gameObject.name)
            {
                case "OrafoBtn":
                    userRole = "orafo";
                    break;
                case "PulitoreBtn":
                    userRole = "pulitore";
                    break;
            }
        }

        if (SceneManager.GetActiveScene().name == "Steps")
        {
            StepsMenuNavigation.currentPage = 0;
            OpenStep.currentOpenedStep = "";
            //HandsTrackingManager.Action = "";
        }

        SceneManager.LoadScene("Models");
    }
}
