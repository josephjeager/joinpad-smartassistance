﻿using UnityEngine;
using UnityEngine.SceneManagement;
using HoloToolkit.Unity.InputModule;

public class GoToScene : MonoBehaviour, IInputClickHandler
{
    public string sceneName;

    private void Start()
    {

    }

    public void OnInputClicked(InputEventData eventData)
    {
        SceneManager.LoadScene(sceneName);
    }
}
