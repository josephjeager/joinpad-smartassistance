﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RemovePanel : MonoBehaviour, IInputClickHandler
{
    public string sceneName;

    void Start ()
    {
		
	}
	
	void Update ()
    {
		
	}

    public void OnInputClicked(InputEventData eventData)
    {
        SceneManager.UnloadSceneAsync(sceneName);
    }
}