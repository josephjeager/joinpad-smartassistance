﻿using UnityEngine;
using HoloToolkit.Unity.InputModule;
using System.Collections.Generic;

public class StepsMenuNavigation : MonoBehaviour, IInputClickHandler
{
    public static int currentPage = 0;
    private List<List<List<string>>> stepsArrayForRole;
    List<GameObject> menuBtns;

    List<List<List<string>>> orafoPages = new List<List<List<string>>>()
    {
        new List<List<string>>()
        {
            new List<string>()
            {"COD 000 CONTROLLO KIT", "none", "Step01"},
            new List<string>()
            {"COD 000 CONTROLLO SEMILAV", "none", "Step02"},
            new List<string>()
            {"MC 0100 MARCHIATURA", "MC", "Step03"},
            new List<string>()
            {"ASS 0100 SRAMATURA", "ASS", "Step04"},
            new List<string>()
            {"ASS 0100 CONTROLLO PRE SALD", "ASS", "Step05"}
        },
        new List<List<string>>()
        {
            new List<string>()
            {"ASS 0100 DEPOSIZIONE SALD", "ASS", "Step06"},
            new List<string>()
            {"ASS 0100 ASSEMBLAGGIO PARTI", "ASS", "Step07"},
            new List<string>()
            {"ASS 0100 LEGATURA COMPRESS", "ASS", "Step08"},
            new List<string>()
            {"ASS 0100 LEGATURA ESPANSIONE", "ASS", "Step09"},
            new List<string>()
            {"ASS 0100 CONTROLLO POST SALD", "ASS", "Step10a"}
        },
        new List<List<string>>()
        {
            new List<string>()
            {"MC 300 VERIFICA SIMMETRIA", "MC", "Step10b"},
            new List<string>()
            {"MC 300 VERIFICA MISURA", "MC", "Step11"},
            new List<string>()
            {"MC 300 SERIALIZZAZIONE", "MC", "Step12"},
            new List<string>()
            {"", "none", ""},
            new List<string>()
            {"", "none", ""}
        }

    };

    List<List<List<string>>> pulitorePages = new List<List<List<string>>>()
    {
        new List<List<string>>()
        {
            new List<string>()
            {"PS 0400 SGROSSATURA INTERNA", "PS", "Step13"},
            new List<string>()
            {"PS 0400 SGROSSATURA ESTERNA", "PS", "Step14"},
            new List<string>()
            {"PS 0400 LUCIDATURA ESTERNA", "PS", "Step15"},
            new List<string>()
            {"", "none", ""},
            new List<string>()
            {"", "none", ""}
        }
    };

    private void Start()
    {
        switch (GoToModels.userRole)
        {
            case "orafo":
                stepsArrayForRole = orafoPages;
                break;
            case "pulitore":
                stepsArrayForRole = pulitorePages;
                break;
        }

        menuBtns = new List<GameObject>()
        {
            GameObject.Find("MenuBtn0"),
            GameObject.Find("MenuBtn1"),
            GameObject.Find("MenuBtn2"),
            GameObject.Find("MenuBtn3"),
            GameObject.Find("MenuBtn4")
        };

        populateMenuVoices(0);
    }

    public void OnInputClicked(InputEventData eventData)
    {
        if (gameObject.transform.name == "UpBtn")
        {
            if (currentPage > 0)
            {
                currentPage = currentPage - 1;
            }
            else
            {
                return;
            }
        }

        if (gameObject.transform.name == "DownBtn")
        {
            if (currentPage < (stepsArrayForRole[0].Count - 1))
            {
                currentPage = currentPage + 1;
            }
            else
            {
                return;
            }
        }

        switch (currentPage)
        {
            case 0:
                populateMenuVoices(0);
                break;
            case 1:
                populateMenuVoices(1);
                break;
            case 2:
                populateMenuVoices(2);
                break;
        }
    }

    public void populateMenuVoices(int page)
    {
        for (int i = 0; i < 5; i++)
        {
            menuBtns[i].GetComponentInChildren<TextMesh>().text = stepsArrayForRole[page][i][0];
            menuBtns[i].GetComponentInChildren<TextMesh>().fontStyle = FontStyle.Normal;
            menuBtns[i].GetComponentInChildren<SpriteRenderer>().color = new Color(1, 1, 1, 0);

            switch (stepsArrayForRole[page][i][1])
            {
                case "MC":
                    menuBtns[i].GetComponentInChildren<Renderer>().material.color = new Color(1, 0.6f, 0.8f);
                    break;
                case "ASS":
                    menuBtns[i].GetComponentInChildren<Renderer>().material.color = new Color(0.8f, 1, 0.8f);
                    break;
                case "PS":
                    menuBtns[i].GetComponentInChildren<Renderer>().material.color = new Color(1, 1, 0.6f);
                    break;
                case "none":
                    menuBtns[i].GetComponentInChildren<Renderer>().material.color = new Color(0, 0, 0);
                    break;
            }

            menuBtns[i].GetComponent<OpenStep>().stepReference = stepsArrayForRole[page][i][2];
        }
    }
}
