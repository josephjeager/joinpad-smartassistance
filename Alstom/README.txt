- Versione per esecuzione in Locale

    Il Progetto è composto dalle seguenti Cartelle :
	
		ffmpeg = script relativi all'esecuzione della libreria che permette lo streaming video 
		jsmpeg = script che aprono le socket per inserimento widget e invio ricezione streaming  
		AlstomSubscriber = sito web per controllo remoto Hololens
		SmartAssistance = applicazione Hololens Unity/VisualStudio 
	
	Per l' esecuzione in locale accedere alle cartelle precedentemente descritte e eseguire i comandi che seguono:
		jsmpeg :
			1) node widgetServer.js
			2) node stream-server.js pippo ( pippo è la password ;) )
		SmartAssistance:
			1) aprire la soluzione gia creata da Unity nella cartella App
			2) aprire il file WebSocket.cs e cercare la funzione ConnectAsync
			3) modificare la riga [ myUri = new Uri("ws://server:8085"); ] e modificare la striga server con l'indirizzio ip della macchina locale
		AlstomSubscriber:
			1) aprire la soluzione in visualstudio 
		    2) aprire il file logged.jade
			3) modificare la stringa var client = new WebSocket( 'ws://serverip:8084/' ); con l'indirizzo del server locale
			4) modificare la stringa var ws = new WebSocket("ws://serverip:8086"); con l'indirizzo del server locale
			5) accedere alla cartella AlstomSubscriber -> bin eseguire node www
		ffmpeg:
			1) aprire run.bat impostare ip Hololens e ip server per streaming
			2) eseguire run.bat

- Versione Online
	ffmpeg:
			aprire run.bat impostare ip Hololens e ip server per streaming
			eseguire run.bat
			
	SmartAssistance:
			1) aprire la soluzione gia creata da Unity nella cartella App
			2) aprire il file WebSocket.cs e cercare la funzione ConnectAsync
			3) modificare la riga [ myUri = new Uri("ws://server:8085"); ] e modificare la striga server con l'indirizzio ip della macchina locale
	
	Aprire il sito web
		http://alstom.azurewebsites.net/
		
		
GOOD WORK SMART ASSISTANCE