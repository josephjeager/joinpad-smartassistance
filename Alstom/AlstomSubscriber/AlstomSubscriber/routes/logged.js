﻿var express = require('express');
var router = express.Router();
var multer = require('multer');

/* GET home page. */
router.get('/', function (req, res) {
    if (typeof loggedIn != 'undefined' && loggedIn)
        res.render('logged', { title: 'Express' });
    else
        res.redirect('/');
});

var storage = multer.diskStorage({
    destination: function (request, file, callback) {
        callback(null, 'uploads');
    },
    filename: function (request, file, callback) {
        console.log(file);
        callback(null, file.originalname)
    }
});

var upload = multer({ storage: storage }).single('photo');

router.post('/upload', function (request, response) {
    upload(request, response, function (err) {
        if (err) {
            console.log('Error Occured' + err);
            return;
        }
        console.log(request.file);
        //response.end('Your File Uploaded');
        response.send('Your File Uploaded');
    })
});

module.exports = router;