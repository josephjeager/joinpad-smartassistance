@ECHO OFF
REM This script requires admin privileges.
sc create "SmartAssistanceServer" binPath="%~dp0bin\sa_server.exe" start=auto
sc start "SmartAssistanceServer"
PAUSE