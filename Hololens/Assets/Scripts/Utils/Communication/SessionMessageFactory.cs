﻿public class SessionMessageFactory : CommandMessageFactory
{

    public SessionMessageFactory()
    {
        this.dispatcher = Dispatcher.SESSION;
    }

    public CommandMessage CallBack(string command)
    {
        return new CommandMessage()
        {
            dispatcher = this.dispatcher,
            command = command
        };
    }

}
