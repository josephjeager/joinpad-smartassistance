﻿public class DocumentMessageFactory : CommandMessageFactory
{

    public DocumentMessageFactory()
    {
        this.dispatcher = Dispatcher.DOCUMENTS;
    }

    public CommandMessage CallBack(string command)
    {
        return new CommandMessage()
        {
            dispatcher = this.dispatcher,
            command = command
        };
    }

}
