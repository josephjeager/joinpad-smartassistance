﻿public class ImageMessageFactory : CommandMessageFactory
{

    public ImageMessageFactory()
    {
        this.dispatcher = Dispatcher.IMAGES;
    }

    public CommandMessage CallBack(string command)
    {
        return new CommandMessage()
        {
            dispatcher = this.dispatcher,
            command = command
        };
    }

}
