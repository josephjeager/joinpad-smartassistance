﻿public class TextMessageFactory : CommandMessageFactory
{

    public TextMessageFactory()
    {
        this.dispatcher = Dispatcher.TEXTS;
    }

    public CommandMessage CallBack(string command)
    {
        return new CommandMessage()
        {
            dispatcher = this.dispatcher,
            command = command
        };
    }

}
