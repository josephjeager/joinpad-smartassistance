﻿public class VideoMessageFactory : CommandMessageFactory
{

    public VideoMessageFactory()
    {
        this.dispatcher = Dispatcher.VIDEOS;
    }

    public CommandMessage CallBack(string command)
    {
        return new CommandMessage()
        {
            dispatcher = this.dispatcher,
            command = command
        };
    }

}
