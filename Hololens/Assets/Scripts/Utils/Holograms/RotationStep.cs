﻿
public class RotationStep : IStep {

    private readonly int _step;

    public RotationStep(int step)
    {
        _step = step;
    }

    public float GetEffectiveStep()
    {
        return _step % 360;
    }

}
