﻿using System;
using System.Collections.Generic;

[Serializable]
public class TextsStore
{
    public List<TextRecord> textRecords;

    public TextsStore()
    {
        textRecords = new List<TextRecord>();
    }
}

[Serializable]
public class TextRecord
{
    public string id;
    public Colors.Name colorName;
    public string text;
}