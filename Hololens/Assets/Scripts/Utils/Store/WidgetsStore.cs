﻿using System;
using System.Collections.Generic;

[Serializable]
public class WidgetsStore {

    public List<WidgetRecord> widgetRecords;

    public WidgetsStore()
    {
        widgetRecords = new List<WidgetRecord>();
    }

}

[Serializable]
public class WidgetRecord
{
    public string id;
    public Colors.Name colorName;
    public string type;
    public string text;
}