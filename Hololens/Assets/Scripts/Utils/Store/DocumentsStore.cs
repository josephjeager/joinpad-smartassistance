﻿using System;
using System.Collections.Generic;

[Serializable]
public class DocumentsStore
{
    public List<DocumentRecord> documentRecords;

    public DocumentsStore()
    {
        documentRecords = new List<DocumentRecord>();
    }
}

[Serializable]
public class DocumentRecord
{
    public string id;
    public int pagesNumber;
    public string url;
    public float bookmark;
}