﻿
public enum Dispatcher
{
    WIDGETS,
    TEXTS,
    IMAGES,
    DOCUMENTS,
    VIDEOS,
    SESSION
}
