﻿using System.Collections.Generic;
using UnityEngine;

public class Colors {

    public enum Name
    {
        WHITE,
        RED,
        YELLOW,
        GREEN,
        BLUE
    }

    private static Colors _instance;
    private readonly Dictionary<Name, Color32> _colors;

    public Colors()
    {
        _colors = new Dictionary<Name, Color32>
            {
                { Name.WHITE, new Color32(255, 255, 255, 255) },
                { Name.RED, new Color32(255, 0, 0, 255) },
                { Name.YELLOW, new Color32(255, 255, 0, 255) },
                { Name.GREEN, new Color32(0, 255, 0, 255) },
                { Name.BLUE, new Color32(0, 0, 255, 255) },
            };
    }

    public Color32 GetColor(Name name)
    {
        return _colors[name];
    }

    public static Colors GetInstance()
    {
        return _instance ?? (_instance = new Colors());
    }

}