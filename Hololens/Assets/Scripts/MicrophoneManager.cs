﻿using System;

public class MicrophoneManager : IMicrophoneManager
{

    private bool _isMute;

    public MicrophoneManager()
    {
        _isMute = false;
    }  

    /* Public Methods */

    public void SwitchState(Action onMute, Action onUnmute)
    {

        if (_isMute)
        {
            _isMute = false;
            onMute.Invoke();
        } else
        {
            _isMute = true;
            onUnmute.Invoke();
        }
    }

}
