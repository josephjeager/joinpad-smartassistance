﻿using HoloToolkit.UI.Keyboard;
using UnityEngine;

public class KeyboardManager : IKeyboardManager {

    /* Private Properties */

    private readonly GameObject _keyboard;
    private GameObject Keyboard
    {
        get
        {
            return _keyboard;
        }
    }

    /* Constructor */

    public KeyboardManager(GameObject keyboard)
    {
        _keyboard = keyboard;
    }

    /* Public Methods */

    public void CloseKeyboard()
    {
        Keyboard.GetComponent<Keyboard>().Close();
    }

}
