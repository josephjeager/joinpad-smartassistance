﻿using HoloToolkit.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.XR.WSA;
using UnityEngine.XR.WSA.Persistence;

public class ImagesManager : IImagesManager {

    private Action<string> _onImageLoaded;
    public Action<string> OnImageLoaded
    {
        set { _onImageLoaded = value; }
    }

    private Action<string, RawImage> _onImageInstantiated;
    public Action<string, RawImage> OnImageInstantiated
    {
        set { _onImageInstantiated = value; }
    }

    private Action<Guid, string> _onImageCreated;
    public Action<Guid, string> OnImageCreated
    {
        set { _onImageCreated = value; }
    }

    private Action<string> _onImageDestroyed;
    public Action<string> OnImageDestroyed
    {
        set { _onImageDestroyed = value; }
    }

    private readonly GameObject _imagesContainer;
    private GameObject ImagesContainer
    {
        get
        {
            return _imagesContainer;
        }
    }

    private GameObject _verticalImage;
    private GameObject VerticalImage
    {
        get
        {
            return _verticalImage ?? (_verticalImage = ImagesContainer.transform.Find("VerticalImage").gameObject);
        }
    }

    private GameObject _horizontalImage;
    private GameObject HorizontalImage
    {
        get
        {
            return _horizontalImage ?? (_horizontalImage =
                       ImagesContainer.transform.Find("HorizontalImage").gameObject);
        }
    }

    private readonly ISpatialManager _spatialManager;
    private readonly ISettings _settings;
    private readonly Dictionary<Guid, GameObject> _images;
    private int _imagesCounter;
    private GameObject _selectedImage;
    private GameObject _catchedImage;

    private ImagesStore _imagesStore;
    private readonly WorldAnchorStoreContainer _anchorStoreContainer;
    private WorldAnchorStore AnchorStore
    {
        get
        {
            return _anchorStoreContainer.AnchorStore;
        }
    }

    public ImagesManager(GameObject imagesContainer, ISpatialManager spatialManager, ISettings settings, WorldAnchorStoreContainer anchorStoreContainer)
    {
        _imagesContainer = imagesContainer;
        _spatialManager = spatialManager;
        _settings = settings;
        _anchorStoreContainer = anchorStoreContainer;
        _images = new Dictionary<Guid, GameObject>();
        _imagesCounter = 0;
        _imagesStore = new ImagesStore();
    }

    [Serializable]
    private class ImageIncomingCommand
    {
        public HoloCommand command;
        public int step;
        public Direction direction;
        public string imageId;
        public string url;
        public ReferenceSystem referenceSystem;
    }

    [Serializable]
    private class ImageCallBackCommand
    {
        public HoloCommand command;
        public int index;
        public string id;
    }

    public void ReadCommand(string command)
    {
        bool needToSave = false;
        var imageMessage = JsonUtility.FromJson<ImageIncomingCommand>(command);
        var imageId = new Guid(imageMessage.imageId);
        switch (imageMessage.command)
        {
            case HoloCommand.CLEAR:
                ClearImages();
                needToSave = true;
                break;
            case HoloCommand.COORDINATES:
                CreateImage(imageMessage.direction, imageMessage.url);
                needToSave = true;
                break;
            case HoloCommand.ACTIVE_DEACTIVE:
                RemoveImage(imageId);
                needToSave = true;
                break;
            case HoloCommand.UP:
                SetImageCoordinatesW(HoloCommand.UP, new MovementStep(imageMessage.step).GetEffectiveStep(), imageId);
                break;
            case HoloCommand.DOWN:
                SetImageCoordinatesW(HoloCommand.DOWN, new MovementStep(imageMessage.step).GetEffectiveStep(), imageId);
                break;
            case HoloCommand.LEFT:
                SetImageCoordinatesWR(HoloCommand.LEFT, new MovementStep(imageMessage.step).GetEffectiveStep(), imageId, imageMessage.referenceSystem);
                break;
            case HoloCommand.RIGHT:
                SetImageCoordinatesWR(HoloCommand.RIGHT, new MovementStep(imageMessage.step).GetEffectiveStep(), imageId, imageMessage.referenceSystem);
                break;
            case HoloCommand.ZOOM_IN:
                SetImageCoordinatesW(HoloCommand.ZOOM_IN, 0.05f, imageId);
                break;
            case HoloCommand.ZOOM_OUT:
                SetImageCoordinatesW(HoloCommand.ZOOM_OUT, 0.05f, imageId);
                break;
            case HoloCommand.INCREASE_Z:
                SetImageCoordinatesWR(HoloCommand.INCREASE_Z, new MovementStep(imageMessage.step).GetEffectiveStep(), imageId, imageMessage.referenceSystem);
                break;
            case HoloCommand.DECREASE_Z:
                SetImageCoordinatesWR(HoloCommand.DECREASE_Z, new MovementStep(imageMessage.step).GetEffectiveStep(), imageId, imageMessage.referenceSystem);
                break;
            case HoloCommand.ROTATE_XUP:
                SetImageCoordinatesW(HoloCommand.ROTATE_XUP, new RotationStep(imageMessage.step).GetEffectiveStep(), imageId);
                break;
            case HoloCommand.ROTATE_YRIGHT:
                SetImageCoordinatesW(HoloCommand.ROTATE_YRIGHT, new RotationStep(imageMessage.step).GetEffectiveStep(), imageId);
                break;
            case HoloCommand.ROTATE_ZRIGHT:
                SetImageCoordinatesW(HoloCommand.ROTATE_ZRIGHT, new RotationStep(imageMessage.step).GetEffectiveStep(), imageId);
                break;
            case HoloCommand.ROTATE_XDOWN:
                SetImageCoordinatesW(HoloCommand.ROTATE_XDOWN, new RotationStep(imageMessage.step).GetEffectiveStep(), imageId);
                break;
            case HoloCommand.ROTATE_YLEFT:
                SetImageCoordinatesW(HoloCommand.ROTATE_YLEFT, new RotationStep(imageMessage.step).GetEffectiveStep(), imageId);
                break;
            case HoloCommand.ROTATE_ZLEFT:
                SetImageCoordinatesW(HoloCommand.ROTATE_ZLEFT, new RotationStep(imageMessage.step).GetEffectiveStep(), imageId);
                break;
            case HoloCommand.SELECTION:
                SelectImage(imageId);
                break;
            case HoloCommand.UNSELECTION:
                UnselectImage();
                break;
        }
        if (needToSave) SaveImages();
    }

    public void ClearImagesContainer()
    {
        ClearImages();
    }

    public void LoadImagesContainer()
    {
        if (string.IsNullOrEmpty(_settings.ImagesBackup)) return;
        _imagesStore = JsonUtility.FromJson<ImagesStore>(_settings.ImagesBackup);
        _imagesStore.imageRecords.ForEach(i =>
        {
            GameObject image;
            InstantiateImage(out image, new Guid(i.id), i.direction, i.url, im => 
            {
                AnchorStore.Load(i.id.ToString(), im);
                Component.Destroy(im.GetComponent<WorldAnchor>());
            });
            _onImageLoaded.Invoke(JsonUtility.ToJson(new ImageCallBackCommand()
            {
                command = HoloCommand.COORDINATES,
                index = _imagesCounter,
                id = i.id,
            }));
            image.GetComponent<Billboard>().enabled = false;
            image.GetComponent<Tagalong>().enabled = false;
            image.transform.Find("Canvas/FooterPanel").gameObject.SetActive(false);
        });
    }

    public void HideImagesContainer()
    {
        ImagesContainer.SetActive(false);
        ClearImages();
    }

    public void ShowImagesContainer()
    {
        ImagesContainer.SetActive(true);
    }

    public void CatchImage(GameObject image)
    {
        _catchedImage = image;
        _catchedImage.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, 2));
        _catchedImage.GetComponent<Billboard>().enabled = true;
        _catchedImage.GetComponent<Tagalong>().enabled = true;
        var fixedAngularSize = _catchedImage.GetComponent<FixedAngularSize>();
        if (fixedAngularSize!=null)
            fixedAngularSize.enabled = true;
        _catchedImage.transform.Find("Canvas/FooterPanel").gameObject.SetActive(true);
    }

    public Guid PlaceCatchedImage()
    {
        var imageId = _images.FirstOrDefault(i => i.Value.Equals(_catchedImage)).Key;
        _catchedImage.GetComponent<Billboard>().enabled = false;
        _catchedImage.GetComponent<Tagalong>().enabled = false;
        _catchedImage.GetComponent<FixedAngularSize>().enabled = false;
        _catchedImage.transform.Find("Canvas/FooterPanel").gameObject.SetActive(false);
        _catchedImage = null;
        return imageId;
    }

    public void FixImagePosition(Guid imageId)
    {
        RaycastHit hitInfo;
        var ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
        if (!Physics.Raycast(ray, out hitInfo, 100, _spatialManager.GetLayerMask())) return;
        var image = _images.FirstOrDefault(i => i.Key.Equals(imageId)).Value;
        if (image.transform.position.z > hitInfo.point.z)
        {
            image.transform.position = new Vector3(hitInfo.point.x, hitInfo.point.y, hitInfo.point.z - 0.1f);
            image.transform.rotation = Quaternion.identity;
        }
        UpdateImage(imageId, image, null, null);
    }

    public void RemoveCatchedImage()
    {
        if (_catchedImage == null) return;
        var imageId = _images.FirstOrDefault(i => i.Value.Equals(_catchedImage)).Key;
        RemoveImage(imageId);
        _catchedImage = null;
        SaveImages();
    }

    private void SelectImage(Guid id)
    {
        if (_selectedImage != null)
            ((Behaviour)_selectedImage.transform.Find("Canvas/RawImage").GetComponent("Halo")).enabled = false;
        GameObject image;
        if (!_images.TryGetValue(id, out image)) return;
        ((Behaviour)image.transform.Find("Canvas/RawImage").GetComponent("Halo")).enabled = true;
        _selectedImage = image;
    }

    private void UnselectImage()
    {
        if (_selectedImage != null)
            ((Behaviour)_selectedImage.transform.Find("Canvas/RawImage").GetComponent("Halo")).enabled = false;
        _selectedImage = null;
    }

    private void UpdateImage(Guid id, GameObject image, Direction? direction, string url)
    {
        if (image == null)
        {
            AnchorStore.Delete(id.ToString());
            _imagesStore.imageRecords.Remove(_imagesStore.imageRecords.FirstOrDefault(i => i.id == id.ToString()));
            return;
        }
        image.AddComponent<WorldAnchor>();
        AnchorStore.Delete(id.ToString());
        AnchorStore.Save(id.ToString(), image.GetComponent<WorldAnchor>());
        Component.Destroy(image.GetComponent<WorldAnchor>());
        var imageStored =  _imagesStore.imageRecords.FirstOrDefault(i => i.id == id.ToString());
        if (imageStored != null) return;
        _imagesStore.imageRecords.Add(new ImageRecord
        {
            id = id.ToString(),
            direction = direction.Value,
            url = url
        });
    }

    private void SaveImages()
    {
        _settings.ImagesBackup = JsonUtility.ToJson(_imagesStore);
        _settings.CallToResume = true;
        _settings.Save();
    }

    private void ClearImages()
    {
        _images.Keys.ToList().ForEach(RemoveImage);
        _imagesCounter = 0;
        _settings.ImagesBackup = string.Empty;
        _settings.Save();
    }

    private void RemoveImage(Guid id)
    {
        GameObject image;
        if (!_images.TryGetValue(id, out image)) return;
        UpdateImage(id, null, null, null);
        if (_selectedImage!=null && _selectedImage.Equals(image)) _selectedImage = null;
        GameObject.Destroy(image);
        _images.Remove(id);
        if(_onImageDestroyed!=null) _onImageDestroyed.Invoke(JsonUtility.ToJson(new ImageCallBackCommand()
        {
            command = HoloCommand.ACTIVE_DEACTIVE,
            id = id.ToString(),
        }));
    }

    private void CreateImage(Direction direction, string encodedImage)
    {
        RemoveCatchedImage();
        GameObject image;
        var id = Guid.NewGuid();
        InstantiateImage(out image, id, direction, encodedImage,
            i => {
                i.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, 2));
            });
        _catchedImage = image;
        UpdateImage(id, image, direction, encodedImage);
        if(_onImageCreated!=null) _onImageCreated.Invoke(id, JsonUtility.ToJson(new ImageCallBackCommand()
        {
            command = HoloCommand.COORDINATES,
            index = _imagesCounter,
            id = id.ToString(),
        }));
    }

    private void InstantiateImage(out GameObject image, Guid id, Direction direction, string url, Action<GameObject> setPositionAndRotation)
    {
        switch (direction)
        {
            default:
                image = null;
                break;
            case Direction.HORIZONTAL:
                Debug.Log("NEW HORIZONTAL IMAGE:" + id);
                image = (GameObject)GameObject.Instantiate(HorizontalImage, _imagesContainer.transform);
                break;
            case Direction.VERTICAL:
                Debug.Log("NEW VERTICAL IMAGE:" + id);
                image = (GameObject)GameObject.Instantiate(VerticalImage, _imagesContainer.transform);
                break;
        }
        if (setPositionAndRotation != null) setPositionAndRotation.Invoke(image);
        _imagesCounter++;
        if (image == null) return;
        image.transform.Find("Canvas/HeaderPanel/HeaderPanelContainer/HeaderText").gameObject.GetComponent<TextMesh>().text = _imagesCounter.ToString();
        if(_onImageInstantiated!=null) _onImageInstantiated.Invoke(url, image.transform.Find("Canvas/RawImage").gameObject.GetComponent<RawImage>());
        image.SetActive(true);
        _images.Add(id, image);
    }

    private void SetImageCoordinatesW(HoloCommand command, float step, Guid imageId)
    {
        float x, y, z;
        GameObject image;
        if (!_images.TryGetValue(imageId, out image)) return;
        x = image.transform.localPosition.x;
        y = image.transform.localPosition.y;
        z = image.transform.localPosition.z;
        switch (command)
        {
            case HoloCommand.UP:
                image.transform.localPosition = new Vector3(x, y + step, z);
                break;
            case HoloCommand.DOWN:
                image.transform.localPosition = new Vector3(x, y - step, z);
                break;
            case HoloCommand.LEFT:
                image.transform.localPosition = new Vector3(x - step, y, z);
                break;
            case HoloCommand.RIGHT:
                image.transform.localPosition = new Vector3(x + step, y, z);
                break;
            case HoloCommand.ZOOM_IN:
                image.transform.localScale += new Vector3(step, step, step);
                break;
            case HoloCommand.ZOOM_OUT:
                image.transform.localScale -= new Vector3(step, step, step);
                break;
            case HoloCommand.INCREASE_Z:
                image.transform.localPosition = new Vector3(x, y, z + step);
                break;
            case (HoloCommand.DECREASE_Z):
                image.transform.localPosition = new Vector3(x, y, z - step);
                break;
            case HoloCommand.ROTATE_XUP:
                image.transform.Rotate(step, 0, 0);
                break;
            case HoloCommand.ROTATE_YRIGHT:
                image.transform.Rotate(0, step, 0);
                break;
            case HoloCommand.ROTATE_ZRIGHT:
                image.transform.Rotate(0, 0, step);
                break;
            case HoloCommand.ROTATE_XDOWN:
                image.transform.Rotate(-step, 0, 0);
                break;
            case HoloCommand.ROTATE_YLEFT:
                image.transform.Rotate(0, -step, 0);
                break;
            case HoloCommand.ROTATE_ZLEFT:
                image.transform.Rotate(0, 0, -step);
                break;
        }
        UpdateImage(imageId, image, null, null);
    }

    private void SetImageCoordinatesWR(HoloCommand command, float step, Guid imageId, ReferenceSystem referenceSystem)
    {
        float x, y, z;
        GameObject image;
        if (!_images.TryGetValue(imageId, out image)) return;
        x = image.transform.localPosition.x;
        y = image.transform.localPosition.y;
        z = image.transform.localPosition.z;
        switch (command)
        {
            case HoloCommand.LEFT:
                if (referenceSystem == ReferenceSystem.RELATIVE)
                    image.transform.localPosition = new Vector3(x - step, y, z);
                else
                {
                    step = step * 10;
                    var dir = -Camera.main.transform.right;
                    image.transform.Translate(dir * Time.deltaTime * step, Space.World);
                }
                break;
            case HoloCommand.RIGHT:
                if (referenceSystem == ReferenceSystem.RELATIVE)
                    image.transform.localPosition = new Vector3(x + step, y, z);
                else
                {
                    step = step * 10;
                    var dir = Camera.main.transform.right;
                    image.transform.Translate(dir * Time.deltaTime * step, Space.World);
                }
                break;
            case HoloCommand.INCREASE_Z:
                if (referenceSystem == ReferenceSystem.RELATIVE)
                    image.transform.localPosition = new Vector3(x, y, z + step);
                else
                {
                    step = step * 10;
                    var dir = -Camera.main.transform.forward;
                    image.transform.Translate(dir * Time.deltaTime * step, Space.World);
                }
                break;
            case HoloCommand.DECREASE_Z:
                if (referenceSystem == ReferenceSystem.RELATIVE)
                    image.transform.localPosition = new Vector3(x, y, z - step);
                else
                {
                    step = step * 10;
                    var dir = Camera.main.transform.forward;
                    image.transform.Translate(dir * Time.deltaTime * step, Space.World);
                }
                break;
        }
        UpdateImage(imageId, image, null, null);
    }

    public IEnumerator SetBlinkImage(Guid imageId)
    {
        GameObject image;
        if (!_images.TryGetValue(imageId, out image)) yield break;
        for (var i = 0; i < 3; i++)
        {
            if(image!=null) image.SetActive(true);
            yield return new WaitForSeconds(0.2f);
            if (image != null) image.SetActive(false);
            yield return new WaitForSeconds(0.2f);
        }
        if (image != null) image.SetActive(true);
    }

    public IEnumerator LoadImageFromUrl(string url, RawImage rawImage)
    {
        using (var www = UnityWebRequestTexture.GetTexture(url))
        {
            yield return www.SendWebRequest();
            rawImage.transform.Find("LoadPlaceHolder").gameObject.SetActive(false);
            if (!string.IsNullOrEmpty(www.error))
            {
                rawImage.transform.Find("ErrorPlaceHolder").gameObject.SetActive(true);
                yield break;
            }
            var texture = DownloadHandlerTexture.GetContent(www);
            rawImage.color = Color.white;
            rawImage.texture = texture;
        }
    }

}
