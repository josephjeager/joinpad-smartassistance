﻿using System.Linq;
using System;
using UnityEngine;
using System.Collections.Generic;
#if !UNITY_EDITOR
using System.Threading.Tasks;
using WebRtcWrapper;
#endif

public class ConnectionManager : IConnectionManager
{

    /* Public Properties */

    private Func<string, bool> _onIncomingCallFunction;
    public Func<string, bool> OnIncomingCallFunction
    {
       set { _onIncomingCallFunction = value; }
    }

    private Action _onOutgoingCallAcceptedAction;
    public Action OnOutgoingCallAcceptedAction
    {
        set { _onOutgoingCallAcceptedAction = value; }
    }

    private Action _onCallEndAction;
    public Action OnCallEndAction
    {
        set { _onCallEndAction = value; }
    }

    private Action<CommandMessage> _onCommandMessageReceivedAction;
    public Action<CommandMessage> OnCommandMessageReceivedAction
    {
        set { _onCommandMessageReceivedAction = value; }
    }

    private Action<int, string> _onContactAdded;
    public Action<int, string> OnContactAdded
    {
        set { _onContactAdded = value; }
    }

    private Action<int> _onContactRemoved;
    public Action<int> OnContactRemoved
    {
        set { _onContactRemoved = value; }
    }

    public bool IsConnected
    {
        get
        {
            #if !UNITY_EDITOR
            return _webRtcControl.IsConnected;
            #endif
            #if UNITY_EDITOR
            return false;
            #endif
        }
    }

    public bool IsConnectedToPeer
    {
        get
        {
            #if !UNITY_EDITOR
            return _webRtcControl.IsConnectedToPeer;
            #endif
            #if UNITY_EDITOR
            return false;
            #endif
        }
    }

    private int _selectedPeerId;
    public int SelectedPeerId
    {
        set { _selectedPeerId = value; }
        get { return _selectedPeerId; }
    }

    private const string CLIENT = "CLIENT";
    private const string EXPERT = "EXPERT";

    /* Private Properties */

#   if !UNITY_EDITOR
    private readonly IWebRtcControl _webRtcControl;
    #endif

    private readonly ISettings _settings;

    /* Constructor */

    #if !UNITY_EDITOR
    public ConnectionManager(IWebRtcControl webRtcControl, ISettings settings)
    {
        _webRtcControl = webRtcControl;
        _webRtcControl.OnPeerConnected += OnPeerConnected;
        _webRtcControl.OnPeerDisconnected += OnPeerDisconnected;
        _webRtcControl.OnPeerSdpOfferReceived += OnIncomingCall;
        _webRtcControl.OnPeerSdpAnswerReceived += OnOutgoingCallAccepted;
        _webRtcControl.OnPeerConnectionClosed += OnCallEnd;
        _webRtcControl.OnPeerMessageDataReceived += OnMessageReceived;
        _webRtcControl.Initialize();
        _settings = settings;
        _selectedPeerId = -1;
    }
    #endif

    /* Public Methods */

    public void ConnectToServer()
    {
        #if !UNITY_EDITOR
        Task.Run(() =>
        {
            _webRtcControl.ConnectToServer(_settings.ServerAddress, _settings.ServerPort, string.Format("{0}:{1}", CLIENT, _settings.ClientName));
        });
        #endif
    }

    public void DisableMicrophoneStream()
    {
        #if !UNITY_EDITOR
        _webRtcControl.MuteMicrophone();
        #endif
    }

    public void EnableMicrophoneStream()
    {
        #if !UNITY_EDITOR
        _webRtcControl.UnmuteMicrophone();
        #endif
    }

    public void DisconnectFromPeer()
    {
        #if !UNITY_EDITOR
        if (_webRtcControl.IsConnectedToPeer)
            _webRtcControl.DisconnectFromPeer();
        #endif
    }

    public void DisconnectFromServer()
    {
        #if !UNITY_EDITOR
        DisconnectFromPeer();   
        if (_webRtcControl.IsConnected)
            _webRtcControl.DisconnectFromServer();
        #endif
    }

    public void StartCall()
    {
         #if !UNITY_EDITOR
         Task.Run(() =>
         {
             if (_selectedPeerId == -1) return;
             _webRtcControl.SelectedPeer = _webRtcControl.Peers.FirstOrDefault(p => p.Id == _selectedPeerId);
             _webRtcControl.ConnectToPeer();
             UnityMainThreadDispatcher.Instance().Enqueue(() =>
             {
                 _settings.LastPeerNameBackup = _webRtcControl.SelectedPeer.Name;
                 _settings.Save();
             });
         });
         #endif
    }

    public void ResumeCall()
    {
        #if !UNITY_EDITOR
        Task.Run(() =>
        {
            if (string.IsNullOrEmpty(_settings.LastPeerNameBackup)) return;
            var selectedPeer = _webRtcControl.Peers.FirstOrDefault(p => p.Name.Equals(_settings.LastPeerNameBackup));
            if (selectedPeer == null) return;
            _webRtcControl.SelectedPeer = selectedPeer;
            _webRtcControl.ConnectToPeer();
            UnityMainThreadDispatcher.Instance().Enqueue(() =>
            {
                _settings.LastPeerNameBackup = _webRtcControl.SelectedPeer.Name;
                _settings.Save();
            });
        });
        #endif
    }

    public void GetContacts(Action<List<Contact>> callBack)
    {
        #if !UNITY_EDITOR
        Task.Run(() =>
        {
            var contacts = new List<Contact>();
            if (_webRtcControl.Peers != null)
                _webRtcControl.Peers.ToList().ForEach(p => { if (p.Name.StartsWith(EXPERT)) contacts.Add(new Contact(p.Id, p.Name.Substring(7))); });
            callBack(contacts);
        });
        #endif
    }

    /* Private Methods */

    #if !UNITY_EDITOR
    private async Task<bool> OnIncomingCall(int peerId, string message)
    {
        string peerName = string.Empty;
        var firstOrDefault = _webRtcControl.Peers.FirstOrDefault(p => p.Id == peerId);
        if (firstOrDefault != null)
            peerName = firstOrDefault.Name;
        UnityMainThreadDispatcher.Instance().Enqueue(() =>
        {
            _settings.LastPeerNameBackup = peerName;
            _settings.Save();
        });
        return await Task.Run(() => _onIncomingCallFunction.Invoke(peerName.Substring(7)));
    }
    #endif

    private void OnOutgoingCallAccepted(int peerId, string message)
    {
        if (_onOutgoingCallAcceptedAction != null) _onOutgoingCallAcceptedAction.Invoke();
    }

    private void OnCallEnd()
    {
        _onCallEndAction.Invoke();
    }

    private void OnPeerConnected(int id, string username)
    {
        if (_onContactAdded!=null && username.StartsWith(EXPERT)) _onContactAdded.Invoke(id, username.Substring(7));
    }

    private void OnPeerDisconnected(int id)
    {
        if (_selectedPeerId == id)
            _selectedPeerId = -1;
        if(_onContactRemoved!=null) _onContactRemoved.Invoke(id);
    }

    private void OnMessageReceived(int peerId, string message)
    {
        var messageData = JsonUtility.FromJson<MessageData>(message);
        if(_onCommandMessageReceivedAction!=null) _onCommandMessageReceivedAction.Invoke(messageData.content);
    }

    public void SendMessageToConnectedPeer(CommandMessage content)
    {
        #if !UNITY_EDITOR
        _webRtcControl.SendPeerMessageData(JsonUtility.ToJson(content));
        #endif
    }
}