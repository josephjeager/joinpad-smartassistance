﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public interface IImagesManager : IMessageDispatcher
{
    Action<string> OnImageLoaded { set; }
    Action<Guid, string> OnImageCreated { set; }
    Action<string> OnImageDestroyed { set; }
    Action<string, RawImage> OnImageInstantiated { set; }
    IEnumerator SetBlinkImage(Guid id);
    IEnumerator LoadImageFromUrl(string url, RawImage rawImage);
    void ShowImagesContainer();
    void HideImagesContainer();
    void ClearImagesContainer();
    void LoadImagesContainer();
    void CatchImage(GameObject image);
    Guid PlaceCatchedImage();
    void RemoveCatchedImage();
    void FixImagePosition(Guid imageId);
}