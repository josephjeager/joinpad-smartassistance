﻿
public interface IAudioManager {

    void PlayAudio(Audio audio);
    void StopAudio(Audio audio);

}
