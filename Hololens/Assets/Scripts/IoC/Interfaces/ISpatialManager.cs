﻿using System.Collections;

public interface ISpatialManager  {

    void EnableSpatialMapping();
    void DisableSpatialMapping();
    void ShowVisualMeshes();
    IEnumerator HideVisualMeshesCoroutine(float seconds);
    int GetLayerMask();

}
