﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUIManager  {

    Action OnOkDialogBoxClick { get; }
    Action OnCancelDialogBoxClick { get; }
    void ShowMenu(Menu menu);
    void HideMenu(Menu menu);
    void ShowBox(Box box);
    void HideBox(Box box);
    void ShowDialogBox(string firstMessage, string secondMessage, string thirdMessage, Action onOKDialogBoxClick, Action onCancelDialogBoxClick);
    void LockMenu(Menu menu);
    void UnlockMenu(Menu menu);
    bool CanStoreSettings();
    void ReadAndSaveSettings();
    void LoadAndFillSettings();
    void RestoreCallContactButton();
    void UpdateMenuFooterStatus(Status status);
    IEnumerator MainMenuConnectCoroutine();
    IEnumerator OutgoingCallCoroutine();
    IEnumerator IncomingCallCoroutine();
    IEnumerator ResumeCallCoroutine();
    IEnumerator HideLookAroundBoxCoroutine(float seconds, Action endCallback);
    IEnumerator WaitCallMenuCoroutine(float seconds, Action callBack);
    void ShowMuteButtonState();
    void ShowUnmuteButtonState();
    IEnumerator AddContact(Contact contact);
    IEnumerator RemoveContact(int id);
    IEnumerator AddContacts(List<Contact> contacts);
    void SelectContact(int id);
    void UnSelectContact();
    void SetCaller(string caller);

}
