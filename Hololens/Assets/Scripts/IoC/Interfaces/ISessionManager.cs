﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISessionManager : IMessageDispatcher {

    Action<string> OnSessionChecked { set; }

}
