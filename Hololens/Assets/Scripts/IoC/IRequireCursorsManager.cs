﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRequireCursorsManager {

    ICursorsManager CursorsManager { set; }
}
