﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRequireImagesManager {

    IImagesManager ImagesManager { set; }

}
