﻿public class Contact  {

    private readonly int _id;
    public int Id
    {
        get { return _id; }
    }

    private readonly string _username;
    public string Username
    {
        get { return _username; }
    }

    public Contact(int id, string username)
    {
        _id = id;
        _username = username;
    }

}
