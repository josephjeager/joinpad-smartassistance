﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class BackContactsButtonScript : MonoBehaviour, IInputClickHandler
{
    public ControlBusScript ControlBus;

    public void OnInputClicked(InputClickedEventData eventData)
    {
        ControlBus.UndoContacts();
    }
}
