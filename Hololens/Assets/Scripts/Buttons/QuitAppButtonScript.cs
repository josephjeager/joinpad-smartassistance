﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class QuitAppButtonScript : MonoBehaviour, IInputClickHandler
{
    public ControlBusScript ControlBus;

    public void OnInputClicked(InputClickedEventData eventData)
    {
        ControlBus.QuitApplication();
    }
}