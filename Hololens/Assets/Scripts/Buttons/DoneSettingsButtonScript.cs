﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class DoneSettingsButtonScript : MonoBehaviour, IInputClickHandler
{
    public ControlBusScript ControlBus;

    public void OnInputClicked(InputClickedEventData eventData)
    {
        ControlBus.SaveSettings();
    }
}