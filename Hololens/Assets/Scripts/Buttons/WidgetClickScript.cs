﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class WidgetClickScript : MonoBehaviour, IInputClickHandler
{
    public ControlBusScript ControlBus;

    public void OnInputClicked(InputClickedEventData eventData)
    {
        ControlBus.RemoveWidget(this.gameObject);
    }
}