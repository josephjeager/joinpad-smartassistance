﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class DocumentClickScript : MonoBehaviour, IInputClickHandler
{
    public ControlBusScript ControlBus;

    public void OnInputClicked(InputClickedEventData eventData)
    {
        ControlBus.CatchDocument(this.gameObject);
    }
}