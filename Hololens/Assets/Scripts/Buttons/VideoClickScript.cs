﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class VideoClickScript : MonoBehaviour, IInputClickHandler
{
    public ControlBusScript ControlBus;

    public void OnInputClicked(InputClickedEventData eventData)
    {
        ControlBus.CatchVideo(this.gameObject);
    }
}