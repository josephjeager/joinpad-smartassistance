﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class ImageClickScript : MonoBehaviour, IInputClickHandler
{
    public ControlBusScript ControlBus;

    public void OnInputClicked(InputClickedEventData eventData)
    {
        ControlBus.CatchImage(this.gameObject);
    }
}