﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class ReconnectButtonScript : MonoBehaviour, IInputClickHandler
{
    public ControlBusScript ControlBus;

    public void OnInputClicked(InputClickedEventData eventData)
    {
        ControlBus.ReconnectToServer();
    }
}