﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class MuteCallMenuButtonScript : MonoBehaviour, IInputClickHandler
{
    public ControlBusScript ControlBus;

    public void OnInputClicked(InputClickedEventData eventData)
    {
        ControlBus.SwitchMicrophoneState();
    }
}
