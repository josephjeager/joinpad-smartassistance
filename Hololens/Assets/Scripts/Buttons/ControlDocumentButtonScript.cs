﻿using HoloToolkit.Unity.InputModule;
using System;
using UnityEngine;

public class ControlDocumentButtonScript : MonoBehaviour, IInputClickHandler
{

    public ControlBusScript ControlBus;
    public DocumentCommand Command;
    public Guid DocumentId;

    public void OnInputClicked(InputClickedEventData eventData)
    {
        if (DocumentId == null) return;
        switch (Command)
        {
            case DocumentCommand.ZOOM_IN:
                ControlBus.DocumentZoomIn(DocumentId);
                break;
            case DocumentCommand.ZOOM_OUT:
                ControlBus.DocumentZoomOut(DocumentId);
                break;
            case DocumentCommand.SCROLL_UP:
                ControlBus.DocumentScrollUp(DocumentId);
                break;
            case DocumentCommand.SCROLL_DOWN:
                ControlBus.DocumentScrollDown(DocumentId);
                break;
        }
    }

}
