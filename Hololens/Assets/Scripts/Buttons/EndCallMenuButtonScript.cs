﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class EndCallMenuButtonScript : MonoBehaviour, IInputClickHandler
{
    public ControlBusScript ControlBus;

    public void OnInputClicked(InputClickedEventData eventData)
    {
        ControlBus.EndCall();
    }
}
