﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class OKDialogButtonScript : MonoBehaviour, IInputClickHandler
{
    public ControlBusScript ControlBus;

    public void OnInputClicked(InputClickedEventData eventData)
    {
        ControlBus.OnDialogBoxOkButtonClick();
    }
}