﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class PlaceImageButtonScript : MonoBehaviour, IInputClickHandler
{
    public ControlBusScript ControlBus;

    public void OnInputClicked(InputClickedEventData eventData)
    {
        ControlBus.PlaceCatchedImage();
    }
}