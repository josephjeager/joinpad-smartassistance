﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class CallContactButtonScript : MonoBehaviour, IInputClickHandler
{
    public ControlBusScript ControlBus;

    public void OnInputClicked(InputClickedEventData eventData)
    {
        ControlBus.StartCall();
    }
}
