﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class RemoveDocumentButtonScript : MonoBehaviour, IInputClickHandler
{
    public ControlBusScript ControlBus;

    public void OnInputClicked(InputClickedEventData eventData)
    {
        ControlBus.RemoveCatchedDocument();
    }
}
