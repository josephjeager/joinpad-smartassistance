﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class CleanUpCallMenuButtonScript : MonoBehaviour, IInputClickHandler
{
    public ControlBusScript ControlBus;

    public void OnInputClicked(InputClickedEventData eventData)
    {
        ControlBus.CleanUp();
    }
}
