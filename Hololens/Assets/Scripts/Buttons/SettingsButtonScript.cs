﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class SettingsButtonScript : MonoBehaviour, IInputClickHandler
{
    public ControlBusScript ControlBus;

    public void OnInputClicked(InputClickedEventData eventData)
    {
        ControlBus.ShowSettingsMenu();
    }
}
