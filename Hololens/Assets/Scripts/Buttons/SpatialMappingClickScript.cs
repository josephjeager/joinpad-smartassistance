﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class SpatialMappingClickScript : MonoBehaviour, IHoldHandler, IInputClickHandler
{
    public ControlBusScript ControlBus;

    public void OnHoldCanceled(HoldEventData eventData)
    {
        ControlBus.BlockCallMenuWaiting();
    }

    public void OnHoldCompleted(HoldEventData eventData)
    {
        ControlBus.BlockCallMenuWaiting();
    }

    public void OnHoldStarted(HoldEventData eventData)
    {
        ControlBus.WaitCallMenu();
    }

    public void OnInputClicked(InputClickedEventData eventData)
    {
        ControlBus.StartMapping();
    }

}
