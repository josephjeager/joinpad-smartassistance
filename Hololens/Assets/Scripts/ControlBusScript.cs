﻿using System;
using System.Collections;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class ControlBusScript : MonoBehaviour, 
                                IRequireSettings, 
                                IRequireUIManager,
                                IRequireAudioManager,
                                IRequireSpatialManager,
                                IRequireKeyboardManager,
                                IRequireConnectionManager,
                                IRequireWidgetsManager,
                                IRequireTextsManager,
                                IRequireImagesManager,
                                IRequireMicrophoneManager,
                                IRequireCursorsManager,
                                IRequireDocumentsManager,
                                IRequireVideosManager,
                                IRequireSessionManager
{

    /* IoC Properties */

    private ISettings _settings;
    public ISettings Settings
    {
        set { _settings = value; }
    }

    private IUIManager _uiManager;
    public IUIManager UIManager
    {
        set { _uiManager = value; }
    }

    private IAudioManager _audioManager;
    public IAudioManager AudioManager
    {
        set { _audioManager = value; }
    }

    private ISpatialManager _spatialManager;
    public ISpatialManager SpatialManager
    {
        set { _spatialManager = value; }
    }

    private IKeyboardManager _keyboardManager;
    public IKeyboardManager KeyboardManager
    {
        set { _keyboardManager = value; }
    }

    private IConnectionManager _connectionManager;
    public IConnectionManager ConnectionManager
    {
        set { _connectionManager = value; }
    }

    private IWidgetsManager _widgetsManager;
    public IWidgetsManager WidgetsManager
    {
        set { _widgetsManager = value; }
    }

    private ITextsManager _textsManager;
    public ITextsManager TextsManager
    {
        set { _textsManager = value; }
    }

    private IImagesManager _imagesManager;
    public IImagesManager ImagesManager
    {
        set { _imagesManager = value; }
    }

    private IMicrophoneManager _microphoneManager;
    public IMicrophoneManager MicrophoneManager
    {
        set { _microphoneManager = value; }
    }

    private ICursorsManager _cursorsManager;
    public ICursorsManager CursorsManager
    {
        set { _cursorsManager = value; }
    }

    private IDocumentsManager _documentsManager;
    public IDocumentsManager DocumentsManager
    {
        set { _documentsManager = value; }
    }

    private IVideosManager _videosManager;
    public IVideosManager VideosManager
    {
        set { _videosManager = value; }
    }

    private ISessionManager _sessionManager;
    public ISessionManager SessionManager
    {
        set { _sessionManager = value; }
    }

    /* Private Properties */

    private float _timeLeft;

    private bool _incomingCallIgnored;
    private bool _incomingCallCancelled;
    private bool _outgoingCallAccepted;

    private readonly object _incomingCallLocker;
    private readonly object _outgoingCallLocker;

    private bool _commandsExecutionEnabled;

    private readonly EventWaitHandle _incomingCallEventHandle;

    private Coroutine _waitCallMenuCoroutine;

    /* Conustructor */

    public ControlBusScript()
    {
        _incomingCallEventHandle = new AutoResetEvent(false);
        _incomingCallLocker = new object();
        _outgoingCallLocker = new object();
        _commandsExecutionEnabled = true;
    }

    /* Class Overrides */

    void Start()
    {
        // Init ConnectionManager callback functions
        _connectionManager.OnIncomingCallFunction = OnIncomingCall;
        _connectionManager.OnOutgoingCallAcceptedAction = OnOutgoingCallAccepted;
        _connectionManager.OnCallEndAction = OnCallEnd;
        _connectionManager.OnCommandMessageReceivedAction = OnCommandMessageReceived;
        _connectionManager.OnContactAdded = OnContactAdded;
        _connectionManager.OnContactRemoved = OnContactRemoved;

        // Init WidgetsManager callback functions
        _widgetsManager.OnWidgetLoaded = OnWidgetLoaded;
        _widgetsManager.OnWidgetCreated = OnWidgetCreated;
        _widgetsManager.OnWidgetDestroyed = OnWidgetDestroyed;

        // Init TextsManager callback functions
        _textsManager.OnTextLoaded = OnTextLoaded;
        _textsManager.OnTextCreated = OnTextCreated;
        _textsManager.OnTextDestroyed = OnTextDestroyed;

        // Init ImagesManager callback functions
        _imagesManager.OnImageLoaded = OnImageLoaded;
        _imagesManager.OnImageCreated = OnImageCreated;
        _imagesManager.OnImageInstantiated = OnImageInstantiated;
        _imagesManager.OnImageDestroyed = OnImageDestroyed;

        // Init DocumentsManager callback functions
        _documentsManager.OnDocumentLoaded = OnDocumentLoaded;
        _documentsManager.OnDocumentCreated = OnDocumentCreated;
        _documentsManager.OnDocumentInstantiated = OnDocumentInstantiated;
        _documentsManager.OnDocumentDestroyed = OnDocumentDestroyed;

        // Init VideosManager callback functions
        _videosManager.OnVideoLoaded = OnVideoLoaded;
        _videosManager.OnVideoCreated = OnVideoCreated;
        _videosManager.OnVideoDestroyed = OnVideoDestroyed;

        // Init SessionManager callback functions
        _sessionManager.OnSessionChecked = OnSessionChecked;

        /* JOINPAD FIRST ACCESS */
        if (!_settings.Initialized)
        {
            _settings.ClientName = "HOLOLENS-ALSTOM";
            _settings.ServerAddress = "35.158.223.36";
            //_settings.ClientName = "HOLOLENS-JOINPAD";
            //_settings.ServerAddress = "18.184.81.137";
            _settings.ServerPort = "8888";
            _settings.Initialized = true;
            _settings.Save();
        }
        /* END */

        if (_settings.Initialized)
        {
            if(_settings.CallToResume)
            {
                _uiManager.ShowDialogBox("UNEXPECTED END OF PREVIOUS CALL", "CLICK OK TO RESUME", "CANCEL TO UNDO", 
                () => {
                    _uiManager.ShowBox(Box.RESUME_CALL);
                    _audioManager.PlayAudio(Audio.OUTGOING_CALL);
                    var resumeCallCoroutine = StartCoroutine(_uiManager.ResumeCallCoroutine());
                    _connectionManager.ConnectToServer();
                    StartCoroutine(ResumeCallCoroutine(resumeCallCoroutine));
                }, 
                () => {
                    _widgetsManager.ClearWidgetsContainer();
                    _textsManager.ClearTextsContainer();
                    _imagesManager.ClearImagesContainer();
                    _documentsManager.ClearDocumentsContainer();
                    _videosManager.ClearVideosContainer();
                    StartConnection();
                });
                return;
            }
            StartConnection();
        }
        else
        {
            _audioManager.PlayAudio(Audio.WELCOME_FIRST_ACCESS);
            _uiManager.ShowMenu(Menu.SETTINGS);
        }
    }

    void Update()
    {
        _timeLeft -= Time.deltaTime;
    }

    /* Public Methods */

    public void StartCall()
    {
        if (!_connectionManager.IsConnected)
        {
            _uiManager.ShowDialogBox("NO SERVER CONNECTION", "PLASE, CHECK YOUR SETTINGS", string.Empty,
                () => _uiManager.ShowMenu(Menu.CONTACTS),
                () => _uiManager.ShowMenu(Menu.CONTACTS));
            return;
        }
        if (_connectionManager.SelectedPeerId == -1)
        {
            _uiManager.ShowDialogBox("YOU HAVE TO SELECT A CONTACT", "IN ORDER TO START A CALL", string.Empty,
                () => _uiManager.ShowMenu(Menu.CONTACTS),
                () => _uiManager.ShowMenu(Menu.CONTACTS));
            return;
        }
        _audioManager.PlayAudio(Audio.OUTGOING_CALL);
        var outgoingCallCoroutine = StartCoroutine(_uiManager.OutgoingCallCoroutine());
        _connectionManager.StartCall();
        StartCoroutine(CallCoroutine(outgoingCallCoroutine));
    }

    public void ShowSettingsMenu()
    {
        _uiManager.LoadAndFillSettings();
        _uiManager.ShowMenu(Menu.SETTINGS);
    }

    public void ShowContatcsMenu()
    {
        _uiManager.UnSelectContact();
        _connectionManager.SelectedPeerId = -1;
        _uiManager.RestoreCallContactButton();
        _uiManager.ShowMenu(Menu.CONTACTS);
    }

    public void AcceptIncomingCall()
    {
        _incomingCallEventHandle.Set();
    }

    public void IgnoreIncomingCall()
    {
        _incomingCallIgnored = true;
        _incomingCallEventHandle.Set();
    }

    public void OnOutgoingCallAccepted()
    {
        lock (_outgoingCallLocker)
            _outgoingCallAccepted = true;
    }

    public void SaveSettings()
    {
        _keyboardManager.CloseKeyboard();
        if (!_uiManager.CanStoreSettings())
        {
            _uiManager.ShowDialogBox("INPUT FIELDS CANNOT BE EMPTIES", "PLASE, CHECK YOUR INPUT DATA", string.Empty,
                () => _uiManager.ShowMenu(Menu.SETTINGS),
                () => _uiManager.ShowMenu(Menu.SETTINGS));
            return;
        }
        _uiManager.ReadAndSaveSettings();
        _uiManager.ShowMenu(Menu.MAIN);
        var connectCoroutine = StartCoroutine(_uiManager.MainMenuConnectCoroutine());
        _connectionManager.ConnectToServer();
        StartCoroutine(ConnectionCoroutine(connectCoroutine));
    }

    public void UndoContacts()
    {
        UnSelectContact();
        if (_connectionManager.IsConnectedToPeer)
            EndCall();
        else _uiManager.ShowMenu(Menu.MAIN);
    }

    public void UndoSettings()
    {
        _keyboardManager.CloseKeyboard();
        if (!_uiManager.CanStoreSettings() && !_settings.Initialized)
        {
            _uiManager.ShowDialogBox("INVALID CONFIGURATION SETTINGS", "PLASE, CHECK YOUR INPUT DATA", string.Empty,
                () => _uiManager.ShowMenu(Menu.SETTINGS),
                () => _uiManager.ShowMenu(Menu.SETTINGS));
            return;
        }
        _uiManager.ShowMenu(Menu.MAIN);
    }

    public void LockSettingsMenu()
    {
        _uiManager.LockMenu(Menu.SETTINGS);
    }

    public void UnlockSettingsMenu()
    {
        _uiManager.UnlockMenu(Menu.SETTINGS);
    }

    public void OnDialogBoxOkButtonClick()
    {
        _uiManager.OnOkDialogBoxClick.Invoke();
    }

    public void OnDialogBoxCancelButtonClick()
    {
        _uiManager.OnCancelDialogBoxClick.Invoke();
    }

    public void RemoveText(GameObject text)
    {
        _textsManager.RemoveText(text);
    }

    public void RemoveWidget(GameObject widget)
    {
        _widgetsManager.RemoveWidget(widget);
    }

    public void CatchImage(GameObject image)
    {
        _commandsExecutionEnabled = false;
        _spatialManager.DisableSpatialMapping();
        _imagesManager.CatchImage(image);
    }

    public void CatchDocument(GameObject image)
    {
        _commandsExecutionEnabled = false;
        _spatialManager.DisableSpatialMapping();
        _documentsManager.CatchDocument(image);
    }

    public void CatchVideo(GameObject video)
    {
        _commandsExecutionEnabled = false;
        _spatialManager.DisableSpatialMapping();
        _videosManager.CatchVideo(video);
    }

    public void RemoveCatchedImage()
    {
        _imagesManager.RemoveCatchedImage();
        _spatialManager.EnableSpatialMapping();
        _commandsExecutionEnabled = true;
    }

    public void PlaceCatchedImage()
    {
        var imageId = _imagesManager.PlaceCatchedImage();
        _spatialManager.EnableSpatialMapping();
        _imagesManager.FixImagePosition(imageId);
        _commandsExecutionEnabled = true;
    }

    public void RemoveCatchedDocument()
    {
        _documentsManager.RemoveCatchedDocument();
        _spatialManager.EnableSpatialMapping();
        _commandsExecutionEnabled = true;
    }

    public void RemoveCatchedVideo()
    {
        _videosManager.RemoveCatchedVideo();
        _spatialManager.EnableSpatialMapping();
        _commandsExecutionEnabled = true;
    }

    public void PlaceCatchedDocument()
    {
        var documentId = _documentsManager.PlaceCatchedDocument();
        _spatialManager.EnableSpatialMapping();
        _documentsManager.FixDocumentPosition(documentId);
        _commandsExecutionEnabled = true;
    }

    public void PlaceCatchedVideo()
    {
        var videoId = _videosManager.PlaceCatchedVideo();
        _spatialManager.EnableSpatialMapping();
        _videosManager.FixVideoPosition(videoId);
        _commandsExecutionEnabled = true;
    }

    public void StartMapping()
    {
        _cursorsManager.HideCursor();
        _uiManager.ShowBox(Box.LOOK_AROUND);
        _spatialManager.ShowVisualMeshes();
        StartCoroutine(_uiManager.HideLookAroundBoxCoroutine(5f, _cursorsManager.ShowCursor));
        StartCoroutine(_spatialManager.HideVisualMeshesCoroutine(5f));
    }

    public void WaitCallMenu()
    {
        _commandsExecutionEnabled = false;
        _waitCallMenuCoroutine = StartCoroutine(_uiManager.WaitCallMenuCoroutine(1f, () => _spatialManager.DisableSpatialMapping()));
    }

    public void BlockCallMenuWaiting()
    {
        if (_waitCallMenuCoroutine == null) return;
        StopCoroutine(_waitCallMenuCoroutine);
        _uiManager.HideBox(Box.CONTEXT_MENU);
        _commandsExecutionEnabled = true;
    }

    public void CancelCallMenu()
    {
        _uiManager.HideMenu(Menu.CALL);
        _spatialManager.EnableSpatialMapping();
        _commandsExecutionEnabled = true;
    }

    public void CleanUp()
    {
        _widgetsManager.ClearWidgetsContainer();
        _textsManager.ClearTextsContainer();
        _imagesManager.ClearImagesContainer();
        _documentsManager.ClearDocumentsContainer();
        _videosManager.ClearVideosContainer();
        _settings.CallToResume = false;
        _settings.LastPeerNameBackup = string.Empty;
        _settings.LastPeerSessionIdBackup = string.Empty;
        _settings.Save();
        _uiManager.HideMenu(Menu.CALL);
        _spatialManager.EnableSpatialMapping();
        _commandsExecutionEnabled = true;
    }

    public void SwitchMicrophoneState()
    {
        _microphoneManager.SwitchState(
            () => {
                _uiManager.ShowMuteButtonState();
                _connectionManager.EnableMicrophoneStream();
            }, 
            () => {
                _uiManager.ShowUnmuteButtonState();
                _connectionManager.DisableMicrophoneStream();
            });
        _uiManager.HideMenu(Menu.CALL);
        _spatialManager.EnableSpatialMapping();
        _commandsExecutionEnabled = true;
    }

    public void EndCall()
    {
        _connectionManager.DisconnectFromPeer();
        OnCallEnd();
        _commandsExecutionEnabled = true;
    }

    public void ShowNavigationCursor()
    {
        _cursorsManager.SwitchCursor(CustomCursor.BASIC);
    }

    public void HideNavigationCursor()
    {
        _cursorsManager.SwitchCursor(CustomCursor.DEFAULT);
    }

    public void UnSelectContact()
    {
        _uiManager.UnSelectContact();
        _connectionManager.SelectedPeerId = -1;
    }

    public void SelectContactToCall(int contactId)
    {
        _uiManager.SelectContact(contactId);
        if (_connectionManager.SelectedPeerId == contactId)
            _connectionManager.SelectedPeerId = -1;
        else _connectionManager.SelectedPeerId = contactId;
    }

    public void QuitApplication()
    {
        StopAllCoroutines();
        _connectionManager.DisconnectFromServer();
        _spatialManager.DisableSpatialMapping();
        _widgetsManager.HideWidgetsContainer();
        _textsManager.HideTextsContainer();
        _imagesManager.HideImagesContainer();
        _videosManager.HideVideosContainer();
        _documentsManager.HideDocumentsContainer();
        _settings.CallToResume = false;
        _settings.LastPeerNameBackup = string.Empty;
        _settings.LastPeerSessionIdBackup = string.Empty;
        _settings.Save();
        Application.Quit();
    }

    public void ReconnectToServer()
    {
        var connectCoroutine = StartCoroutine(_uiManager.MainMenuConnectCoroutine());
        _connectionManager.ConnectToServer();
        StartCoroutine(ConnectionCoroutine(connectCoroutine));
    }

    public void DocumentZoomIn(Guid documentId)
    {
        _documentsManager.ZoomIn(documentId);
    }

    public void DocumentZoomOut(Guid documentId)
    {
        _documentsManager.ZoomOut(documentId);
    }

    public void DocumentScrollUp(Guid documentId)
    {
        _documentsManager.ScrollUp(documentId);
    }

    public void DocumentScrollDown(Guid documentId)
    {
        _documentsManager.ScrollDown(documentId);
    }

    /* Private Methods */

    private void StartConnection()
    {
        _audioManager.PlayAudio(Audio.WELCOME);
        _uiManager.ShowMenu(Menu.MAIN);
        var connectCoroutine = StartCoroutine(_uiManager.MainMenuConnectCoroutine());
        _connectionManager.ConnectToServer();
        StartCoroutine(ConnectionCoroutine(connectCoroutine));
    }

    private bool OnIncomingCall(string peerName)
    {
        lock (_incomingCallLocker)
            _incomingCallCancelled = false;
        _incomingCallIgnored = false;
        Coroutine incomingCallcoroutine = null;
        UnityMainThreadDispatcher.Instance().Enqueue(() =>
        {
            _audioManager.PlayAudio(Audio.INCOMING_CALL);
            _uiManager.SetCaller(peerName.ToUpper());
            _uiManager.ShowMenu(Menu.INCOMING_CALL);
            incomingCallcoroutine = StartCoroutine(_uiManager.IncomingCallCoroutine());
        });
        _incomingCallEventHandle.Reset();
        _incomingCallEventHandle.WaitOne(); // Waiting for: Call Accepted / Ignored Call / BYE signal from remote peer

        bool incomingCallCancelled;
        lock(_incomingCallLocker)
            incomingCallCancelled = _incomingCallCancelled;

        if (incomingCallCancelled) return false; // We have a timeout or a undo from remote peer
        else if (_incomingCallIgnored)
        {
            OnCallEnd();
            return false; // Will send a BYE to remote peer
        }

        // Call accepted
        UnityMainThreadDispatcher.Instance().Enqueue(() =>
        {
            StopCoroutine(incomingCallcoroutine);
            _uiManager.HideMenu(Menu.INCOMING_CALL);
            _audioManager.StopAudio(Audio.INCOMING_CALL);
            _spatialManager.EnableSpatialMapping();
            _cursorsManager.HideCursor();
            _uiManager.ShowBox(Box.LOOK_AROUND);
            _spatialManager.ShowVisualMeshes();
            _audioManager.PlayAudio(Audio.CONNECTED);
            StartCoroutine(_uiManager.HideLookAroundBoxCoroutine(5f, _cursorsManager.ShowCursor));
            StartCoroutine(_spatialManager.HideVisualMeshesCoroutine(5f));
            _widgetsManager.ShowWidgetsContainer();
            _textsManager.ShowTextsContainer();
            _imagesManager.ShowImagesContainer();
            _documentsManager.ShowDocumentsContainer();
            _videosManager.ShowVideosContainer();
        });
        return true;
    }

    private void OnCallEnd()
    {
       lock (_incomingCallLocker)
            _incomingCallCancelled = true;
        _incomingCallEventHandle.Set();
        UnityMainThreadDispatcher.Instance().Enqueue(() =>
        {
            StopAllCoroutines();
            _audioManager.PlayAudio(Audio.CALL_TERMINATED);
            _spatialManager.DisableSpatialMapping();
            _widgetsManager.HideWidgetsContainer();
            _textsManager.HideTextsContainer();
            _imagesManager.HideImagesContainer();
            _documentsManager.HideDocumentsContainer();
            _videosManager.HideVideosContainer();
            _settings.CallToResume = false;
            _settings.LastPeerNameBackup = string.Empty;
            _settings.LastPeerSessionIdBackup = string.Empty;
            _settings.Save();
            _uiManager.ShowMenu(Menu.MAIN);
            _cursorsManager.SwitchCursor(CustomCursor.DEFAULT);
            _cursorsManager.ShowCursor();
        }); 
    }

    private void OnCommandMessageReceived(CommandMessage commandMessage)
    {
        if (!_commandsExecutionEnabled) return;
        UnityMainThreadDispatcher.Instance().Enqueue(() =>
        {
            GetDispatcher(commandMessage.dispatcher).ReadCommand(commandMessage.command);
        });
    }

    private void OnContactAdded(int id, string username)
    {
        UnityMainThreadDispatcher.Instance().Enqueue(() =>
        {
           StartCoroutine(_uiManager.AddContact(new Contact(id, username)));
        });
    }

    private void OnContactRemoved(int id)
    {
        UnityMainThreadDispatcher.Instance().Enqueue(() =>
        {
            StartCoroutine(_uiManager.RemoveContact(id));
        });
    }

    private IMessageDispatcher GetDispatcher(Dispatcher dispatcher)
    {
        switch (dispatcher)
        {
            default:
                return null;
            case Dispatcher.WIDGETS:
                return _widgetsManager;
            case Dispatcher.TEXTS:
                return _textsManager;
            case Dispatcher.IMAGES:
                return _imagesManager;
            case Dispatcher.DOCUMENTS:
                return _documentsManager;
            case Dispatcher.VIDEOS:
                return _videosManager;
            case Dispatcher.SESSION:
                return _sessionManager;
        }
    }

    private void SendWidgetCallBackCommand(string command)
    {
        var commandMessageFactory = new WidgetMessageFactory();
        _connectionManager.SendMessageToConnectedPeer(commandMessageFactory.CallBack(command));
    }

    private void OnWidgetLoaded(string command)
    {
        SendWidgetCallBackCommand(command);
    }

    private void OnWidgetCreated(Guid widgetId, string command)
    {
        SendWidgetCallBackCommand(command);
        StartCoroutine(_widgetsManager.SetBlinkWidget(widgetId));
    }

    private void OnWidgetDestroyed(string command)
    {
        SendWidgetCallBackCommand(command);
    }

    private void SendTextCallBackCommand(string command)
    {
        var commandMessageFactory = new TextMessageFactory();
        _connectionManager.SendMessageToConnectedPeer(commandMessageFactory.CallBack(command));
    }

    private void OnTextLoaded(string command)
    {
        SendTextCallBackCommand(command);
    }

    private void OnTextCreated(Guid textId, string command)
    {
        SendTextCallBackCommand(command);
        StartCoroutine(_textsManager.SetBlinkText(textId));
    }

    private void OnTextDestroyed(string command)
    {
        SendTextCallBackCommand(command);
    }

    private void SendImageCallBackCommand(string command)
    {
        var commandMessageFactory = new ImageMessageFactory();
        _connectionManager.SendMessageToConnectedPeer(commandMessageFactory.CallBack(command));
    }

    private void OnImageLoaded(string command)
    {
        SendImageCallBackCommand(command);
    }

    private void OnImageCreated(Guid imageId, string command)
    {
        SendImageCallBackCommand(command);
        _commandsExecutionEnabled = false;
        _spatialManager.DisableSpatialMapping();
        StartCoroutine(_imagesManager.SetBlinkImage(imageId));
    }

    private void OnImageInstantiated(string url, RawImage rawImage)
    {
        StartCoroutine(_imagesManager.LoadImageFromUrl(url, rawImage));
    }

    private void OnImageDestroyed(string command)
    {
        SendImageCallBackCommand(command);
    }

    private void SendDocumentCallBackCommand(string command)
    {
        var commandMessageFactory = new DocumentMessageFactory();
        _connectionManager.SendMessageToConnectedPeer(commandMessageFactory.CallBack(command));
    }

    private void OnDocumentLoaded(string command)
    {
        SendDocumentCallBackCommand(command);
    }

    private void OnDocumentCreated(Guid documentId, string command)
    {
        SendDocumentCallBackCommand(command);
        _commandsExecutionEnabled = false;
        _spatialManager.DisableSpatialMapping();
        StartCoroutine(_documentsManager.SetBlinkDocument(documentId));
    }

    private void OnDocumentInstantiated(string url, float bookmark, GameObject scrollView)
    {
        StartCoroutine(_documentsManager.LoadDocumentFromUrl(url, bookmark, scrollView));
    }

    private void OnDocumentDestroyed(string command)
    {
        SendDocumentCallBackCommand(command);
    }

    private void SendVideoCallBackCommand(string command)
    {
        var commandMessageFactory = new VideoMessageFactory();
        _connectionManager.SendMessageToConnectedPeer(commandMessageFactory.CallBack(command));
    }

    private void OnVideoLoaded(string command)
    {
        SendVideoCallBackCommand(command);
    }

    private void OnVideoCreated(Guid videoId, string command)
    {
        SendVideoCallBackCommand(command);
        _commandsExecutionEnabled = false;
        _spatialManager.DisableSpatialMapping();
        StartCoroutine(_videosManager.SetBlinkVideo(videoId));
    }

    private void OnVideoDestroyed(string command)
    {
        SendVideoCallBackCommand(command);
    }

    private void SendSessionCallBackCommand(string command)
    {
        var commandMessageFactory = new SessionMessageFactory();
        _connectionManager.SendMessageToConnectedPeer(commandMessageFactory.CallBack(command));
    }

    private void OnSessionChecked(string command)
    {
        SendSessionCallBackCommand(command);
    }

    private IEnumerator ConnectionCoroutine(Coroutine connectCoroutine)
    {
        yield return new WaitForSeconds(3); // Server Connection Timeout
        if (!_connectionManager.IsConnected)
        {
            StopCoroutine(connectCoroutine);
            _uiManager.UpdateMenuFooterStatus(Status.NOT_CONNECTED);
            _uiManager.ShowDialogBox("ERROR DURING SERVER CONNECTION", "CLICK OK TO RETRY", "OR CHECK YOUR SETTINGS",
                () => {
                    _uiManager.ShowMenu(Menu.MAIN);
                    connectCoroutine = StartCoroutine(_uiManager.MainMenuConnectCoroutine());
                    _connectionManager.ConnectToServer();
                    StartCoroutine(ConnectionCoroutine(connectCoroutine));
                },
                () => _uiManager.ShowMenu(Menu.MAIN));
            yield break;
        }
        StopCoroutine(connectCoroutine);
        _connectionManager.GetContacts((contacts) => {
            UnityMainThreadDispatcher.Instance().Enqueue(() =>
            {
                StartCoroutine(_uiManager.AddContacts(contacts));
            });
        });
        _uiManager.UpdateMenuFooterStatus(Status.CONNECTED);
    }

    private IEnumerator CallCoroutine(Coroutine outgoingCallCoroutine)
    {
        lock (_outgoingCallLocker)
            _outgoingCallAccepted = false;
        yield return new WaitForSeconds(3); // Peer Connection Timeout
        if (!_connectionManager.IsConnectedToPeer)
        {
            StopCoroutine(outgoingCallCoroutine);
            _audioManager.StopAudio(Audio.OUTGOING_CALL);
            _uiManager.RestoreCallContactButton();
            _uiManager.ShowDialogBox("ERROR DURING PEER CONNECTION", "CLICK OK TO RETRY", "CANCEL TO UNDO",
               () => {
                   _uiManager.ShowMenu(Menu.CONTACTS);
                   StartCall();
               },
               () => _uiManager.ShowMenu(Menu.CONTACTS));
            yield break;
        }
        _timeLeft = 7.0f; // Outgoing Call Timeout
        bool outgoingCallAccepted;
        lock (_outgoingCallLocker)
            outgoingCallAccepted = _outgoingCallAccepted;
        while (!outgoingCallAccepted && _timeLeft > 0) // If a BYE is received during the loop the call is not accepted
        {
            lock (_outgoingCallLocker)
                outgoingCallAccepted = _outgoingCallAccepted;
            yield return null;
        }
        if (_timeLeft <= 0) {
            EndCall();
            yield break;
        }
        StopCoroutine(outgoingCallCoroutine);
        _audioManager.StopAudio(Audio.OUTGOING_CALL);
        _spatialManager.EnableSpatialMapping();
        _cursorsManager.HideCursor();
        _uiManager.ShowBox(Box.LOOK_AROUND);
        _spatialManager.ShowVisualMeshes();
        _audioManager.PlayAudio(Audio.CONNECTED);
        StartCoroutine(_uiManager.HideLookAroundBoxCoroutine(5f, _cursorsManager.ShowCursor));
        StartCoroutine(_spatialManager.HideVisualMeshesCoroutine(5f));
        _widgetsManager.ShowWidgetsContainer();
        _textsManager.ShowTextsContainer();
        _imagesManager.ShowImagesContainer();
        _documentsManager.ShowDocumentsContainer();
        _videosManager.ShowVideosContainer();
    }

    private IEnumerator ResumeCallCoroutine(Coroutine resumeCallCoroutine)
    {
        yield return ResumeConnectionSubroutine(resumeCallCoroutine);
        yield return ResumeCallSubroutine(resumeCallCoroutine);
    }

    private IEnumerator ResumeConnectionSubroutine(Coroutine resumeCallCoroutine)
    {
        yield return new WaitForSeconds(3); // Server Connection Timeout
        if (!_connectionManager.IsConnected)
        {
            StopCoroutine(resumeCallCoroutine);
            _audioManager.StopAudio(Audio.OUTGOING_CALL);
            _uiManager.ShowDialogBox("ERROR DURING SERVER CONNECTION", "CLICK OK TO RETRY", "CANCEL TO UNDO",
                () => {
                    _uiManager.ShowBox(Box.RESUME_CALL);
                    _audioManager.PlayAudio(Audio.OUTGOING_CALL);
                    _connectionManager.ConnectToServer();
                    resumeCallCoroutine = StartCoroutine(_uiManager.ResumeCallCoroutine());
                    StartCoroutine(ResumeCallCoroutine(resumeCallCoroutine));
                },
                () => {
                    _widgetsManager.ClearWidgetsContainer();
                    _textsManager.ClearTextsContainer();
                    _imagesManager.ClearImagesContainer();
                    _documentsManager.ClearDocumentsContainer();
                    _videosManager.ClearVideosContainer();
                    _uiManager.UpdateMenuFooterStatus(Status.NOT_CONNECTED);
                    _uiManager.ShowMenu(Menu.MAIN);
                });
            yield break;
        }
        _uiManager.UpdateMenuFooterStatus(Status.CONNECTED);
    }

    private IEnumerator ResumeCallSubroutine(Coroutine resumeCallCoroutine)
    {
        _connectionManager.ResumeCall();
        lock (_outgoingCallLocker)
            _outgoingCallAccepted = false;
        yield return new WaitForSeconds(3); // Peer Connection Timeout
        if (!_connectionManager.IsConnectedToPeer)
        {
            StopCoroutine(resumeCallCoroutine);
            _audioManager.StopAudio(Audio.OUTGOING_CALL);
            _uiManager.ShowDialogBox("ERROR DURING PEER CONNECTION", "CLICK OK TO RETRY", "CANCEL TO UNDO",
                 () => {
                     _uiManager.ShowBox(Box.RESUME_CALL);
                     _audioManager.PlayAudio(Audio.OUTGOING_CALL);
                     resumeCallCoroutine = StartCoroutine(_uiManager.ResumeCallCoroutine());
                     StartCoroutine(ResumeCallCoroutine(resumeCallCoroutine));
                 },
                 () => {
                     _widgetsManager.ClearWidgetsContainer();
                     _textsManager.ClearTextsContainer();
                     _imagesManager.ClearImagesContainer();
                     _documentsManager.ClearDocumentsContainer();
                     _videosManager.ClearVideosContainer();
                     _uiManager.ShowMenu(Menu.MAIN);
                 });
            yield break;
        }
        _timeLeft = 7.0f; // Outgoing Call Timeout
        bool outgoingCallAccepted;
        lock (_outgoingCallLocker)
            outgoingCallAccepted = _outgoingCallAccepted;
        while (!outgoingCallAccepted && _timeLeft > 0) // If a BYE is received during the loop the call is not accepted
        {
            lock (_outgoingCallLocker)
                outgoingCallAccepted = _outgoingCallAccepted;
            yield return null;
        }
        if (_timeLeft <= 0)
        {
            EndCall();
            yield break;
        }
        _spatialManager.EnableSpatialMapping();
        _widgetsManager.LoadWidgetsContainer();
        _textsManager.LoadTextsContainer();
        _imagesManager.LoadImagesContainer();
        _documentsManager.LoadDocumentsContainer();
        _videosManager.LoadVideosContainer();
        StopCoroutine(resumeCallCoroutine);
        _audioManager.StopAudio(Audio.OUTGOING_CALL);
        _uiManager.HideBox(Box.RESUME_CALL);
        _audioManager.PlayAudio(Audio.CONNECTED);
        _widgetsManager.ShowWidgetsContainer();
        _textsManager.ShowTextsContainer();
        _imagesManager.ShowImagesContainer();
        _documentsManager.ShowDocumentsContainer();
        _videosManager.ShowVideosContainer();
    }

}
